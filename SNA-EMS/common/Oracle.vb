Imports System.Collections.Generic
Imports System.Text
Imports System.Data
Imports System.IO

Imports Oracle.ManagedDataAccess.Types
Imports Oracle.ManagedDataAccess.Client

'
'   クラス：Oracle
'   処理　：DB共通機能を提供する
'

Public Class Oracle
    Private connStr As String

    Private Conn As OracleConnection
    Private Comm As OracleCommand
    Private Txn As OracleTransaction
    ''' <summary>
    ''' Oracle接続情報
    ''' </summary>
    ''' <remarks></remarks>
    Public Shared CONNECTSTR_ORACLE As String = "User Id={0}; Password={1}; Data Source=(DESCRIPTION = (ADDRESS_LIST = (ADDRESS=(PROTOCOL = TCP)(HOST = {2})(PORT = 1521)))(CONNECT_DATA =(SERVICE_NAME = {3}))); Pooling=false"

    '
    '   クラスの新規生成処理
    '   
    Public Sub New()
        Init()
    End Sub

    '
    '   初期化処理
    '   
    Private Sub Init()
        Dim Sid As String = Ini.ReadValue(Ini.Config.DB, "Setting", "SID")
        Dim UserId As String = Ini.ReadValue(Ini.Config.DB, "Setting", "UID")
        Dim Password As String = Ini.ReadValue(Ini.Config.DB, "Setting", "PSW")
        Dim Server As String = Ini.ReadValue(Ini.Config.DB, "Setting", "SVR")
        Dim Port As String = Ini.ReadValue(Ini.Config.DB, "Setting", "PORT")

        Server = Get_ActiveHost()

        Dim DataSource As String = "(DESCRIPTION=" _
     + "(ADDRESS=(PROTOCOL=TCP)(HOST=" + Server + ")(PORT=" + Port + "))" _
     + "(CONNECT_DATA=(SERVICE_NAME=" + Sid + ")));"

        connStr = "User ID=" & UserId.Trim() & ";Password =" & Password.Trim() & ";Data Source=" & DataSource.Trim()
        Conn = New OracleConnection(connStr)

        Comm = Conn.CreateCommand()
        Return

    End Sub




    ''' <summary>
    ''' LocalネットワークとVPNのそれぞれのHostに接続可能かチェックして、有効なIPを返す
    ''' </summary>
    ''' <returns>IP</returns>
    Private Function Get_ActiveHost() As String

        Try
            '個別のHOST名 or IPを指定 .ini もしくはapp.configで取得を想定
            'Dim IPAdrs() As String = New String() {MyHOSTLOCAL, MyHOSTVPN}
            Dim IPAdrs() As String = New String() {"10.2.27.8", "172.16.53.8", "localhost"}

            For i = 0 To 2

                'Pingオブジェクトの作成
                Dim p As New System.Net.NetworkInformation.Ping()
                Dim reply As System.Net.NetworkInformation.PingReply = p.Send(IPAdrs(i))

                '結果を取得
                If reply.Status = System.Net.NetworkInformation.IPStatus.Success Then
                    Console.WriteLine("Ping送信に成功。({0})" & IPAdrs(i), reply.Status)
                    p.Dispose()
                    Return IPAdrs(i)

                Else
                    Console.WriteLine("Ping送信に失敗。({0})" & IPAdrs(i), reply.Status)
                    p.Dispose()
                End If
            Next

        Catch ex As Exception

        End Try

        'local もしくは、VPNのどちらも不可だった場合は、nullを返却
        Return ""

    End Function


    '
    '   DBのアクセス処理
    '   
    Public Function ExecuteQuery(ByVal tableName As String) As DataTable
        Dim dt As New DataTable()
        Dim da As New OracleDataAdapter()
        da.SelectCommand = Comm
        da.Fill(dt)
        dt.TableName = tableName
        Return dt
    End Function

    '
    '   テーブル情報の取得処理
    '   
    Public Function DataTable(ByVal strSql As String, ByVal ParamArray values As OracleParameter()) As DataTable
        Me.Open()

        Dim dt As New DataTable()
        Dim da As New OracleDataAdapter()
        Comm.CommandText = strSql
        If values IsNot Nothing Then
            For index = 0 To values.Length - 1
                Comm.Parameters.Add(values.GetValue(index))
            Next
        End If
        da.SelectCommand = Comm
        da.Fill(dt)
        Return dt
    End Function

    '
    '   テーブル情報の取得処理
    '   
    Public Function DataTable(ByVal strSql As String) As DataTable
        Me.Open()

        Dim ds As New DataSet()
        Dim adapter As New OracleDataAdapter(strSql, Conn)
        adapter.Fill(ds)
        Return ds.Tables(0)
    End Function
 
    Public Function ExecuteReader(ByVal strSql As String, ByVal ParamArray values As OracleParameter()) As OracleDataReader
        Me.Open()

        Comm.CommandText = strSql
        If values IsNot Nothing Then
            For index = 0 To values.Length - 1
                Comm.Parameters.Add(values.GetValue(index))
            Next
        End If
        Return Comm.ExecuteReader()
    End Function

    Public Function ExecuteNonQuery(ByVal strSql As String, ByVal ParamArray values As OracleParameter()) As Integer
        Me.Open()
        Comm.CommandText = strSql
        Comm.Parameters.Clear()
        If values IsNot Nothing Then
            For index = 0 To values.Length - 1
                Comm.Parameters.Add(values.GetValue(index))
            Next
        End If
        Return Comm.ExecuteNonQuery()
    End Function

    Public Function ExecuteNonQuery() As Integer
        Return Comm.ExecuteNonQuery()
    End Function

    '
    '   DB接続処理
    '   
    Public Sub Open()

        'Connecttionがない場合は、Connectionを再作成
        If IsNothing(Conn) Then
            Init()
        End If

        If Conn.State = ConnectionState.Closed Then
            Conn.Open()
        End If
    End Sub

    '
    '   DB接続の中断処理
    '   
    Public Sub Close()
        If Not IsNothing(Conn) Then
            If Conn.State = ConnectionState.Open Then
                Conn.Close()
            End If
            Conn.Dispose()
        End If
    End Sub

    'Public Sub Close()
    '    Conn.Close()
    'End Sub

    '
    '   DBトランザクションの開始処理
    '   
    Public Sub Begin()
        'Comm.Transaction = Conn.BeginTransaction(IsolationLevel.ReadCommitted)
        Txn = Conn.BeginTransaction()
    End Sub

    '
    '   DBトランザクションの確定処理
    '   
    Public Sub Commit()
        Txn.Commit()

    End Sub

    '
    '   DBトランザクションのキャンセル処理
    '   
    Public Sub Rollback()
        Txn.Rollback()
    End Sub

    ''' <summary>
    ''' Oracle更新処理
    ''' </summary>
    ''' <param name="sql"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ExecuteNonQuery(ByVal sql As String) As Integer
        Me.Open()

        Dim cmd As New OracleCommand(sql, Conn)
        cmd.CommandType = CommandType.Text
        Return cmd.ExecuteNonQuery
    End Function
End Class

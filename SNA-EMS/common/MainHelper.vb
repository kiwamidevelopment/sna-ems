﻿Imports System.Reflection
Imports System.Windows.Forms.Form
Imports System.IO
Imports System.Drawing.Imaging
Imports System.Text

Imports Oracle.ManagedDataAccess.Client

'
'   クラス：Ini
'   処理　：システムの共通機能を提供する
'

Public Class MainHelper

    ''' <summary>
    ''' Insert UI all components value into database
    ''' </summary>
    ''' <param name="main">Main UI Object</param>
    Public Shared Function AddTabControlData(ByVal main As Main)
        Dim gid = DateTime.Now.Ticks.ToString()
        Dim tabControlNameDict = New Dictionary(Of String, String)()
        Dim sqlInfoList As List(Of SqlInfo) = New List(Of SqlInfo)
        Try
            For Each control As Control In main.TabControl1.Controls
                Dim tableName As String = control.Tag
                If tableName Is Nothing Then
                    Continue For
                End If
                If control.HasChildren Then
                    Dim tabControlDict As Dictionary(Of String, Object)
                    tabControlDict = New Dictionary(Of String, Object)
                    Helper.GetAllControlsValues(control, tabControlDict, tableName)
                    InsertSqlContent(main, tableName, tabControlDict, sqlInfoList, gid)

                    tabControlNameDict.Add(tableName.ToLower(), "")
                End If
            Next


            'For Each tabPage As TabPage In main.tabPageList
            '    Dim tbName = tabPage.Tag.ToString().ToLower()
            '    If Not tabControlNameDict.ContainsKey(tbName) Then
            '        insertEmptyData(main, sqlInfoList, tbName, gid)
            '    End If
            'Next

            Dim oracle As New Oracle()
            Try
                oracle.Open()
                oracle.Begin()
                For index = 0 To sqlInfoList.Count
                    If index < sqlInfoList.Count Then
                        Dim tSqlInfo As SqlInfo = sqlInfoList(index)
                        Dim result = oracle.ExecuteNonQuery(tSqlInfo.Sql, tSqlInfo.ParameterList.ToArray())
                        If result < 1 Then
                            'Throw New Exception("add data error")
                        End If
                    End If
                Next
                oracle.Commit()
                main.gid = gid
            Catch ex As Exception
                oracle.Rollback()
                Throw ex
            Finally
                oracle.Close()
            End Try
        Catch ex As Exception
            Throw ex
        End Try
        Return Nothing
    End Function

    Private Shared Function insertEmptyData(ByVal main As Main, ByVal sqlInfoList As List(Of SqlInfo), ByVal tableName As String, ByVal gid As String)
        Dim tabControlDict = New Dictionary(Of String, Object)
        InsertSqlContent(main, tableName, tabControlDict, sqlInfoList, gid)
        Return Nothing
    End Function

    Public Shared Function InsertSqlContent(ByVal main As Main, ByVal tableName As String, ByVal tabControlDict As Dictionary(Of String, Object), ByVal sqlInfoList As List(Of SqlInfo), ByVal gid As String)
        Dim pair As KeyValuePair(Of String, Object)
        Dim sql As StringBuilder = New StringBuilder()
        Dim sqlp As StringBuilder = New StringBuilder()
        Dim sqlv As StringBuilder = New StringBuilder()
        Dim parameterList As List(Of OracleParameter) = New List(Of OracleParameter)
        'add gid
        If Not tabControlDict.ContainsKey("gid") Then
            tabControlDict.Add("gid", gid)
        End If

        'add MODEL
        If ("m_header" = tableName.ToLower()) And Not tabControlDict.ContainsKey("model") Then
            tabControlDict.Add("model", main.M_HEADER_MODEL.Text)

        End If
        'add KANRI_NO
        If Not tabControlDict.ContainsKey("kanri_no") Then
            tabControlDict.Add("kanri_no", main.M_HEADER_KANRI_NO.Text)
        End If

        'create user
        If tabControlDict.ContainsKey("created_by") Then
            tabControlDict.Item("created_by") = LoginRoler.Employee.EMP_NAME
        Else
            tabControlDict.Add("created_by", LoginRoler.Employee.EMP_NAME)
        End If
        If tabControlDict.ContainsKey("created_date") Then
            tabControlDict.Item("created_date") = DateTime.Now
        Else
            tabControlDict.Add("created_date", DateTime.Now)
        End If
        'insert into table (...) values (...)
        For Each pair In tabControlDict
            Dim parameter As OracleParameter
            'If ("m_header" = tableName.ToLower()) And pair.Key = "certificate" Then
            '    If pair.Value <> "" And File.Exists(pair.Value) Then
            '        parameter = New OracleParameter(String.Format(":{0}", pair.Key), File.ReadAllBytes(pair.Value))
            '    Else
            '        parameter = New OracleParameter(String.Format(":{0}", pair.Key), pair.Value)
            '    End If
            'Else
            '    parameter = New OracleParameter(String.Format(":{0}", pair.Key), pair.Value)
            'End If
            parameter = New OracleParameter(String.Format(":{0}", pair.Key), pair.Value)
            sqlp.AppendFormat(", {0}", pair.Key)
            sqlv.AppendFormat(", :{0}", pair.Key)
            parameterList.Add(parameter)
        Next
        sql.AppendFormat("INSERT INTO {0} ({1}) VALUES ({2})", tableName, sqlp.ToString().Substring(1), sqlv.ToString().Substring(1))
        Dim sqlInfo As SqlInfo = New SqlInfo
        sqlInfo.Sql = sql.ToString()
        sqlInfo.ParameterList = parameterList
        sqlInfoList.Add(sqlInfo)

        Return Nothing
    End Function


    Public Shared Function DelTabControlData(ByVal main As Main)

        Dim sqlInfoList As List(Of SqlInfo) = New List(Of SqlInfo)
        Dim oracle As New Oracle()
        Try
            oracle.Open()
            oracle.Begin()
            For Each control As Control In main.tabPageList
                Dim tableName As String = control.Tag
                If tableName Is Nothing Then
                    Continue For
                End If

                Dim sql As String = String.Format("DELETE FROM {0} WHERE gid = :gid", tableName)
                oracle.ExecuteNonQuery(sql, New OracleParameter(":gid", main.gid))
            Next
            oracle.Commit()
            main.gid = Nothing
        Catch ex As Exception
            oracle.Rollback()
            Throw ex
        Finally
            oracle.Close()
        End Try

        Return Nothing
    End Function


    Public Shared Function TrancateT_TANA()

        Dim sqlInfoList As List(Of SqlInfo) = New List(Of SqlInfo)
        Dim oracle As New Oracle()
        Try
            oracle.Open()
            oracle.Begin()
            Dim tableName As String = ""
            Dim sql As String = String.Format("TRUNCATE TABLE T_TANA", tableName)
            oracle.ExecuteNonQuery(sql, New OracleParameter(":gid", ""))

            oracle.Commit()

        Catch ex As Exception
            oracle.Rollback()
            Throw ex
        Finally
            oracle.Close()
        End Try

        Return Nothing
    End Function


    ''' <summary>
    ''' 選択タブのコントロール値をデータベースへ反映
    ''' </summary>
    ''' <param name="main"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function UpdateTabControlData(ByVal main As Main) As Boolean
        Dim tabControlNameDict = New Dictionary(Of String, String)()
        Dim sqlInfoList As List(Of SqlInfo) = New List(Of SqlInfo)
        Try
            For Each control As Control In main.TabControl1.Controls
                Dim tableName As String = control.Tag
                If tableName Is Nothing Then
                    Continue For
                End If
                If control.HasChildren Then
                    Dim tabControlDict As Dictionary(Of String, Object) = New Dictionary(Of String, Object)

                    Helper.GetAllControlsValues(control, tabControlDict, tableName) '全てのコントロール値を取得
                    Dim exist = MasterRepository.IsExistFromDB(tableName, main.gid)
                    If exist Then
                        updateSqlContent(main, tableName, tabControlDict, sqlInfoList, main.gid)
                    Else
                        InsertSqlContent(main, tableName, tabControlDict, sqlInfoList, main.gid)
                    End If

                    tabControlNameDict.Add(tableName.ToLower(), "")
                End If
            Next

            If sqlInfoList.Count = 0 Then
                Return False
            End If

            Dim oracle As New Oracle()
            Try
                oracle.Open()
                oracle.Begin()

                For index = 0 To sqlInfoList.Count
                    If index < sqlInfoList.Count Then
                        Dim tSqlInfo As SqlInfo = sqlInfoList(index)
                        Dim result = oracle.ExecuteNonQuery(tSqlInfo.Sql, tSqlInfo.ParameterList.ToArray())
                        If result < 1 Then
                        End If
                    End If
                Next

                For Each tabPage As TabPage In main.tabPageList
                    Dim tbName = tabPage.Tag.ToString().ToLower()
                    If Not tabControlNameDict.ContainsKey(tbName) Then
                        'updateEmptyData(main, sqlInfoList, tbName, main.gid)
                        'delete
                        Dim sql As String = String.Format("DELETE FROM {0} WHERE gid = :gid", tbName)
                        oracle.ExecuteNonQuery(sql, New OracleParameter(":gid", main.gid))
                    End If
                Next

                oracle.Commit()

            Catch ex As Exception
                oracle.Rollback()
                Throw ex
            Finally
                oracle.Close()
            End Try

            If sqlInfoList.Count > 0 Then
                Return True
            End If
        Catch ex As Exception
            Throw ex
        End Try
        Return False
    End Function

    Private Shared Function updateEmptyData(ByVal main As Main, ByVal sqlInfoList As List(Of SqlInfo), ByVal tableName As String, ByVal gid As String)
        Dim tabControlDict = New Dictionary(Of String, Object)
        updateSqlContent(main, tableName, tabControlDict, sqlInfoList, gid)
        Return Nothing
    End Function

    Private Shared Function updateSqlContent(ByVal main As Main, ByVal tableName As String, ByVal tabControlDict As Dictionary(Of String, Object), ByVal sqlInfoList As List(Of SqlInfo), ByVal gid As String)
        Dim pair As KeyValuePair(Of String, Object)
        Dim sql As StringBuilder = New StringBuilder()
        Dim sqlp As StringBuilder = New StringBuilder()
        Dim sqlv As StringBuilder = New StringBuilder()
        Dim parameterList As List(Of OracleParameter) = New List(Of OracleParameter)
        'add gid
        If Not tabControlDict.ContainsKey("gid") Then
            tabControlDict.Add("gid", gid)
        End If
        'add KANRI_NO
        If Not tabControlDict.ContainsKey("kanri_no") Then
            tabControlDict.Add("kanri_no", main.M_HEADER_KANRI_NO.Text)
        End If
        'M_HEADER_MODIFIED_BY   M_HEADER_MODIFIED_DATE
        If tabControlDict.ContainsKey("modified_by") Then
            tabControlDict.Item("modified_by") = LoginRoler.Employee.EMP_NAME
        Else
            tabControlDict.Add("modified_by", LoginRoler.Employee.EMP_NAME)
        End If
        If tabControlDict.ContainsKey("modified_date") Then
            tabControlDict.Item("modified_date") = DateTime.Now
        Else
            tabControlDict.Add("modified_date", DateTime.Now)
        End If
        If tableName.ToLower() = "m_header" Then
            'add MODEL
            If Not tabControlDict.ContainsKey("model") Then
                tabControlDict.Add("model", main.M_HEADER_MODEL.Text)
            End If
        End If

        If main.gmsValues.ContainsKey(tableName.ToLower) Then
            If Not Helper.IsDataChanged(main, tabControlDict, main.gmsValues.Item(tableName.ToLower)) Then
                Return Nothing
            End If
        End If

        'update tablename set ... where ..
        For Each pair In tabControlDict
            'If pair.Key = "kanri_no" Then
            '    Continue For
            'End If
            If "gid".Equals(pair.Key) Then
                Continue For
            End If
            Dim parameter As OracleParameter
            'If ("m_header" = tableName.ToLower()) And pair.Key = "certificate" Then
            '    If pair.Value <> "" And File.Exists(pair.Value) Then
            '        parameter = New OracleParameter(String.Format(":{0}", pair.Key), File.ReadAllBytes(pair.Value))
            '    Else
            '        parameter = New OracleParameter(String.Format(":{0}", pair.Key), pair.Value)
            '    End If
            'Else
            '    parameter = New OracleParameter(String.Format(":{0}", pair.Key), pair.Value)
            'End If
            parameter = New OracleParameter(String.Format(":{0}", pair.Key), pair.Value)
            sqlp.AppendFormat(", {0}=:{0}", pair.Key)
            parameterList.Add(parameter)
        Next
        parameterList.Add(New OracleParameter(":gid", tabControlDict.Item("gid")))

        sql.AppendFormat("UPDATE {0} SET {1} WHERE gid = :gid", tableName, sqlp.ToString().Substring(1))
        Dim sqlInfo As SqlInfo = New SqlInfo
        sqlInfo.Sql = sql.ToString()
        sqlInfo.ParameterList = parameterList
        sqlInfoList.Add(sqlInfo)

        Return Nothing
    End Function

    Public Shared Sub ClearAllControlsValues(ByVal main As Main)
        For Each tabPage As TabPage In main.tabPageList
            If tabPage.HasChildren Then
                'clear all data
                Helper.ClearAllControlsValues(tabPage)
            End If
        Next
        'For Each control As Control In main.TabControl1.Controls
        '    If control.HasChildren Then
        '        'clear all data
        '        Helper.ClearAllControlsValues(control)
        '    End If
        'Next
        main.ClearFLG(True, True)

        main.gid = Nothing
    End Sub

    
End Class


Public Class SqlInfo
    Public Sub New()
    End Sub
    Private _sql As String
    Public Property Sql() As String
        Get
            Return _sql
        End Get
        Set(ByVal Value As String)
            _sql = Value
        End Set
    End Property

    Private _parameterList As List(Of OracleParameter)
    Public Property ParameterList() As List(Of OracleParameter)
        Get
            Return _parameterList
        End Get
        Set(ByVal Value As List(Of OracleParameter))
            _parameterList = Value
        End Set
    End Property
End Class

﻿Imports System.Reflection
Imports System.Windows.Forms.Form
Imports System.IO
Imports System.Drawing.Imaging
Imports System.Text

Imports Oracle.ManagedDataAccess.Client

'
'   クラス：Ini
'   処理　：HTRentalの共通機能を提供する
'

Public Class ReaseStatusHelper

    ''' <summary>
    ''' HT取込データの更新
    ''' </summary>
    ''' <param name="gid">GID</param>
    ''' <param name="REASE_FLG">貸出状態(0:貸出/1:返却)</param>
    ''' <param name="w_o">W/O</param>
    ''' <param name="REASE_DATE">貸出日</param>
    ''' <param name="syain_cd">貸出社員番号</param>
    ''' <param name="return_date">返却日</param>
    ''' <returns>処理結果</returns>
    ''' <remarks></remarks>
    Public Shared Function UpdateHTData(ByVal gid As String, ByVal REASE_FLG As String, ByVal w_o As String, ByVal REASE_DATE As String, ByVal syain_cd As String, ByVal return_date As String) As Boolean

        Try
            Dim oracle As New Oracle()
            Dim sql As String

            Try
                oracle.Open()
                oracle.Begin()

                sql = "UPDATE M_REASE_STATUS SET " _
                + "REASE_FLG = " + IIf(IsNumeric(REASE_FLG), REASE_FLG, "null") + " ," _
                + "W_O = '" + w_o + "' ," _
                + "REASE_DATE = " + IIf(IsDate(REASE_DATE), "'" + REASE_DATE.Substring(0, 10) + "'", "null") + " ," _
                + "SYAIN_CD = " + IIf(IsNumeric(syain_cd), syain_cd, "null") + " ," _
                + "RETURN_DATE = " + IIf(IsDate(return_date), "to_timestamp('" + return_date + "','yyyy/mm/dd hh24:mi:ss')", "null") + " " _
                + "WHERE KANRI_NO = '" + gid + "'"

                oracle.ExecuteNonQuery(sql, New OracleParameter(":KANRI_NO", gid))

                oracle.Commit()

            Catch ex As Exception
                oracle.Rollback()
                Throw ex
            Finally
                oracle.Close()
            End Try

            Return True
        Catch ex As Exception
            Throw ex
            Return False
        End Try

    End Function

	''' <summary>
	''' HT取込データの削除
	''' </summary>
	''' <returns>処理結果</returns>
	''' <remarks></remarks>
	Public Shared Function DeleteHTData(ByVal LendNo As String) As Boolean
		Try
			Dim oracle As New Oracle()
			Dim sql As String

			Try
				oracle.Open()
				oracle.Begin()

				sql = "DELETE FROM T_LEND " _
				+ "WHERE LEND_NO = :LEND_NO "

				oracle.ExecuteNonQuery(sql, New OracleParameter(":LEND_NO", LendNo))

				sql = "DELETE FROM T_LEND_WO " _
				+ "WHERE LEND_NO = :LEND_NO "

				oracle.ExecuteNonQuery(sql, New OracleParameter(":LEND_NO", LendNo))


				oracle.Commit()

			Catch ex As Exception
				oracle.Rollback()
				Throw ex
			Finally
				oracle.Close()
			End Try

			Return True
		Catch ex As Exception
			Throw ex
			Return False
		End Try
	End Function

End Class

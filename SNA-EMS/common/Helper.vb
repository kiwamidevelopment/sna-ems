﻿Imports System.Reflection
Imports System.Windows.Forms.Form
Imports System.IO
Imports System.Drawing.Imaging

Imports Oracle.ManagedDataAccess.Client
Imports Oracle.ManagedDataAccess.Types


'
'   クラス：Helper
'   処理　：システムの各種共通機能を提供する
'
Public Class Helper

    Public Shared Function DB_NullOrEmpty(ByVal data As Object) As Object
        If (Not data Is Nothing And String.IsNullOrEmpty(data.ToString()) = False) Then
            If TypeOf (data) Is DateTime Then
                Return Format(data, "yyyy/MM/dd")
            End If

            Return data
        ElseIf True Then
            Return ""
        End If
        Return ""
    End Function

    Public Shared Function DB_NullOrEmptyHaveTime(ByVal data As Object) As Object
        If (Not data Is Nothing And String.IsNullOrEmpty(data.ToString()) = False) Then
            Return data
        ElseIf True Then
            Return ""
        End If
        Return ""
    End Function
    '
    Public Shared Function DataReaderToEntity(Of T As {New})(ByVal dataReader As OracleDataReader, ByVal strategy As T) As T
        Try
            If dataReader.HasRows = False Then
                Return Nothing
            Else
                Dim entity As T = New T
                While dataReader.Read()
                    For i = 0 To dataReader.FieldCount - 1
                        For Each prop In strategy.GetType().GetProperties()
                            If dataReader.GetName(i).ToLower() = prop.Name.ToLower() Then
                                If dataReader.GetValue(i) Is DBNull.Value Then
                                    prop.SetValue(entity, Nothing, Nothing)
                                Else
                                    prop.SetValue(entity, dataReader.GetValue(i), Nothing)
                                End If

                            End If
                        Next
                    Next
                End While
                Return entity
            End If
        Catch ex As Exception
            Throw ex
        Finally
            dataReader.Close()
        End Try
    End Function


#Region "InitControlValues"
    'load data from db
    Public Shared Function InitControlsData(ByVal dataReader As OracleDataReader, ByRef control As Control, ByVal tableName As String)
        Try
            If dataReader.HasRows Then
                DataReaderToControls(dataReader, control, tableName)
            End If
        Catch ex As Exception
            Throw ex
        Finally

        End Try
        Return Nothing
    End Function

    Private Shared Function DataReaderToControls(ByVal dataReader As OracleDataReader, ByRef control As Control, ByVal tableName As String)
        Try
            For i = 0 To dataReader.FieldCount - 1
                'MessageBox.Show(Convert.ToString(i))
                If control.HasChildren Then
                    For Each fIns As Control In control.Controls
                        If fIns.HasChildren Then
                            'recursion
                            DataReaderToControls(dataReader, fIns, tableName)
                        Else
                            DataReaderToControl(dataReader, fIns, i, tableName)
                        End If
                    Next
                End If
            Next
        Catch ex As Exception
            Throw ex
        Finally
        End Try
        Return Nothing
    End Function

    Public Shared Function DataReaderToImageOrPDF(ByVal dataReader As OracleDataReader, ByVal main As Main, ByVal tableName As String)
        Try
            For i = 0 To dataReader.FieldCount - 1
                tableName = tableName.ToLower()
                Dim cname = dataReader.GetName(i).ToLower()
                'If "m_header".Equals(tableName) And "certificate".Equals(cname) Then
                '    Dim blob As OracleBlob = dataReader.GetOracleBlob(i)
                '    If Not blob Is OracleBlob.Null And Not blob Is Nothing And blob.Length > 1 Then
                '        main.M_HEADER_CERTIFICATE.Text = SavePdfFile(blob)
                '    Else
                '        main.M_HEADER_CERTIFICATE.Text = ""
                '    End If
                'End If

                If "m_header".Equals(tableName) And "image".Equals(cname) Then
                    Dim blob As OracleBlob = dataReader.GetOracleBlob(i)
                    If Not blob Is OracleBlob.Null And Not blob Is Nothing And blob.Length > 1 Then
                        main.M_HEADER_IMAGE.ImageLocation = SaveImageFile(blob)
                    End If
                End If
            Next
        Catch ex As Exception
            Throw ex
        Finally
        End Try
        Return Nothing
    End Function

    '
    '   指定テーブルのデータを指定画面の各項目に設定
    '
    Private Shared Function DataReaderToControl(ByVal dataReader As OracleDataReader, ByRef control As Control, ByVal i As Int32, ByVal tableName As String)
        Try
            tableName = tableName.ToLower()
            Dim tempTableName = tableName + "_"


            '            System.Diagnostics.Debug.WriteLine("画面項目:" & control.Name & " Control: " & dataReader.GetName(i))
            '            If control.Name = "M_PME_DUE_DATE" And "DUE_DATE" = dataReader.GetName(i) Then
            '            System.Diagnostics.Debug.WriteLine("画面項目:" & control.Name & " Text: " & control.Text)
            '            End If

            If Not String.IsNullOrEmpty(control.Name) And control.Name <> Nothing And control.Name.ToLower().IndexOf(tempTableName) >= 0 Then
                Dim cname = control.Name.ToLower().Replace(tempTableName, "")
                If cname.Equals(dataReader.GetName(i).ToLower()) Then
                    If Not dataReader.GetValue(i) Is Nothing And Not dataReader.GetValue(i) Is DBNull.Value Then
                        Try
                            If TypeOf (control) Is TextBox Then
                                'If "m_header".Equals(tableName) And "certificate".Equals(cname) Then
                                '    Dim blob As OracleBlob = dataReader.GetOracleBlob(i)
                                '    If Not blob Is OracleBlob.Null And Not blob Is Nothing And blob.Length > 1 Then
                                '        control.Text = SavePdfFile(blob)
                                '    Else
                                '        control.Text = ""
                                '    End If
                                'Else
                                '    control.Text = Convert.ToString(dataReader.GetValue(i))
                                'End If
                                control.Text = Convert.ToString(dataReader.GetValue(i))
                            ElseIf TypeOf (control) Is ComboBox Then
                                Dim comboBox As ComboBox = CType(control, ComboBox)
                                comboBox.Text = Convert.ToString(dataReader.GetValue(i))
                            ElseIf TypeOf (control) Is PictureBox Then
                                If "m_header".Equals(tableName) And "image".Equals(cname) Then
                                    Dim blob As OracleBlob = dataReader.GetOracleBlob(i)
                                    If Not blob Is OracleBlob.Null And Not blob Is Nothing And blob.Length > 1 Then
                                        Dim pictureBox As PictureBox = CType(control, PictureBox)
                                        pictureBox.ImageLocation = SaveImageFile(blob)
                                    End If
                                End If
                                'If Not blob Is OracleLob.Null And Not blob Is Nothing And blob.Length > 1 Then
                                '    Dim pictureBox As PictureBox = CType(control, PictureBox)
                                '    Dim buffer As Byte() = New Byte(blob.Length - 1) {}
                                '    blob.Read(buffer, 0, buffer.Length)
                                '    Dim Stream As New IO.MemoryStream(buffer, True)
                                '    pictureBox.Image = New Bitmap(Stream)
                                'End If
                            ElseIf TypeOf (control) Is CheckBox Then
                                Dim checkBox As CheckBox = CType(control, CheckBox)
                                If Convert.ToString(dataReader.GetValue(i)).ToString() = "Y" Then
                                    checkBox.Checked = True
                                Else
                                    checkBox.Checked = False
                                End If
                            ElseIf TypeOf (control) Is DateTimePicker Then
                                Dim dateTimePicker As DateTimePicker = CType(control, DateTimePicker)

                                If String.IsNullOrEmpty(dataReader.GetValue(i)) = True Then
                                    dateTimePicker.Value = Now
                                    dateTimePicker.Checked = False
                                    dateTimePicker.Format = DateTimePickerFormat.Custom
                                    dateTimePicker.CustomFormat = "   "
                                Else
                                    dateTimePicker.Checked = True
                                    dateTimePicker.Value = Convert.ToDateTime(dataReader.GetValue(i))
                                    dateTimePicker.Format = DateTimePickerFormat.Short
                                End If

                            End If
                        Catch ex As Exception
                            Throw ex
                        End Try
                    Else
                        If TypeOf (control) Is DateTimePicker Then
                            Dim dateTimePicker As DateTimePicker = CType(control, DateTimePicker)
                            dateTimePicker.Value = Now.Date '2015/09/11 taskplan MOD 入れ替え(値を設定するとチェックは自動的に入る為)
                            dateTimePicker.Checked = False  '2015/09/11 taskplan MOD
                            ' 2016-04-07 YAMAOKA EDIT 未チェックの場合、表示設定をクリアする START
                            'dateTimePicker.Format = DateTimePickerFormat.Short
                            ''dateTimePicker.CustomFormat = "   "
                            dateTimePicker.Format = DateTimePickerFormat.Custom
                            dateTimePicker.CustomFormat = "   "
                            ' 2016-04-07 YAMAOKA EDIT 未チェックの場合、表示設定をクリアする END


                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            Throw ex
        Finally

        End Try
        Return Nothing
    End Function




    'load data from db
    Public Shared Function InitRefData(ByVal main As Main, ByVal values As Dictionary(Of String, Object), ByRef control As Control)
        Try
            For Each fIns As Control In control.Controls
                If fIns.HasChildren Then
                    'recursion
                    InitRefData(main, values, fIns)
                Else
                    SetRefData(main, values, fIns)
                End If
            Next
        Catch ex As Exception
            Throw ex
        Finally

        End Try
        Return Nothing
    End Function

    Private Shared Function SetRefData(ByVal main As Main, ByVal values As Dictionary(Of String, Object), ByRef control As Control)
        Try
            Dim controlName = control.Name
            If controlName.IndexOf("_ref_") = -1 Then
                Return Nothing
            End If
            controlName = controlName.Substring(controlName.IndexOf("_ref_") + 5)
            Dim tableName = controlName.Substring(0, controlName.IndexOf("_", 2)).ToLower()
            Dim fieldName = controlName.Substring(controlName.IndexOf("_", 2) + 1).ToLower()
            If Not values.ContainsKey(fieldName) Then
                Return Nothing
            End If
            Dim val As String = ""

            If "m_header".Equals(tableName) And "certificate_name".Equals(fieldName) Then
                val = main.M_HEADER_CERTIFICATE_NAME.Text
            ElseIf "image".Equals(fieldName) Then
                val = main.M_HEADER_IMAGE.ImageLocation
            ElseIf values.Item(fieldName) Is DBNull.Value Then

            Else
                val = Convert.ToString(values.Item(fieldName))
            End If
            'If String.IsNullOrEmpty(val) Then
            '    Return Nothing
            'End If
            Try
                If TypeOf (control) Is ComboBox Then
                    Dim comboBox As ComboBox = CType(control, ComboBox)
                    If Not comboBox.Items.Contains(val) Then
                        comboBox.Items.Add(val)
                    End If
                    If Not comboBox.Items.Contains("") Then
                        comboBox.Items.Add("")
                    End If

                    comboBox.SelectedItem = val
                ElseIf TypeOf (control) Is PictureBox Then
                    Dim pictureBox As PictureBox = CType(control, PictureBox)
                    pictureBox.ImageLocation = val
                ElseIf TypeOf (control) Is CheckBox Then
                    If Not String.IsNullOrEmpty(val) Then
                        Dim checkBox As CheckBox = CType(control, CheckBox)
                        If "Y".Equals(val) Then
                            checkBox.Checked = True
                        Else
                            checkBox.Checked = False
                        End If
                    End If
                ElseIf TypeOf (control) Is DateTimePicker Then
                    Dim dateTimePicker As DateTimePicker = CType(control, DateTimePicker)
                    If Not String.IsNullOrEmpty(val) Then
                        dateTimePicker.Format = DateTimePickerFormat.Short
                        dateTimePicker.Value = Convert.ToDateTime(val)
                    Else
                        dateTimePicker.Format = DateTimePickerFormat.Custom
                        dateTimePicker.CustomFormat = "   "
                        dateTimePicker.Checked = False
                    End If
                ElseIf TypeOf (control) Is TextBox Then
                    control.Text = val
                Else
                End If
            Catch ex As Exception
                Throw ex
            End Try

        Catch ex As Exception
            Throw ex
        Finally

        End Try
        Return Nothing
    End Function

#End Region

#Region "ClearControlValues"
    Public Shared Function ClearAllControlsValues(ByRef control As Control)
        Try
            If control.HasChildren Then
                For Each fIns As Control In control.Controls
                    If fIns.HasChildren Then
                        'recursion
                        ClearAllControlsValues(fIns)
                    Else
                        ClearControlsValues(fIns)
                    End If
                Next
            End If
        Catch ex As Exception
            Throw ex
        Finally
        End Try
        Return Nothing
    End Function

    Private Shared Function ClearControlsValues(ByRef control As Control)
        Try
            Select Case control.GetType().Name
                Case GetType(TextBox).Name

                    If Not control.Tag Is Nothing And "NUMBER".Equals(control.Tag) Then
                        control.Text = "0"
                    Else
                        control.Text = ""
                    End If
                Case GetType(ComboBox).Name
                    Dim comboBox As ComboBox = CType(control, ComboBox)
                    If comboBox.Items.Count > 0 Then
                        comboBox.SelectedIndex = 0
                    End If
                Case GetType(PictureBox).Name
                    Dim pictureBox As PictureBox = CType(control, PictureBox)
                    pictureBox.Image = Nothing
                    pictureBox.ImageLocation = ""
                Case GetType(CheckBox).Name
                    Dim checkBox As CheckBox = CType(control, CheckBox)
                    checkBox.Checked = False
                Case GetType(DateTimePicker).Name
                    Dim dateTimePicker As DateTimePicker = CType(control, DateTimePicker)
                    dateTimePicker.Value = Date.Now
                    If dateTimePicker.ShowCheckBox = True Then  '2015/09/11 taskplan ADD
                        dateTimePicker.Checked = False
                    End If
            End Select
        Catch ex As Exception
            Throw ex
        Finally

        End Try
        Return Nothing
    End Function

#End Region


    Private Shared Function SetControlText(ByRef control As Control)
        Try
            Dim temp As String
            Select Case control.GetType().Name
                Case GetType(CheckBox).Name
                    Dim checkBox As CheckBox = CType(control, CheckBox)
                    checkBox.Text = ""
                    temp = checkBox.Name.Split("_")(1) + "_"
                    temp = checkBox.Name.Substring(checkBox.Name.IndexOf(temp)).Replace(temp, "")
                    checkBox.Text = Ini.ReadValue(Ini.Config.UX, "Setting", temp)
                Case GetType(Label).Name
                    Dim lbl As Label = CType(control, Label)
                    'lbl.Text = ""
                    If lbl.Name.IndexOf("_lbl_") > 0 Then
                        temp = lbl.Name.Substring(lbl.Name.IndexOf("_lbl_")).Replace("_lbl_", "")
                    Else
                        temp = Mid(lbl.Name, "lbl_".Length + 1)
                    End If
                    Dim val = Ini.ReadValue(Ini.Config.UX, "Setting", temp)
                    If Not String.IsNullOrEmpty(val) Then
                        lbl.Text = val
                    End If
                Case GetType(Button).Name
                    Dim btn As Button = CType(control, Button)
                    btn.Text = ""
                    btn.Text = Ini.ReadValue(Ini.Config.UX, "Setting", Mid(btn.Name, "btn_".Length + 1))
                Case GetType(GroupBox).Name
                    Dim gpx As GroupBox = CType(control, GroupBox)
                    gpx.Text = ""
                    If gpx.Name.IndexOf("_gpx_") > 0 Then
                        temp = gpx.Name.Substring(gpx.Name.IndexOf("_gpx_")).Replace("_gpx_", "")
                    Else
                        temp = Mid(gpx.Name, "gpx_".Length + 1)
                    End If
                    gpx.Text = Ini.ReadValue(Ini.Config.UX, "Setting", temp)
            End Select
        Catch ex As Exception
            Throw ex
        Finally

        End Try
        Return Nothing
    End Function

    Public Shared Function SetAllControlText(ByRef control As Control)

        Try
            If control.HasChildren Then
                If GetType(TabControl).Name.StartsWith(control.GetType().Name) Then
                    Dim tb As TabControl = CType(control, TabControl)
                    For Each tp As TabPage In tb.TabPages
                        tp.Text = ""
                        tp.Text = Ini.ReadValue(Ini.Config.UX, "Setting", Mid(tp.Name, "lbl_".Length + 1))

                    Next

                ElseIf GetType(GroupBox).Name.StartsWith(control.GetType().Name) Then
                    Dim gpx As GroupBox = CType(control, GroupBox)
                    Dim temp As String = ""
                    gpx.Text = ""
                    If gpx.Name.IndexOf("_gpx_") > 0 Then
                        temp = gpx.Name.Substring(gpx.Name.IndexOf("_gpx_")).Replace("_gpx_", "")
                    Else
                        temp = Mid(gpx.Name, "gpx_".Length + 1)
                    End If
                    gpx.Text = Ini.ReadValue(Ini.Config.UX, "Setting", temp)
                End If

                For Each fIns As Control In control.Controls
                    If fIns.HasChildren Then
                        'recursion
                        SetAllControlText(fIns)
                    Else
                        SetControlText(fIns)
                    End If
                Next
            End If
        Catch ex As Exception
            Throw ex
        Finally
        End Try
        Return Nothing
    End Function


#Region "GetControlValues"
    Public Shared Function GetAllControlsValues(ByVal control As Control, ByRef dict As Dictionary(Of String, Object), ByVal tableName As String)
        Try
            If control.HasChildren Then
                For Each fIns As Control In control.Controls

                    If fIns.HasChildren Then
                        'recursion
                        GetAllControlsValues(fIns, dict, tableName)
                        'GetControlsValues(fIns, dict, tableName)
                    Else
                        GetControlsValues(fIns, dict, tableName)
                        'GetAllControlsValues(fIns, dict, tableName)
                    End If
                Next

            End If
        Catch ex As Exception
            Throw ex
        Finally

        End Try
        Return Nothing
    End Function

    Private Shared Function GetControlsValues(ByVal control As Control, ByRef dict As Dictionary(Of String, Object), ByVal tableName As String)
        If dict Is Nothing Then
            dict = New Dictionary(Of String, Object)
        End If
        Try

            If control.Name.IndexOf("_ref_") > -1 Then
                Return Nothing
            End If


            tableName = tableName.ToLower()
            Dim ttName = tableName + "_"
            If Not String.IsNullOrEmpty(control.Name) And control.Name <> Nothing And control.Name.ToLower().IndexOf(ttName) >= 0 Then
                Dim cname = control.Name.ToLower()
                If cname.IndexOf(ttName) > -1 Then
                    cname = cname.Replace(ttName, "")
                End If
                If Not dict.ContainsKey(cname) Then
                    If TypeOf (control) Is TextBox Then
                        If "m_header".Equals(tableName) And "certificate_name".Equals(cname) Then
                            Dim byte_buffer As Byte() = Nothing
                            If Not String.IsNullOrEmpty(control.Text) And File.Exists(control.Text) Then
                                byte_buffer = IO.File.ReadAllBytes(control.Text)
                                dict.Add("certificate", byte_buffer)
                                dict.Add("certificate_name", IO.Path.GetFileName(control.Text))
                            Else
                                If String.IsNullOrEmpty(control.Text) Then
                                    byte_buffer = New Byte(0) {}
                                    dict.Add("certificate", byte_buffer)
                                    dict.Add("certificate_name", "")
                                Else
                                End If
                            End If
                        Else
                            dict.Add(cname, GetControlValue(control))
                        End If
                    ElseIf TypeOf (control) Is ComboBox Then
                        Dim comboBox As ComboBox = CType(control, ComboBox)
                        If Not comboBox.Items Is Nothing And comboBox.Items.Count > 0 Then
                            dict.Add(cname, comboBox.SelectedValue)
                        End If
                    ElseIf TypeOf (control) Is PictureBox Then
                        Dim pictureBox As PictureBox = CType(control, PictureBox)
                        Dim byte_buffer As Byte() = Nothing
                        If Not String.IsNullOrEmpty(pictureBox.ImageLocation) And File.Exists(pictureBox.ImageLocation) Then
                            byte_buffer = IO.File.ReadAllBytes(pictureBox.ImageLocation)
                            dict.Add("image_name", IO.Path.GetFileName(pictureBox.ImageLocation))
                            dict.Add(cname, byte_buffer)
                        ElseIf Not pictureBox.Image Is Nothing Then
                            Dim picStream As New MemoryStream
                            pictureBox.Image.Save(picStream, ImageFormat.Gif)
                            byte_buffer = picStream.ToArray()
                            picStream.Close()

                            dict.Add("image_name", IO.Path.GetFileName(pictureBox.ImageLocation))
                            dict.Add(cname, byte_buffer)
                        Else
                            If String.IsNullOrEmpty(control.Text) Then
                                byte_buffer = New Byte(0) {}
                                dict.Add("image_name", IO.Path.GetFileName(pictureBox.ImageLocation))
                                dict.Add(cname, byte_buffer)
                            End If
                        End If

                    ElseIf TypeOf (control) Is CheckBox Then
                        Dim checkBox As CheckBox = CType(control, CheckBox)
                        If checkBox.Checked Then
                            dict.Add(cname, "Y")
                        Else
                            dict.Add(cname, "N")
                        End If
                    ElseIf TypeOf (control) Is DateTimePicker Then
                        '   2015/03/22  日付データの取得修正
                        Dim dateTimePicker As DateTimePicker = CType(control, DateTimePicker)
                        Dim val = dateTimePicker.Value
                        Dim maxval As DateTime = Now
                        Dim flg As Integer = 0

                        If maxval.Date = val.Date Then
                            flg = 1
                        End If

                        If dateTimePicker.ShowCheckBox = True AndAlso dateTimePicker.Checked = False Then '2015/09/11 taskplan check値の判断処理追加
                            dict.Add(cname, "") '2015/09/11 taskplan check値の判断処理追加
                        Else '2015/09/11 taskplan check値の判断処理追加

                            Dim val2 = dateTimePicker.Text.ToString.Trim()

                            If "".Equals(val2) And flg = 1 Then
                                dict.Add(cname, "")
                            Else
                                ' Dim dateTimePicker As DateTimePicker = CType(control, DateTimePicker)
                                dict.Add(cname, dateTimePicker.Value)
                            End If

                        End If '2015/09/11 taskplan check値の判断処理追加

                    End If
                End If
            End If
        Catch ex As Exception
            Throw ex
        Finally

        End Try
        Return Nothing
    End Function

#End Region

    Private Shared Function GetControlValue(ByVal control As Control) As Object

        If control.Tag Is Nothing Then
            Return control.Text
        End If

        Select Case control.Tag.ToString().ToUpper()
            Case "NUMBER"
                Return Convert.ToDecimal(control.Text)
            Case Else
                Return control.Text
        End Select
    End Function

    Public Shared Function GetDataReaderValues(ByVal dataReader As OracleDataReader) As Dictionary(Of String, String)
        Dim data As Dictionary(Of String, String) = New Dictionary(Of String, String)
        If dataReader.HasRows Then
            Dim fcount = dataReader.FieldCount - 1
            For i = 0 To fcount
                Dim o = dataReader.GetValue(i)
                Dim name = dataReader.GetName(i).ToLower()

                If name.Equals("modified_date") Then
                    Continue For
                End If

                If o.GetType() Is Nothing Then
                    Continue For
                ElseIf o.GetType() Is GetType(Byte()) Then
                    Dim bytes As Byte() = o
                    If (Not bytes Is Nothing) And bytes.Length > 1 Then
                        If name.Equals("image") Then
                            Dim imagePath = Path.Combine(Ini.ReadValue(Ini.Config.DB, "Setting", "TEMPPATH"), Ini.ReadValue(Ini.Config.DB, "Setting", "IMAGENAME"))
                            data.Add(name, imagePath)
                            Continue For
                        ElseIf name.Equals("certificate") Then
                            'Dim certificatePath = Path.Combine(Ini.ReadValue(Ini.Config.DB, "Setting", "TEMPPATH"), Ini.ReadValue(Ini.Config.DB, "Setting", "CERTIFICATE"))
                            'data.Add(name, certificatePath)
                            'Continue For
                        End If
                    End If
                    data.Add(name, "")
                    Continue For
                End If
                data.Add(name, Convert.ToString(dataReader.GetValue(i)))
            Next
        End If
        Return data
    End Function

    Public Shared Function IsDataChanged(ByVal main As Main, ByVal source As Dictionary(Of String, Object), ByVal target As Dictionary(Of String, String)) As Boolean
        Dim isChanged As Boolean = False
        For Each pair In target
            If "modified_by".Equals(pair.Key) Then
                Continue For
            End If
            Try
                If pair.Key.Equals("image") Then
                    If Not main.M_HEADER_IMAGE.ImageLocation.ToLower.Equals(pair.Value.ToLower) Then
                        Return True
                    End If
                    Continue For
                End If
                If pair.Key.Equals("certificate_name") Then
                    If Not main.M_HEADER_CERTIFICATE_NAME.Text.ToLower.Equals(pair.Value.ToLower) Then
                        Return True
                    End If
                    Continue For
                End If
                If Not source.ContainsKey(pair.Key) Then
                    Continue For
                End If
                If source.ContainsKey(pair.Key) And pair.Value.Equals(Convert.ToString(source.Item(pair.Key))) Then
                    Continue For
                End If
            Catch ex As Exception
                Helper.MsgBoxAndLog(Ini.ReadValue(Ini.Config.LANG, "System", "E00001"), ex)
            End Try
            Return True
        Next
        ''image
        'If target.ContainsKey("image") Then
        '    Dim imagePath = Path.Combine(Ini.ReadValue(Ini.Config.DB, "Setting", "TEMPPATH"), Ini.ReadValue(Ini.Config.DB, "Setting", "IMAGENAME")).ToLower
        '    If Not imagePath.Equals(main.M_HEADER_IMAGE.ImageLocation.ToLower) Then
        '        Return True
        '    End If
        'End If
        ''certificate
        'If target.ContainsKey("certificate") Then
        '    Dim certificatePath = Path.Combine(Ini.ReadValue(Ini.Config.DB, "Setting", "TEMPPATH"), Ini.ReadValue(Ini.Config.DB, "Setting", "CERTIFICATE")).ToLower
        '    If Not certificatePath.Equals(main.M_HEADER_CERTIFICATE.Text.ToLower) Then
        '        Return True
        '    End If
        'End If

        Return isChanged
    End Function


    Public Shared Function SaveImageFile(ByVal blob As OracleBlob) As String
        Return SaveFile(blob, Ini.ReadValue(Ini.Config.DB, "Setting", "IMAGENAME"))
    End Function

    'Public Shared Function SavePdfFile(ByVal blob As OracleBlob) As String
    '    Return SaveFile(blob, Ini.ReadValue(Ini.Config.DB, "Setting", "CERTIFICATE"))
    'End Function

    Public Shared Function DownloadFile(ByVal blob As Byte(), ByVal file_name As String) As Boolean
        Try
            If Not blob Is Nothing And blob.Length > 1 Then
                Dim filePath As String = ""
                Dim saveFileDialog As New SaveFileDialog()
                saveFileDialog.FileName = IO.Path.GetFileName(file_name)
                If saveFileDialog.ShowDialog() = DialogResult.OK Then
                    filePath = saveFileDialog.FileName
                End If
                If filePath = String.Empty Then
                    Return False
                End If

                IO.File.WriteAllBytes(filePath, blob)
                Return True
            End If
            Return False
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    
    Public Shared Function SaveFile(ByVal blob As OracleBlob, ByVal fileName As String) As String
        If Not blob Is OracleBlob.Null And Not blob Is Nothing And blob.Length > 1 Then
            Dim filePath As String = Ini.ReadValue(Ini.Config.DB, "Setting", "TEMPPATH")
            If Not Directory.Exists(filePath) Then
                Directory.CreateDirectory(filePath)
            End If
            Dim buffer As Byte() = New Byte(blob.Length - 1) {}
            blob.Read(buffer, 0, buffer.Length)
            Dim fPath = Path.Combine(filePath, fileName)
            File.WriteAllBytes(fPath, buffer)
            Return fPath
        End If
        Return ""
    End Function

    '
    '   メッセージログの出力
    '
    Public Shared Sub Log(ByVal msg As String, Optional ByVal ex As Exception = Nothing)

        Try
            '   出力パスを作成
            Dim temp As String = Ini.ReadValue(Ini.Config.DB, "Setting", "TEMPPATH")

            ' nothing
            Dim log As String = temp & "\" & "sna-ems"
            Try
                ' ログフォルダを作成
                If Not Directory.Exists(log) Then
                    System.IO.Directory.CreateDirectory(log)
                End If
            Catch
                log = GetUserTempPath() & "\" & "sna-ems"
                If Not Directory.Exists(log) Then
                    System.IO.Directory.CreateDirectory(log)
                End If
            End Try


            ' ログファイル名作成
            Dim time As DateTime = Now
            Dim logPath As String = log & "\system" & time.ToString("dd") & ".log"

            '１ヶ月分のログファイルしか保存しないようにする
            '   Dim delete As String = log & "\system" & time.AddDays(1).ToString("dd") & ".log"
            '   System.IO.File.Delete(delete)

            ' ログ出力文字列作成
            Dim text As String
            text = time.ToString("yyyy/MM/dd HH:mm:ss") & vbTab & msg
            If ex Is Nothing = False Then
                text = text & vbCrLf & ex.ToString
            End If

            ' Shift-JISでログ出力
            Try
                File.AppendAllText(logPath, text, System.Text.Encoding.GetEncoding("Shift-JIS"))
            Catch ex2 As Exception
            Finally
            End Try

        Catch ex2 As Exception
        End Try

    End Sub

    Public Shared Function MsgBoxAndLog(ByVal msg As String, Optional ByVal ex As Exception = Nothing) As DialogResult
        Try
            Helper.Log(msg, ex)
            Return MsgBox(msg & Environment.NewLine & Environment.NewLine & "詳細：" & ex.Message, MsgBoxStyle.Critical, "Error")
        Catch ex2 As Exception
        End Try
        Return Nothing
    End Function

    Public Shared Function CheckContentLength(ByVal main As Main, ByVal msg As String, Optional ByVal ex As Exception = Nothing) As Boolean
        Dim showMsg As String = ""
        Try
            Dim err As String
            err = ex.Message
            If err.Contains("ORA-12899") Then
                Dim Sid As String = Ini.ReadValue(Ini.Config.DB, "Setting", "SID")
                Dim str = """."""
                Dim errMsgArr() As String = err.Split(str)
                If errMsgArr.Length >= 7 And errMsgArr(1).ToLower = Sid.ToLower Then
                    Dim tableName = errMsgArr(3)
                    Dim column = errMsgArr(5)
                    Dim tableNameText As String = Ini.ReadValue(Ini.Config.UX, "Setting", tableName)
                    Dim columnText As String = Ini.ReadValue(Ini.Config.UX, "Setting", column)

                    '入力項目が20半角文字を超えてはいけません
                    'ORA-12899: 列 "GSE"."M_HEADER"."MEISYOU" 的值太大 (实际值: 27, 最大值: 20) errMsgArr(6)
                    showMsg = "入力項目(" + tableNameText + "タブの「" + columnText + "」)" +
                        err.Substring(err.LastIndexOf(":") + 1, err.LastIndexOf(")") - err.LastIndexOf(":") - 1) + "半角文字を超えてはいけません"

                    ' select tab
                    main.TabControl1.SelectTab("lbl_" + tableName)

                    MsgBox(msg & Environment.NewLine & Environment.NewLine & "詳細：" & showMsg, MsgBoxStyle.Information, "Warning")
                    Return True
                End If
            ElseIf err.Contains("ORA-00001") Then
                If err.ToLower.Contains("kanri_no") Then
                    ' select tab  
                    main.TabControl1.SelectTab("lbl_M_HEADER")
                    MsgBox(Ini.ReadValue(Ini.Config.LANG, "Main", "M02106"), MsgBoxStyle.Information, "Warning")
                    Return True
                End If
              
            End If
        Catch ex2 As Exception
        End Try
        Return False
    End Function

    Public Shared Function GetUserTempPath() As String
        Dim windir As String, tmppath As String
        windir = Environ("windir") 'Temporary Internet Files 
        tmppath = Mid$(windir, 1, 2) & Environ("homepath") & "\"
        Return tmppath
    End Function


    Public Shared Function IsDecimal(ByVal text As String) As Boolean
        Try
            If text Is Nothing Or text = "" Then
                Return False
            End If
            Convert.ToDecimal(text)
            Return True
        Catch ex As Exception
            Return False
        End Try

    End Function
End Class

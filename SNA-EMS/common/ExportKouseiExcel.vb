﻿Imports System.Text
Imports System.IO
Imports System.Drawing.Imaging
Imports System.Data
Imports System.Threading

Public Class ExportKouseiExcel
    Private kousei As Kousei
    Private filePath As String
    Private template As String
    Private xlsFilePath As String
    Private isNoData As Boolean = True

    Private selected_KUBUN As String

    '
    '   クラスの生成処理
    '
    Public Sub New(ByVal kousei As Kousei)
        Me.kousei = kousei
        InitData()
    End Sub

    '
    '   情報の初期化処理
    '
    Private Sub InitData()
        filePath = Ini.ReadValue(Ini.Config.DB, "Setting", "TEMPPATH")
        template = Ini.ReadValue(Ini.Config.DB, "Setting", "KOUSEI_KANRI") '校正計測器管理帳票.xls
    End Sub

    '
    '   情報の出力処理
    '
    Public Sub Export()
        'write data into new file,save the file
        Dim xlApp As New Microsoft.Office.Interop.Excel.Application
        Dim xlBook As Microsoft.Office.Interop.Excel.Workbook = Nothing

        Try
            'choose path to save document
            Dim buffer As Byte() = Nothing
            Dim saveFileDialog As New SaveFileDialog()
            saveFileDialog.Filter = "Excel(.xls)|*.xls"
            If saveFileDialog.ShowDialog() = DialogResult.OK Then
                xlsFilePath = saveFileDialog.FileName
            End If
            If xlsFilePath = String.Empty Then
                Return
            End If

            kousei.pgs.Visible = True
            kousei.pgs.Value = 0
            kousei.pgs.Value += 5

            'copy template file to the choose path 
            FileCopy(String.Format("{0}\config\", Application.StartupPath()) + template, xlsFilePath)
            kousei.pgs.Value += 5
            xlApp = CreateObject("Excel.Application")
            xlApp.Visible = False
            kousei.pgs.Value += 5
            xlBook = xlApp.Workbooks.Open(xlsFilePath)    'open template file

            isNoData = True
            '===========
            kousei.pgs.Value += 15
            kousei.pgs.Value += 25
            kousei.pgs.Value += 10
            kousei.pgs.Value += 5
            kousei.pgs.Value += 5
            Exp_KOUSEI_KANRI(xlApp)
            kousei.pgs.Value += 5
            kousei.pgs.Value += 5
            kousei.pgs.Value += 5
            kousei.pgs.Value += 3
            kousei.pgs.Value += 2
            kousei.pgs.Value += 3
            kousei.pgs.Value += 2
            '===========

            xlBook.Save()
            kousei.pgs.Value = 100

            kousei.pgs.Visible = False
            kousei.pgs.Value = 0
            If isNoData = True Then
                MsgBox(Ini.ReadValue(Ini.Config.LANG, "Main", "M02112"), MsgBoxStyle.Information, "Information")
                If Not xlBook Is Nothing Then
                    xlBook.Close()
                End If
                If Not xlApp Is Nothing Then
                    xlApp.Workbooks.Close()
                End If
                xlBook = Nothing
                xlApp = Nothing
                File.Delete(xlsFilePath)
            Else
                MsgBox(Ini.ReadValue(Ini.Config.LANG, "Main", "M02107"), MsgBoxStyle.Information, "Information")
            End If
        Catch ex As Exception
            Throw ex
        Finally
            If Not xlBook Is Nothing Then
                xlBook.Close()
            End If
            If Not xlApp Is Nothing Then
                xlApp.Workbooks.Close()
            End If
            xlBook = Nothing
            xlApp = Nothing
            kousei.pgs.Visible = False
            kousei.pgs.Value = 0
        End Try
    End Sub

    '
    '   情報の出力処理（校正計測器管理帳票）
    '
    Private Sub Exp_KOUSEI_KANRI(ByRef xlApp As Microsoft.Office.Interop.Excel.Application)
        Dim xlSheet As Microsoft.Office.Interop.Excel.Worksheet
        Try
            xlSheet = xlApp.ActiveSheet
            xlSheet.Cells(5, 3).Value = Me.kousei.M_HEADER_MEISYOU.Text '名称
            xlSheet.Cells(5, 9).Value = Me.kousei.M_HEADER_BUNRUI_01.Text '管理番号：分類番号
            xlSheet.Cells(6, 3).Value = Me.kousei.M_HEADER_MODEL.Text '型式
            xlSheet.Cells(6, 9).Value = Me.kousei.M_HEADER_MFG_NO.Text '製造番号
            xlSheet.Cells(7, 3).Value = Me.kousei.M_PME_KOUSEI_SEIDO.Text '計測範囲/精度
            xlSheet.Cells(7, 9).Value = Me.kousei.M_HEADER_MFG.Text '製造者名
            xlSheet.Cells(8, 3).Value = Me.kousei.M_HEADER_CREATED_DATE.Text '登録年月日
            xlSheet.Cells(8, 9).Value = Me.kousei.M_PME_KOUSEI_INTERVAL.Text '校正間隔
            xlSheet.Cells(9, 3).Value = Me.kousei.txt_kousei_company '校正委託先
            xlSheet.Cells(9, 9).Value = Me.kousei.txt_nouryoku '委託先選定区分
            xlSheet.Cells(10, 3).Value = Me.kousei.txt_remark '備考

            Dim cnt As Integer = kousei.M_KOUSEI_KANRI_DataGridView.Rows.Count
            If cnt > 0 Then
                isNoData = False

                xlSheet.Cells(10, 9).Value = Helper.DB_NullOrEmpty(kousei.M_KOUSEI_KANRI_DataGridView.Rows(0).Cells("DG_DUE_DATE").Value) '次回有効期限

                Dim DataArray(65000, 28) As Object
                For index = 0 To cnt - 1
                    DataArray(index, 0) = Helper.DB_NullOrEmpty(kousei.M_KOUSEI_KANRI_DataGridView.Rows(index).Cells("DG_KOUSEI_DATE").Value) '校正実施日
                    DataArray(index, 1) = Helper.DB_NullOrEmpty(kousei.M_KOUSEI_KANRI_DataGridView.Rows(index).Cells("DG_KOUSEI_COMPANY").Value) '校正会社
                    DataArray(index, 3) = Helper.DB_NullOrEmpty(kousei.M_KOUSEI_KANRI_DataGridView.Rows(index).Cells("DG_JURYOU_CHECK_DATE").Value) '受領検査日
                    DataArray(index, 4) = Helper.DB_NullOrEmpty(kousei.M_KOUSEI_KANRI_DataGridView.Rows(index).Cells("DG_JURYOU_KEKKA").Value) '合否
                    DataArray(index, 5) = Helper.DB_NullOrEmpty(kousei.M_KOUSEI_KANRI_DataGridView.Rows(index).Cells("DG_JURYOU_KOUSEI_BY").Value) '実施者
                    DataArray(index, 6) = Helper.DB_NullOrEmpty(kousei.M_KOUSEI_KANRI_DataGridView.Rows(index).Cells("DG_TENKEN_CHECK_DATE").Value) '点検検査日
                    ' DataArray(index, 7) = Helper.DB_NullOrEmpty(kousei.M_KOUSEI_KANRI_DataGridView.Rows(index).Cells("DG_TENKEN_KEKKA").Value) '合否
                    DataArray(index, 7) = Helper.DB_NullOrEmpty(kousei.M_KOUSEI_KANRI_DataGridView.Rows(index).Cells("DG_TENKEN_KOUSEI_BY").Value) '実施者
                    DataArray(index, 9) = Helper.DB_NullOrEmpty(kousei.M_KOUSEI_KANRI_DataGridView.Rows(index).Cells("DG_DUE_DATE").Value) '有効期限
                    DataArray(index, 10) = Helper.DB_NullOrEmpty(kousei.M_KOUSEI_KANRI_DataGridView.Rows(index).Cells("DG_TENKEN_REMARKS").Value) '備考
                Next
                xlSheet.Range("A17").Resize(cnt, 11).Value = DataArray
                xlSheet = Nothing
            End If
        Catch ex As Exception
            Throw ex
        Finally
        End Try
    End Sub

End Class

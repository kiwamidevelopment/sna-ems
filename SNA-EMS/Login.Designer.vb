﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
<Global.System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1726")> _
Partial Class Login
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub
    Friend WithEvents lbl_User As System.Windows.Forms.Label
    Friend WithEvents lbl_Password As System.Windows.Forms.Label
    Friend WithEvents UserName_TextBox As System.Windows.Forms.TextBox
    Friend WithEvents Password_TextBox As System.Windows.Forms.TextBox
    Friend WithEvents btn_Close As System.Windows.Forms.Button
    Friend WithEvents btn_Login As System.Windows.Forms.Button

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Login))
        Me.lbl_User = New System.Windows.Forms.Label()
        Me.lbl_Password = New System.Windows.Forms.Label()
        Me.UserName_TextBox = New System.Windows.Forms.TextBox()
        Me.Password_TextBox = New System.Windows.Forms.TextBox()
        Me.btn_Close = New System.Windows.Forms.Button()
        Me.btn_Login = New System.Windows.Forms.Button()
        Me.lbl_SYSNAME = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.backgroundWorker = New System.ComponentModel.BackgroundWorker()
        Me.AX_KOSE_Combo = New System.Windows.Forms.ComboBox()
        Me.lbl_AX_KOSE = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'lbl_User
        '
        Me.lbl_User.Location = New System.Drawing.Point(13, 87)
        Me.lbl_User.Name = "lbl_User"
        Me.lbl_User.Size = New System.Drawing.Size(80, 23)
        Me.lbl_User.TabIndex = 0
        Me.lbl_User.Text = "ユーザー"
        Me.lbl_User.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lbl_Password
        '
        Me.lbl_Password.Location = New System.Drawing.Point(13, 113)
        Me.lbl_Password.Name = "lbl_Password"
        Me.lbl_Password.Size = New System.Drawing.Size(80, 23)
        Me.lbl_Password.TabIndex = 2
        Me.lbl_Password.Text = "パスワード"
        Me.lbl_Password.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'UserName_TextBox
        '
        Me.UserName_TextBox.Location = New System.Drawing.Point(102, 88)
        Me.UserName_TextBox.Name = "UserName_TextBox"
        Me.UserName_TextBox.Size = New System.Drawing.Size(153, 19)
        Me.UserName_TextBox.TabIndex = 1
        '
        'Password_TextBox
        '
        Me.Password_TextBox.Location = New System.Drawing.Point(102, 114)
        Me.Password_TextBox.Name = "Password_TextBox"
        Me.Password_TextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.Password_TextBox.Size = New System.Drawing.Size(153, 19)
        Me.Password_TextBox.TabIndex = 2
        '
        'btn_Close
        '
        Me.btn_Close.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_Close.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btn_Close.Location = New System.Drawing.Point(26, 155)
        Me.btn_Close.Name = "btn_Close"
        Me.btn_Close.Size = New System.Drawing.Size(65, 21)
        Me.btn_Close.TabIndex = 4
        Me.btn_Close.Text = "閉じる"
        '
        'btn_Login
        '
        Me.btn_Login.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_Login.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btn_Login.Location = New System.Drawing.Point(226, 155)
        Me.btn_Login.Name = "btn_Login"
        Me.btn_Login.Size = New System.Drawing.Size(65, 21)
        Me.btn_Login.TabIndex = 3
        Me.btn_Login.Text = "ログイン"
        '
        'lbl_SYSNAME
        '
        Me.lbl_SYSNAME.Font = New System.Drawing.Font("MS PGothic", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lbl_SYSNAME.Location = New System.Drawing.Point(26, 17)
        Me.lbl_SYSNAME.Name = "lbl_SYSNAME"
        Me.lbl_SYSNAME.Size = New System.Drawing.Size(240, 31)
        Me.lbl_SYSNAME.TabIndex = 7
        '
        'Label3
        '
        Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label3.Location = New System.Drawing.Point(0, 142)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(315, 3)
        Me.Label3.TabIndex = 8
        '
        'backgroundWorker
        '
        '
        'AX_KOSE_Combo
        '
        Me.AX_KOSE_Combo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.AX_KOSE_Combo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.AX_KOSE_Combo.FormattingEnabled = True
        Me.AX_KOSE_Combo.Location = New System.Drawing.Point(102, 63)
        Me.AX_KOSE_Combo.Name = "AX_KOSE_Combo"
        Me.AX_KOSE_Combo.Size = New System.Drawing.Size(153, 20)
        Me.AX_KOSE_Combo.TabIndex = 1013
        '
        'lbl_AX_KOSE
        '
        Me.lbl_AX_KOSE.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl_AX_KOSE.Location = New System.Drawing.Point(27, 63)
        Me.lbl_AX_KOSE.Name = "lbl_AX_KOSE"
        Me.lbl_AX_KOSE.Size = New System.Drawing.Size(66, 20)
        Me.lbl_AX_KOSE.TabIndex = 1014
        Me.lbl_AX_KOSE.Text = "役割"
        Me.lbl_AX_KOSE.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Login
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(313, 188)
        Me.Controls.Add(Me.AX_KOSE_Combo)
        Me.Controls.Add(Me.lbl_AX_KOSE)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.lbl_SYSNAME)
        Me.Controls.Add(Me.btn_Login)
        Me.Controls.Add(Me.btn_Close)
        Me.Controls.Add(Me.Password_TextBox)
        Me.Controls.Add(Me.UserName_TextBox)
        Me.Controls.Add(Me.lbl_Password)
        Me.Controls.Add(Me.lbl_User)
        Me.Font = New System.Drawing.Font("MS PGothic", 9.0!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Login"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "設備管理システム"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lbl_SYSNAME As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents backgroundWorker As System.ComponentModel.BackgroundWorker
    Friend WithEvents AX_KOSE_Combo As System.Windows.Forms.ComboBox
    Friend WithEvents lbl_AX_KOSE As System.Windows.Forms.Label

End Class

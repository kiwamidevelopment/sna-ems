﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Kensaku
	Inherits System.Windows.Forms.Form

	'Form 重写 Dispose，以清理组件列表。
	<System.Diagnostics.DebuggerNonUserCode()>
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		Try
			If disposing AndAlso components IsNot Nothing Then
				components.Dispose()
			End If
		Finally
			MyBase.Dispose(disposing)
		End Try
	End Sub

	'Windows 窗体设计器所必需的
	Private components As System.ComponentModel.IContainer

	'注意: 以下过程是 Windows 窗体设计器所必需的
	'可以使用 Windows 窗体设计器修改它。
	'不要使用代码编辑器修改它。
	<System.Diagnostics.DebuggerStepThrough()>
	Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Kensaku))
        Me.btn_Search = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.btn_New = New System.Windows.Forms.Button()
        Me.btn_Close = New System.Windows.Forms.Button()
        Me.OK = New System.Windows.Forms.Button()
        Me.btn_Clear = New System.Windows.Forms.Button()
        Me.lbl_SEARCH_TYPE = New System.Windows.Forms.Label()
        Me.search_KUBUN = New System.Windows.Forms.ComboBox()
        Me.btn_Print = New System.Windows.Forms.Button()
        Me.pgs = New System.Windows.Forms.ProgressBar()
        Me.lbl_List = New System.Windows.Forms.TabPage()
        Me.M_HEADER_DataGridView = New System.Windows.Forms.DataGridView()
        Me.No = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DG_KANRI_NO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DG_BUNRUI_01 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DG_MEISYOU = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DG_MODEL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DG_KIKAKU = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DG_MFG_NO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DG_CREATED_DATE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DG_BASE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DG_LOCATION = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DG_HAICHI_KIJYUN = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DG_KUBUN_01 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DG_RENTAL_FLG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DG_MFG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DG_SPL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DG_CHECK_INTERVAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DG_CHECK_DATE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DG_NEXT_CHECK_DATE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DG_END_CHECK_DATE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DG_CHECK_COMPANY = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DG_KOUSEI_INTERVAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DG_KOUSEI_COMPANY = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DG_KOUSEI_BY_COMPANY = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DG_KOUSEI_DATE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DG_DUE_DATE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DG_STATUS = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DG_SHISAN_FLG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DG_LEASE_FLG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DG_UN_NO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DG_REMARKS = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TabControl2 = New System.Windows.Forms.TabControl()
        Me.search_DUE_DATE = New System.Windows.Forms.TextBox()
        Me.lbl_DUE_DATE = New System.Windows.Forms.Label()
        Me.search_BASE = New System.Windows.Forms.ComboBox()
        Me.lbl_SEARCH_BASE = New System.Windows.Forms.Label()
        Me.lbl_SEARCH_SELECT = New System.Windows.Forms.Label()
        Me.lbl_HAICHI_KIJYUN = New System.Windows.Forms.Label()
        Me.search_SEARCH_SELECT = New System.Windows.Forms.ComboBox()
        Me.search_HAICHI_KIJYUN = New System.Windows.Forms.ComboBox()
        Me.lbl_END_CHECK_DATE = New System.Windows.Forms.Label()
        Me.search_END_CHECK_DATE = New System.Windows.Forms.TextBox()
        Me.search_WORD = New System.Windows.Forms.TextBox()
        Me.lbl_Word = New System.Windows.Forms.Label()
        Me.btn_HT_List = New System.Windows.Forms.Button()
        Me.btn_Tana = New System.Windows.Forms.Button()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.btn_DelTana = New System.Windows.Forms.Button()
        Me.lbl_List.SuspendLayout()
        CType(Me.M_HEADER_DataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl2.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btn_Search
        '
        Me.btn_Search.Location = New System.Drawing.Point(1119, 9)
        Me.btn_Search.Name = "btn_Search"
        Me.btn_Search.Size = New System.Drawing.Size(58, 21)
        Me.btn_Search.TabIndex = 7
        Me.btn_Search.Text = "検索"
        '
        'Label6
        '
        Me.Label6.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label6.Location = New System.Drawing.Point(0, 551)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(1581, 3)
        Me.Label6.TabIndex = 0
        '
        'btn_New
        '
        Me.btn_New.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btn_New.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btn_New.Location = New System.Drawing.Point(189, 559)
        Me.btn_New.Name = "btn_New"
        Me.btn_New.Size = New System.Drawing.Size(65, 21)
        Me.btn_New.TabIndex = 9
        Me.btn_New.Text = "新規"
        '
        'btn_Close
        '
        Me.btn_Close.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btn_Close.Location = New System.Drawing.Point(12, 559)
        Me.btn_Close.Name = "btn_Close"
        Me.btn_Close.Size = New System.Drawing.Size(65, 21)
        Me.btn_Close.TabIndex = 11
        Me.btn_Close.Text = "閉じる"
        '
        'OK
        '
        Me.OK.Location = New System.Drawing.Point(554, 10)
        Me.OK.Name = "OK"
        Me.OK.Size = New System.Drawing.Size(58, 21)
        Me.OK.TabIndex = 3
        Me.OK.Text = "検索"
        '
        'btn_Clear
        '
        Me.btn_Clear.Location = New System.Drawing.Point(1179, 9)
        Me.btn_Clear.Name = "btn_Clear"
        Me.btn_Clear.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.btn_Clear.Size = New System.Drawing.Size(58, 21)
        Me.btn_Clear.TabIndex = 8
        Me.btn_Clear.Text = "クリア"
        '
        'lbl_SEARCH_TYPE
        '
        Me.lbl_SEARCH_TYPE.Location = New System.Drawing.Point(909, 9)
        Me.lbl_SEARCH_TYPE.Name = "lbl_SEARCH_TYPE"
        Me.lbl_SEARCH_TYPE.Size = New System.Drawing.Size(66, 20)
        Me.lbl_SEARCH_TYPE.TabIndex = 1012
        Me.lbl_SEARCH_TYPE.Text = "台帳タイプ"
        Me.lbl_SEARCH_TYPE.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'search_KUBUN
        '
        Me.search_KUBUN.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.search_KUBUN.FormattingEnabled = True
        Me.search_KUBUN.Location = New System.Drawing.Point(974, 9)
        Me.search_KUBUN.Name = "search_KUBUN"
        Me.search_KUBUN.Size = New System.Drawing.Size(141, 20)
        Me.search_KUBUN.TabIndex = 6
        '
        'btn_Print
        '
        Me.btn_Print.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btn_Print.Location = New System.Drawing.Point(258, 559)
        Me.btn_Print.Name = "btn_Print"
        Me.btn_Print.Size = New System.Drawing.Size(65, 21)
        Me.btn_Print.TabIndex = 10
        Me.btn_Print.Text = "印刷"
        '
        'pgs
        '
        Me.pgs.Location = New System.Drawing.Point(240, 277)
        Me.pgs.MarqueeAnimationSpeed = 1
        Me.pgs.Name = "pgs"
        Me.pgs.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.pgs.Size = New System.Drawing.Size(242, 23)
        Me.pgs.Step = 1
        Me.pgs.TabIndex = 1015
        Me.pgs.Value = 1
        Me.pgs.Visible = False
        '
        'lbl_List
        '
        Me.lbl_List.AutoScroll = True
        Me.lbl_List.Controls.Add(Me.M_HEADER_DataGridView)
        Me.lbl_List.Font = New System.Drawing.Font("ＭＳ Ｐゴシック", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lbl_List.Location = New System.Drawing.Point(4, 4)
        Me.lbl_List.Name = "lbl_List"
        Me.lbl_List.Padding = New System.Windows.Forms.Padding(3)
        Me.lbl_List.Size = New System.Drawing.Size(1217, 486)
        Me.lbl_List.TabIndex = 0
        Me.lbl_List.Text = "  設備一覧  "
        '
        'M_HEADER_DataGridView
        '
        Me.M_HEADER_DataGridView.AllowUserToAddRows = False
        Me.M_HEADER_DataGridView.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.M_HEADER_DataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.M_HEADER_DataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.No, Me.DG_KANRI_NO, Me.DG_BUNRUI_01, Me.DG_MEISYOU, Me.DG_MODEL, Me.DG_KIKAKU, Me.DG_MFG_NO, Me.DG_CREATED_DATE, Me.DG_BASE, Me.DG_LOCATION, Me.DG_HAICHI_KIJYUN, Me.DG_KUBUN_01, Me.DG_RENTAL_FLG, Me.DG_MFG, Me.DG_SPL, Me.DG_CHECK_INTERVAL, Me.DG_CHECK_DATE, Me.DG_NEXT_CHECK_DATE, Me.DG_END_CHECK_DATE, Me.DG_CHECK_COMPANY, Me.DG_KOUSEI_INTERVAL, Me.DG_KOUSEI_COMPANY, Me.DG_KOUSEI_BY_COMPANY, Me.DG_KOUSEI_DATE, Me.DG_DUE_DATE, Me.DG_STATUS, Me.DG_SHISAN_FLG, Me.DG_LEASE_FLG, Me.DG_UN_NO, Me.DG_REMARKS})
        Me.M_HEADER_DataGridView.Location = New System.Drawing.Point(-4, 0)
        Me.M_HEADER_DataGridView.MultiSelect = False
        Me.M_HEADER_DataGridView.Name = "M_HEADER_DataGridView"
        Me.M_HEADER_DataGridView.ReadOnly = True
        Me.M_HEADER_DataGridView.RowHeadersVisible = False
        Me.M_HEADER_DataGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders
        Me.M_HEADER_DataGridView.RowTemplate.Height = 23
        Me.M_HEADER_DataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.M_HEADER_DataGridView.Size = New System.Drawing.Size(1208, 487)
        Me.M_HEADER_DataGridView.TabIndex = 0
        '
        'No
        '
        Me.No.DataPropertyName = "No"
        Me.No.HeaderText = "No"
        Me.No.Name = "No"
        Me.No.ReadOnly = True
        Me.No.Width = 40
        '
        'DG_KANRI_NO
        '
        Me.DG_KANRI_NO.DataPropertyName = "KANRI_NO"
        Me.DG_KANRI_NO.HeaderText = "管理番号"
        Me.DG_KANRI_NO.Name = "DG_KANRI_NO"
        Me.DG_KANRI_NO.ReadOnly = True
        Me.DG_KANRI_NO.Width = 90
        '
        'DG_BUNRUI_01
        '
        Me.DG_BUNRUI_01.DataPropertyName = "BUNRUI_01"
        Me.DG_BUNRUI_01.HeaderText = "分類番号"
        Me.DG_BUNRUI_01.Name = "DG_BUNRUI_01"
        Me.DG_BUNRUI_01.ReadOnly = True
        Me.DG_BUNRUI_01.Width = 90
        '
        'DG_MEISYOU
        '
        Me.DG_MEISYOU.DataPropertyName = "MEISYOU"
        Me.DG_MEISYOU.HeaderText = "名称"
        Me.DG_MEISYOU.Name = "DG_MEISYOU"
        Me.DG_MEISYOU.ReadOnly = True
        '
        'DG_MODEL
        '
        Me.DG_MODEL.DataPropertyName = "MODEL"
        Me.DG_MODEL.HeaderText = "型式"
        Me.DG_MODEL.Name = "DG_MODEL"
        Me.DG_MODEL.ReadOnly = True
        '
        'DG_KIKAKU
        '
        Me.DG_KIKAKU.DataPropertyName = "KIKAKU"
        Me.DG_KIKAKU.HeaderText = "規格"
        Me.DG_KIKAKU.Name = "DG_KIKAKU"
        Me.DG_KIKAKU.ReadOnly = True
        Me.DG_KIKAKU.Width = 90
        '
        'DG_MFG_NO
        '
        Me.DG_MFG_NO.DataPropertyName = "MFG_NO"
        Me.DG_MFG_NO.HeaderText = "製造番号"
        Me.DG_MFG_NO.Name = "DG_MFG_NO"
        Me.DG_MFG_NO.ReadOnly = True
        '
        'DG_CREATED_DATE
        '
        Me.DG_CREATED_DATE.DataPropertyName = "CREATED_DATE"
        Me.DG_CREATED_DATE.HeaderText = "登録日"
        Me.DG_CREATED_DATE.Name = "DG_CREATED_DATE"
        Me.DG_CREATED_DATE.ReadOnly = True
        '
        'DG_BASE
        '
        Me.DG_BASE.DataPropertyName = "BASE"
        Me.DG_BASE.HeaderText = "設置場所"
        Me.DG_BASE.Name = "DG_BASE"
        Me.DG_BASE.ReadOnly = True
        '
        'DG_LOCATION
        '
        Me.DG_LOCATION.DataPropertyName = "LOCATION"
        Me.DG_LOCATION.HeaderText = "ロケーション"
        Me.DG_LOCATION.Name = "DG_LOCATION"
        Me.DG_LOCATION.ReadOnly = True
        '
        'DG_HAICHI_KIJYUN
        '
        Me.DG_HAICHI_KIJYUN.DataPropertyName = "HAICHI_KIJYUN"
        Me.DG_HAICHI_KIJYUN.HeaderText = "配置基準"
        Me.DG_HAICHI_KIJYUN.Name = "DG_HAICHI_KIJYUN"
        Me.DG_HAICHI_KIJYUN.ReadOnly = True
        Me.DG_HAICHI_KIJYUN.Width = 90
        '
        'DG_KUBUN_01
        '
        Me.DG_KUBUN_01.DataPropertyName = "KUBUN_01"
        Me.DG_KUBUN_01.HeaderText = "区分"
        Me.DG_KUBUN_01.Name = "DG_KUBUN_01"
        Me.DG_KUBUN_01.ReadOnly = True
        Me.DG_KUBUN_01.Width = 90
        '
        'DG_RENTAL_FLG
        '
        Me.DG_RENTAL_FLG.DataPropertyName = "RENTAL_FLG"
        Me.DG_RENTAL_FLG.HeaderText = "借用設備"
        Me.DG_RENTAL_FLG.Name = "DG_RENTAL_FLG"
        Me.DG_RENTAL_FLG.ReadOnly = True
        Me.DG_RENTAL_FLG.Visible = False
        '
        'DG_MFG
        '
        Me.DG_MFG.DataPropertyName = "MFG"
        Me.DG_MFG.HeaderText = "製造会社"
        Me.DG_MFG.Name = "DG_MFG"
        Me.DG_MFG.ReadOnly = True
        Me.DG_MFG.Visible = False
        '
        'DG_SPL
        '
        Me.DG_SPL.DataPropertyName = "SPL"
        Me.DG_SPL.HeaderText = "購入会社"
        Me.DG_SPL.Name = "DG_SPL"
        Me.DG_SPL.ReadOnly = True
        Me.DG_SPL.Visible = False
        '
        'DG_CHECK_INTERVAL
        '
        Me.DG_CHECK_INTERVAL.DataPropertyName = "CHECK_INTERVAL"
        Me.DG_CHECK_INTERVAL.HeaderText = "点検間隔"
        Me.DG_CHECK_INTERVAL.Name = "DG_CHECK_INTERVAL"
        Me.DG_CHECK_INTERVAL.ReadOnly = True
        Me.DG_CHECK_INTERVAL.Visible = False
        '
        'DG_CHECK_DATE
        '
        Me.DG_CHECK_DATE.DataPropertyName = "CHECK_DATE"
        Me.DG_CHECK_DATE.HeaderText = "点検実施日"
        Me.DG_CHECK_DATE.Name = "DG_CHECK_DATE"
        Me.DG_CHECK_DATE.ReadOnly = True
        Me.DG_CHECK_DATE.Visible = False
        '
        'DG_NEXT_CHECK_DATE
        '
        Me.DG_NEXT_CHECK_DATE.DataPropertyName = "NEXT_CHECK_DATE"
        Me.DG_NEXT_CHECK_DATE.HeaderText = "次回点検月"
        Me.DG_NEXT_CHECK_DATE.Name = "DG_NEXT_CHECK_DATE"
        Me.DG_NEXT_CHECK_DATE.ReadOnly = True
        Me.DG_NEXT_CHECK_DATE.Visible = False
        '
        'DG_END_CHECK_DATE
        '
        Me.DG_END_CHECK_DATE.DataPropertyName = "END_CHECK_DATE"
        Me.DG_END_CHECK_DATE.HeaderText = "点検期限"
        Me.DG_END_CHECK_DATE.Name = "DG_END_CHECK_DATE"
        Me.DG_END_CHECK_DATE.ReadOnly = True
        Me.DG_END_CHECK_DATE.Visible = False
        '
        'DG_CHECK_COMPANY
        '
        Me.DG_CHECK_COMPANY.DataPropertyName = "CHECK_COMPANY"
        Me.DG_CHECK_COMPANY.HeaderText = "点検会社"
        Me.DG_CHECK_COMPANY.Name = "DG_CHECK_COMPANY"
        Me.DG_CHECK_COMPANY.ReadOnly = True
        Me.DG_CHECK_COMPANY.Visible = False
        '
        'DG_KOUSEI_INTERVAL
        '
        Me.DG_KOUSEI_INTERVAL.DataPropertyName = "KOUSEI_INTERVAL"
        Me.DG_KOUSEI_INTERVAL.HeaderText = "校正間隔"
        Me.DG_KOUSEI_INTERVAL.Name = "DG_KOUSEI_INTERVAL"
        Me.DG_KOUSEI_INTERVAL.ReadOnly = True
        Me.DG_KOUSEI_INTERVAL.Visible = False
        '
        'DG_KOUSEI_COMPANY
        '
        Me.DG_KOUSEI_COMPANY.DataPropertyName = "KOUSEI_COMPANY"
        Me.DG_KOUSEI_COMPANY.HeaderText = "校正会社"
        Me.DG_KOUSEI_COMPANY.Name = "DG_KOUSEI_COMPANY"
        Me.DG_KOUSEI_COMPANY.ReadOnly = True
        Me.DG_KOUSEI_COMPANY.Visible = False
        '
        'DG_KOUSEI_BY_COMPANY
        '
        Me.DG_KOUSEI_BY_COMPANY.DataPropertyName = "KOUSEI_BY_COMPANY"
        Me.DG_KOUSEI_BY_COMPANY.HeaderText = "校正仲介会社"
        Me.DG_KOUSEI_BY_COMPANY.Name = "DG_KOUSEI_BY_COMPANY"
        Me.DG_KOUSEI_BY_COMPANY.ReadOnly = True
        Me.DG_KOUSEI_BY_COMPANY.Visible = False
        '
        'DG_KOUSEI_DATE
        '
        Me.DG_KOUSEI_DATE.DataPropertyName = "KOUSEI_DATE"
        Me.DG_KOUSEI_DATE.HeaderText = "最終校正日"
        Me.DG_KOUSEI_DATE.Name = "DG_KOUSEI_DATE"
        Me.DG_KOUSEI_DATE.ReadOnly = True
        Me.DG_KOUSEI_DATE.Visible = False
        '
        'DG_DUE_DATE
        '
        Me.DG_DUE_DATE.DataPropertyName = "DUE_DATE"
        Me.DG_DUE_DATE.HeaderText = "有効期限"
        Me.DG_DUE_DATE.Name = "DG_DUE_DATE"
        Me.DG_DUE_DATE.ReadOnly = True
        Me.DG_DUE_DATE.Visible = False
        '
        'DG_STATUS
        '
        Me.DG_STATUS.DataPropertyName = "STATUS"
        Me.DG_STATUS.HeaderText = "STATUS"
        Me.DG_STATUS.Name = "DG_STATUS"
        Me.DG_STATUS.ReadOnly = True
        '
        'DG_SHISAN_FLG
        '
        Me.DG_SHISAN_FLG.DataPropertyName = "SHISAN_FLG"
        Me.DG_SHISAN_FLG.HeaderText = "固定資産"
        Me.DG_SHISAN_FLG.Name = "DG_SHISAN_FLG"
        Me.DG_SHISAN_FLG.ReadOnly = True
        Me.DG_SHISAN_FLG.Visible = False
        Me.DG_SHISAN_FLG.Width = 5
        '
        'DG_LEASE_FLG
        '
        Me.DG_LEASE_FLG.DataPropertyName = "LEASE_FLG"
        Me.DG_LEASE_FLG.HeaderText = "リース"
        Me.DG_LEASE_FLG.Name = "DG_LEASE_FLG"
        Me.DG_LEASE_FLG.ReadOnly = True
        Me.DG_LEASE_FLG.Visible = False
        Me.DG_LEASE_FLG.Width = 5
        '
        'DG_UN_NO
        '
        Me.DG_UN_NO.DataPropertyName = "UN_NO"
        Me.DG_UN_NO.HeaderText = "危険物"
        Me.DG_UN_NO.Name = "DG_UN_NO"
        Me.DG_UN_NO.ReadOnly = True
        Me.DG_UN_NO.Visible = False
        Me.DG_UN_NO.Width = 5
        '
        'DG_REMARKS
        '
        Me.DG_REMARKS.DataPropertyName = "REMARKS"
        Me.DG_REMARKS.HeaderText = "備考"
        Me.DG_REMARKS.Name = "DG_REMARKS"
        Me.DG_REMARKS.ReadOnly = True
        Me.DG_REMARKS.Visible = False
        Me.DG_REMARKS.Width = 5
        '
        'TabControl2
        '
        Me.TabControl2.Alignment = System.Windows.Forms.TabAlignment.Bottom
        Me.TabControl2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.TabControl2.Controls.Add(Me.lbl_List)
        Me.TabControl2.Location = New System.Drawing.Point(12, 36)
        Me.TabControl2.Name = "TabControl2"
        Me.TabControl2.SelectedIndex = 0
        Me.TabControl2.Size = New System.Drawing.Size(1225, 512)
        Me.TabControl2.SizeMode = System.Windows.Forms.TabSizeMode.Fixed
        Me.TabControl2.TabIndex = 8
        '
        'search_DUE_DATE
        '
        Me.search_DUE_DATE.Location = New System.Drawing.Point(827, 9)
        Me.search_DUE_DATE.MaxLength = 8
        Me.search_DUE_DATE.Name = "search_DUE_DATE"
        Me.search_DUE_DATE.Size = New System.Drawing.Size(84, 19)
        Me.search_DUE_DATE.TabIndex = 5
        '
        'lbl_DUE_DATE
        '
        Me.lbl_DUE_DATE.Location = New System.Drawing.Point(769, 9)
        Me.lbl_DUE_DATE.Name = "lbl_DUE_DATE"
        Me.lbl_DUE_DATE.Size = New System.Drawing.Size(57, 23)
        Me.lbl_DUE_DATE.TabIndex = 1017
        Me.lbl_DUE_DATE.Text = "有効期限"
        Me.lbl_DUE_DATE.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'search_BASE
        '
        Me.search_BASE.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.search_BASE.FormattingEnabled = True
        Me.search_BASE.Location = New System.Drawing.Point(435, 9)
        Me.search_BASE.Name = "search_BASE"
        Me.search_BASE.Size = New System.Drawing.Size(63, 20)
        Me.search_BASE.TabIndex = 4
        '
        'lbl_SEARCH_BASE
        '
        Me.lbl_SEARCH_BASE.Location = New System.Drawing.Point(375, 9)
        Me.lbl_SEARCH_BASE.Name = "lbl_SEARCH_BASE"
        Me.lbl_SEARCH_BASE.Size = New System.Drawing.Size(59, 23)
        Me.lbl_SEARCH_BASE.TabIndex = 1019
        Me.lbl_SEARCH_BASE.Text = "配置基地"
        Me.lbl_SEARCH_BASE.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lbl_SEARCH_SELECT
        '
        Me.lbl_SEARCH_SELECT.Location = New System.Drawing.Point(226, 9)
        Me.lbl_SEARCH_SELECT.Name = "lbl_SEARCH_SELECT"
        Me.lbl_SEARCH_SELECT.Size = New System.Drawing.Size(55, 23)
        Me.lbl_SEARCH_SELECT.TabIndex = 1022
        Me.lbl_SEARCH_SELECT.Text = "検索項目"
        Me.lbl_SEARCH_SELECT.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lbl_HAICHI_KIJYUN
        '
        Me.lbl_HAICHI_KIJYUN.Location = New System.Drawing.Point(501, 9)
        Me.lbl_HAICHI_KIJYUN.Name = "lbl_HAICHI_KIJYUN"
        Me.lbl_HAICHI_KIJYUN.Size = New System.Drawing.Size(56, 23)
        Me.lbl_HAICHI_KIJYUN.TabIndex = 1023
        Me.lbl_HAICHI_KIJYUN.Text = "配置基準"
        Me.lbl_HAICHI_KIJYUN.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'search_SEARCH_SELECT
        '
        Me.search_SEARCH_SELECT.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.search_SEARCH_SELECT.FormattingEnabled = True
        Me.search_SEARCH_SELECT.Location = New System.Drawing.Point(279, 9)
        Me.search_SEARCH_SELECT.Name = "search_SEARCH_SELECT"
        Me.search_SEARCH_SELECT.Size = New System.Drawing.Size(92, 20)
        Me.search_SEARCH_SELECT.TabIndex = 1024
        '
        'search_HAICHI_KIJYUN
        '
        Me.search_HAICHI_KIJYUN.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.search_HAICHI_KIJYUN.FormattingEnabled = True
        Me.search_HAICHI_KIJYUN.Location = New System.Drawing.Point(558, 9)
        Me.search_HAICHI_KIJYUN.Name = "search_HAICHI_KIJYUN"
        Me.search_HAICHI_KIJYUN.Size = New System.Drawing.Size(46, 20)
        Me.search_HAICHI_KIJYUN.TabIndex = 1025
        '
        'lbl_END_CHECK_DATE
        '
        Me.lbl_END_CHECK_DATE.Location = New System.Drawing.Point(616, 9)
        Me.lbl_END_CHECK_DATE.Name = "lbl_END_CHECK_DATE"
        Me.lbl_END_CHECK_DATE.Size = New System.Drawing.Size(57, 23)
        Me.lbl_END_CHECK_DATE.TabIndex = 1027
        Me.lbl_END_CHECK_DATE.Text = "点検期限"
        Me.lbl_END_CHECK_DATE.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'search_END_CHECK_DATE
        '
        Me.search_END_CHECK_DATE.Location = New System.Drawing.Point(673, 9)
        Me.search_END_CHECK_DATE.MaxLength = 8
        Me.search_END_CHECK_DATE.Name = "search_END_CHECK_DATE"
        Me.search_END_CHECK_DATE.Size = New System.Drawing.Size(84, 19)
        Me.search_END_CHECK_DATE.TabIndex = 1028
        '
        'search_WORD
        '
        Me.search_WORD.Location = New System.Drawing.Point(48, 9)
        Me.search_WORD.MaxLength = 100
        Me.search_WORD.Name = "search_WORD"
        Me.search_WORD.Size = New System.Drawing.Size(171, 19)
        Me.search_WORD.TabIndex = 1029
        Me.search_WORD.Tag = ""
        '
        'lbl_Word
        '
        Me.lbl_Word.Location = New System.Drawing.Point(5, 8)
        Me.lbl_Word.Name = "lbl_Word"
        Me.lbl_Word.Size = New System.Drawing.Size(41, 23)
        Me.lbl_Word.TabIndex = 1030
        Me.lbl_Word.Text = "検索窓"
        Me.lbl_Word.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btn_HT_List
        '
        Me.btn_HT_List.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btn_HT_List.Location = New System.Drawing.Point(83, 559)
        Me.btn_HT_List.Name = "btn_HT_List"
        Me.btn_HT_List.Size = New System.Drawing.Size(100, 21)
        Me.btn_HT_List.TabIndex = 9
        Me.btn_HT_List.Text = "貸出状況一覧"
        '
        'btn_Tana
        '
        Me.btn_Tana.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btn_Tana.Location = New System.Drawing.Point(334, 559)
        Me.btn_Tana.Name = "btn_Tana"
        Me.btn_Tana.Size = New System.Drawing.Size(100, 21)
        Me.btn_Tana.TabIndex = 1031
        Me.btn_Tana.Text = "棚卸結果"
        '
        'DataGridView1
        '
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(923, 557)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.RowTemplate.Height = 21
        Me.DataGridView1.Size = New System.Drawing.Size(142, 22)
        Me.DataGridView1.TabIndex = 1032
        Me.DataGridView1.Visible = False
        '
        'btn_DelTana
        '
        Me.btn_DelTana.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btn_DelTana.Location = New System.Drawing.Point(442, 557)
        Me.btn_DelTana.Name = "btn_DelTana"
        Me.btn_DelTana.Size = New System.Drawing.Size(115, 23)
        Me.btn_DelTana.TabIndex = 1033
        Me.btn_DelTana.Text = "棚卸データ削除"
        Me.btn_DelTana.UseVisualStyleBackColor = True
        '
        'Kensaku
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1278, 587)
        Me.Controls.Add(Me.btn_DelTana)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.btn_Tana)
        Me.Controls.Add(Me.btn_HT_List)
        Me.Controls.Add(Me.lbl_Word)
        Me.Controls.Add(Me.search_WORD)
        Me.Controls.Add(Me.search_END_CHECK_DATE)
        Me.Controls.Add(Me.lbl_END_CHECK_DATE)
        Me.Controls.Add(Me.search_HAICHI_KIJYUN)
        Me.Controls.Add(Me.search_SEARCH_SELECT)
        Me.Controls.Add(Me.lbl_HAICHI_KIJYUN)
        Me.Controls.Add(Me.lbl_SEARCH_SELECT)
        Me.Controls.Add(Me.search_BASE)
        Me.Controls.Add(Me.lbl_SEARCH_BASE)
        Me.Controls.Add(Me.lbl_DUE_DATE)
        Me.Controls.Add(Me.search_DUE_DATE)
        Me.Controls.Add(Me.pgs)
        Me.Controls.Add(Me.btn_Print)
        Me.Controls.Add(Me.search_KUBUN)
        Me.Controls.Add(Me.lbl_SEARCH_TYPE)
        Me.Controls.Add(Me.btn_Clear)
        Me.Controls.Add(Me.TabControl2)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.btn_New)
        Me.Controls.Add(Me.btn_Close)
        Me.Controls.Add(Me.btn_Search)
        Me.Font = New System.Drawing.Font("ＭＳ Ｐゴシック", 9.0!)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(902, 400)
        Me.Name = "Kensaku"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "設備管理システム"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.lbl_List.ResumeLayout(False)
        CType(Me.M_HEADER_DataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl2.ResumeLayout(False)
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btn_Search As System.Windows.Forms.Button
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents btn_New As System.Windows.Forms.Button
    Friend WithEvents btn_Close As System.Windows.Forms.Button
    Friend WithEvents OK As System.Windows.Forms.Button
    Friend WithEvents btn_Clear As System.Windows.Forms.Button

    Friend WithEvents lbl_SEARCH_TYPE As System.Windows.Forms.Label
    Friend WithEvents search_KUBUN As System.Windows.Forms.ComboBox
    Friend WithEvents btn_Print As System.Windows.Forms.Button
    Friend WithEvents pgs As System.Windows.Forms.ProgressBar
    Friend WithEvents lbl_List As System.Windows.Forms.TabPage
    Friend WithEvents M_HEADER_DataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents TabControl2 As System.Windows.Forms.TabControl
    Friend WithEvents search_DUE_DATE As System.Windows.Forms.TextBox
    Friend WithEvents lbl_DUE_DATE As System.Windows.Forms.Label
    Friend WithEvents search_BASE As System.Windows.Forms.ComboBox
    Friend WithEvents lbl_SEARCH_BASE As System.Windows.Forms.Label
    Friend WithEvents lbl_SEARCH_SELECT As System.Windows.Forms.Label
    Friend WithEvents lbl_HAICHI_KIJYUN As System.Windows.Forms.Label
    Friend WithEvents search_SEARCH_SELECT As System.Windows.Forms.ComboBox
    Friend WithEvents search_HAICHI_KIJYUN As System.Windows.Forms.ComboBox
    Friend WithEvents lbl_END_CHECK_DATE As System.Windows.Forms.Label
    Friend WithEvents search_END_CHECK_DATE As System.Windows.Forms.TextBox
    Friend WithEvents search_WORD As System.Windows.Forms.TextBox
    Friend WithEvents lbl_Word As System.Windows.Forms.Label
    Friend WithEvents btn_HT_List As System.Windows.Forms.Button
    Friend WithEvents No As DataGridViewTextBoxColumn
    Friend WithEvents DG_KANRI_NO As DataGridViewTextBoxColumn
    Friend WithEvents DG_BUNRUI_01 As DataGridViewTextBoxColumn
    Friend WithEvents DG_MEISYOU As DataGridViewTextBoxColumn
    Friend WithEvents DG_MODEL As DataGridViewTextBoxColumn
    Friend WithEvents DG_KIKAKU As DataGridViewTextBoxColumn
    Friend WithEvents DG_MFG_NO As DataGridViewTextBoxColumn
    Friend WithEvents DG_CREATED_DATE As DataGridViewTextBoxColumn
    Friend WithEvents DG_BASE As DataGridViewTextBoxColumn
    Friend WithEvents DG_LOCATION As DataGridViewTextBoxColumn
    Friend WithEvents DG_HAICHI_KIJYUN As DataGridViewTextBoxColumn
    Friend WithEvents DG_KUBUN_01 As DataGridViewTextBoxColumn
    Friend WithEvents DG_RENTAL_FLG As DataGridViewTextBoxColumn
    Friend WithEvents DG_MFG As DataGridViewTextBoxColumn
    Friend WithEvents DG_SPL As DataGridViewTextBoxColumn
    Friend WithEvents DG_CHECK_INTERVAL As DataGridViewTextBoxColumn
    Friend WithEvents DG_CHECK_DATE As DataGridViewTextBoxColumn
    Friend WithEvents DG_NEXT_CHECK_DATE As DataGridViewTextBoxColumn
    Friend WithEvents DG_END_CHECK_DATE As DataGridViewTextBoxColumn
    Friend WithEvents DG_CHECK_COMPANY As DataGridViewTextBoxColumn
    Friend WithEvents DG_KOUSEI_INTERVAL As DataGridViewTextBoxColumn
    Friend WithEvents DG_KOUSEI_COMPANY As DataGridViewTextBoxColumn
    Friend WithEvents DG_KOUSEI_BY_COMPANY As DataGridViewTextBoxColumn
    Friend WithEvents DG_KOUSEI_DATE As DataGridViewTextBoxColumn
    Friend WithEvents DG_DUE_DATE As DataGridViewTextBoxColumn
    Friend WithEvents DG_STATUS As DataGridViewTextBoxColumn
    Friend WithEvents DG_SHISAN_FLG As DataGridViewTextBoxColumn
    Friend WithEvents DG_LEASE_FLG As DataGridViewTextBoxColumn
    Friend WithEvents DG_UN_NO As DataGridViewTextBoxColumn
    Friend WithEvents DG_REMARKS As DataGridViewTextBoxColumn
    Friend WithEvents btn_Tana As Button
    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents btn_DelTana As Button
End Class

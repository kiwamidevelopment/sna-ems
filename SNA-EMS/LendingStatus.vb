﻿Imports System.Data
Imports System.Text

'
'   クラス：HT_RentalList
'   処理　：HT取込データの確認と編集
'
Public Class LendingStatus
	Public kanriNo As String = ""
	Dim str As String = ""
	Dim search_model_text As String = ""
	Dim search_kanri_no_text As String = ""
	Dim search_name As String = ""
	Private haita_flag As Boolean = False
	Private insert_flag As Boolean = False

	Private cacheMain As Dictionary(Of String, Main) = New Dictionary(Of String, Main)

	Public tabPageList As List(Of TabPage) = Nothing
	Private sourceTabsList As List(Of String) = Nothing
	Private showTabList As List(Of String) = Nothing

	Public gid As String = Nothing
	'Private kousei As Kousei

	' 2016-04-07 YAMAOKA EDIT 印刷前チェック START
	Private exsType As Integer = -1   '2015/09/11 taskplan ADD 前回表示種
	' 2016-04-07 YAMAOKA EDIT 印刷前チェック END

	Private dg_REASE_DATE_no As Integer
	Private dg_return_date_no As Integer

    '2018/08/31 データテーブル保持用
    Private lenddt As DataTable

    '
    '   クラスの新規生成処理
    '   
    'Public Sub New(ByVal loginForm As Login)
    Public Sub New()
		'loginForm.Visible = False
		InitializeComponent()
		Me.Text = Ini.ReadValue(Ini.Config.UX, "Setting", "HTNAME")

        '   ユーザー権限の検証
        If LoginRoler.Employee Is Nothing Then
            Me.Close()
            Me.TopMost = False
            Application.ExitThread()
            Return
        End If
        'SetUserRole()
    End Sub

	'
	'   画面情報の初期化処理
	'   
	Private Sub Kensaku_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
		Try
			Helper.SetAllControlText(Me)
			SetDataGridViewText(Me)
			M_HEADER_DataGridView.AutoGenerateColumns = False
			search_BASE.DataSource = ParameterRepository.GetParameters("BASE", Nothing)
			search_BASE.DisplayMember = "PARAMETER_VALUE"
			search_BASE.ValueMember = "PARAMETER_VALUE"

            search_STATUS.DataSource = ParameterRepository.GetParameters("USAGE", "")
            search_STATUS.DisplayMember = "PARAMETER_VALUE"
			search_STATUS.ValueMember = "PARAMETER_VALUE"


			For i As Integer = 0 To M_HEADER_DataGridView.Columns.Count - 1
				If M_HEADER_DataGridView.Columns.Item(i).Name = "DG_REASE_DATE" Then
					dg_REASE_DATE_no = i
				End If
				If M_HEADER_DataGridView.Columns.Item(i).Name = "DG_RETURN_DATE" Then
					dg_return_date_no = i
				End If
			Next

			SearchData(False)


            '2018/08/31 設備管理者の場合のみSTATUS編集可能
            If LoginRoler.Employee.ROLE = "1" Then
                M_HEADER_DataGridView.Columns(10).ReadOnly = False
                btn_RowAdd.Visible = True
                btn_Save.Visible = True
            End If
        Catch ex As Exception
			Helper.Log("Kensaku_Load", ex)
		End Try
	End Sub

	'
	'   システムの終了処理
	'   
	Private Sub Kensaku_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
		'LoginOut Log
		EmployeeRepository.AddLoginLog(LoginLog.LOGIN_OUT)

		Me.Dispose()
	End Sub

	'
	'   システムの終了前処理
	'   
	Private Sub Kensaku_FormClosing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
		'If haita_flag Then
		'	Return
		'End If
		'haita_flag = True
		'Me.Cursor = Cursors.WaitCursor

		'Dim Rtn = MsgBox(Ini.ReadValue(Ini.Config.LANG, "Main", "W02002"), MsgBoxStyle.YesNo, "Warning")
		'If Rtn = 6 Then
		'	e.Cancel = False
		'	Return
		'End If

		'e.Cancel = True
		'haita_flag = False
		'Me.Cursor = Cursors.Default
	End Sub

	'
	'   画面の終了処理   
	'   
	Private Sub btn_Close_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Close.Click
		If haita_flag Then
			Return
		End If
		haita_flag = True
		Me.Cursor = Cursors.WaitCursor

		Dim Rtn = MsgBox(Ini.ReadValue(Ini.Config.LANG, "Main", "W02002"), MsgBoxStyle.YesNo, "Warning")
		If Rtn = 6 Then
			Me.Close()
			Return
		End If

		haita_flag = False
		Me.Cursor = Cursors.Default
	End Sub

	'
	'   設備の検索処理
	'   
	Public Function SearchData(isRefreshDetail As Boolean, Optional KeepRow As Boolean = False) As Integer
		Dim recordCount As Integer = 0
		Try
			If haita_flag Then
				Return -1
			End If
			haita_flag = True

			Me.Cursor = Cursors.WaitCursor

			Dim currow As Integer = -1

			If KeepRow = True Then
				If Not M_HEADER_DataGridView.CurrentRow Is Nothing Then
					Try
						currow = M_HEADER_DataGridView.CurrentRow.Index
					Catch ex As Exception

					End Try
				End If
			Else
				currow = -1
			End If

			'Dim select_index As Integer = search_SEARCH_SELECT.SelectedIndex

			Dim sBase As String = ""
			If Not search_BASE.SelectedValue Is DBNull.Value Then
				sBase = search_BASE.SelectedValue
			End If

			'Dim sHaichi As String = ""
			'If Not search_HAICHI_KIJYUN.SelectedValue Is DBNull.Value Then
			'	sHaichi = search_HAICHI_KIJYUN.SelectedValue
			'End If

			Dim sREASE_FLG As Integer = search_STATUS.SelectedIndex

			'Dim headerTable As DataTable = MasterRepository.GetHeaderByModel(100, search_WORD.Text, select_index, sBase, sHaichi, "", "", sREASE_FLG, search_W_O.Text, search_REASE_DATE.Text, search_SYAIN_CD.Text)
			Dim headerTable As DataTable = MasterRepository.GetLendingStatus(100, sBase, search_SYAIN_CD.Text, search_AcNo.Text, search_STATUS.Text)
            '2018/08/31 検索結果を保持する
            lenddt = headerTable

            If Not headerTable Is Nothing Then
				recordCount = headerTable.Rows.Count
			End If
			'設備一覧
			M_HEADER_DataGridView.DataSource = headerTable
			HideDG()

			'/* 2015/09/15 taskplan ADD
			If exsType <> 0 Then currow = -1

			If KeepRow = True Then
				If currow > -1 AndAlso Me.M_HEADER_DataGridView.RowCount - 1 >= currow Then
					Try
						Me.M_HEADER_DataGridView.CurrentCell = Me.M_HEADER_DataGridView.Item(0, currow)
					Catch ex As Exception

					End Try
				Else
					'                Me.M_HEADER_DataGridView.CurrentCell = Nothing 'クリア表示にカレント行を未選択にしたい場合は有効化
				End If
			End If
			exsType = 0


		Catch ex As Exception
			Helper.MsgBoxAndLog(Ini.ReadValue(Ini.Config.LANG, "System", "E00001"), ex)
		Finally
			haita_flag = False
			Me.Cursor = Cursors.Default
		End Try
		Return recordCount
	End Function

	'
	'   グリッド項目の表示有無設定
	'
	Private Sub HideDG()

		'No.Visible = True                       'No
		DG_KANRI_NO.Visible = True              '管理番号
		DG_BUNRUI_01.Visible = True             '分類番号
		DG_MEISYOU.Visible = True               '名称
		DG_MODEL.Visible = True                 '型式
		DG_KIKAKU.Visible = True                '規格
		'DG_MFG_NO.Visible = True                '製造番号
		'DG_CREATED_DATE.Visible = True          '登録日
		DG_BASE.Visible = True                  '設置場所
		DG_LOCATION.Visible = True              'ロケーション
		'DG_HAICHI_KIJYUN.Visible = True         '配置基準
		'DG_KUBUN_01.Visible = True              '区分
		'DG_RENTAL_FLG.Visible = False           '借用設備
		'DG_MFG.Visible = False                  '製造会社
		'DG_SPL.Visible = False                  '購入会社
		'DG_CHECK_INTERVAL.Visible = False       '点検間隔
		'DG_CHECK_DATE.Visible = False           '点検実施日
		'DG_NEXT_CHECK_DATE.Visible = False      '次回点検月
		'DG_END_CHECK_DATE.Visible = False       '点検期限
		'DG_CHECK_COMPANY.Visible = False        '点検会社
		'DG_KOUSEI_INTERVAL.Visible = False      '校正間隔
		'DG_KOUSEI_COMPANY.Visible = False       '校正会社
		'DG_KOUSEI_BY_COMPANY.Visible = False    '校正仲介会社
		'DG_KOUSEI_DATE.Visible = False          '最終校正日
		'DG_DUE_DATE.Visible = False             '有効期限
		DG_STATUS.Visible = False                'STATUS
		'DG_SHISAN_FLG.Visible = False           '固定資産
		'DG_LEASE_FLG.Visible = False            'リース
		'DG_UN_NO.Visible = False                '危険物
		'DG_REMARKS.Visible = False              '備考

		'DG_REASE_FLG.Visible = True         '貸出状態(0:貸出/1:返却)
		'DG_W_O.Visible = True                   'w/o
		DG_REASE_DATE.Visible = True           '貸出日
        DG_SYAIN_CD.Visible = True              '貸出社員番号
        DG_USAGE.Visible = True
        'DG_RETURN_DATE.Visible = True           '返却日時

        Dim i As Integer = 0
        ''2018/09/04 STATUS以外は読み取り専用にする
        'For Each row As DataGridViewRow In M_HEADER_DataGridView.Rows
        '    'セル操作用カウンタ
        '    i = 0
        '    For Each cell As DataGridViewCell In row.Cells
        '        If i <> 13 Then
        '            cell.ReadOnly = True
        '        End If
        '        i += 1
        '    Next
        'Next

        For Each column As DataGridViewColumn In M_HEADER_DataGridView.Columns
            If i <> 13 Then
                column.ReadOnly = True
            End If
            i += 1
        Next

    End Sub

	'
	'   グリッドのヘッダ名取得
	'
	Private Function SetDataGridViewText(ByRef kensaku As LendingStatus)
		For index = 0 To kensaku.M_HEADER_DataGridView.Columns.Count - 6
			Dim val = Ini.ReadValue(Ini.Config.UX, "Setting", Mid(kensaku.M_HEADER_DataGridView.Columns(index).Name, "DG_".Length + 1))
			If val <> "" Then
				kensaku.M_HEADER_DataGridView.Columns(index).HeaderText = val
			End If
		Next
		Return Nothing
	End Function

	'
	'   グリッドリスト入力チェック
	'
	Private Function M_HEADER_DataGridView_inputCheck() As Boolean

		If M_HEADER_DataGridView.Rows.Count <= 0 Then Return False

		'For i As Integer = 0 To M_HEADER_DataGridView.Rows.Count - 1
		'	If M_HEADER_DataGridView.Rows(i).Cells("DG_INPUT").Value = 1 Then
		'		Return True
		'	End If
		'Next

		Return False

	End Function

    '
    '   セルの値変更時チェック
    '   
    'Private Sub M_HEADER_DataGridView_CellValueChanged(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles M_HEADER_DataGridView.CellValueChanged
    '    '2018/09/06 管理番号変更時に他のデータを引っ張ってきて表示する
    '    If e Is Nothing Then Return
    '    Dim sb As New StringBuilder
    '    Dim dt As New DataTable
    '    Dim orcl As New Oracle()
    '    If e.ColumnIndex = 2 Then
    '        Dim dgv As DataGridView = DirectCast(sender, DataGridView)

    '        '2018/09/06 管理番号をもとにM_HEADERを更新
    '        sb.Clear()
    '        sb.Append(" select * from m_header ")
    '        sb.Append(" where kanri_no = '" & sender.ToString & "'")
    '        dt = orcl.DataTable(sb.ToString)
    '        If dt.Rows.Count > 0 Then
    '            M_HEADER_DataGridView.Rows(e.RowIndex).Cells("DG_BUNRUI_01") = dt.Rows(0)("BUNRUI_01")
    '            M_HEADER_DataGridView.Rows(e.RowIndex).Cells("DG_MEISYOU") = dt.Rows(0)("MEISYOU")
    '            M_HEADER_DataGridView.Rows(e.RowIndex).Cells("DG_MODEL") = dt.Rows(0)("MODEL")
    '            M_HEADER_DataGridView.Rows(e.RowIndex).Cells("DG_KIKAKU") = dt.Rows(0)("KIKAKU")
    '            M_HEADER_DataGridView.Rows(e.RowIndex).Cells("DG_BASE") = dt.Rows(0)("BASE")
    '            M_HEADER_DataGridView.Rows(e.RowIndex).Cells("DG_LOCATION") = dt.Rows(0)("LOCATION")
    '            M_HEADER_DataGridView.Rows(e.RowIndex).Cells("DG_LOCATION") = dt.Rows(0)("LOCATION")
    '        End If
    '    End If

    '    '	If e Is Nothing Then Return
    '    '	If e.RowIndex < 0 Then Return

    '    '	M_HEADER_DataGridView.Rows(e.RowIndex).Cells("No").Style.BackColor = Color.Yellow
    '    '	M_HEADER_DataGridView.Rows(e.RowIndex).Cells(e.ColumnIndex).Style.BackColor = Color.Yellow
    '    '	M_HEADER_DataGridView.Rows(e.RowIndex).Cells("DG_INPUT").Value = 1

    'End Sub

    '
    '   「検索」ボタン
    '   
    Private Sub Search_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Search.Click

		If M_HEADER_DataGridView_inputCheck() = True Then
			If MessageBox.Show(Ini.ReadValue(Ini.Config.LANG, "Main", "W02116"), "yes", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then
				Exit Sub
			End If
		End If

		'If search_WORD.Text.Length > 0 And search_SEARCH_SELECT.SelectedIndex = 0 Then
		'	MsgBox(Ini.ReadValue(Ini.Config.LANG, "Main", "M02141"), MsgBoxStyle.Information, "Warning")
		'	Exit Sub
		'End If
		Dim recordCount As Integer
		recordCount = SearchData(True)

		If recordCount = 0 Then
			MsgBox(Ini.ReadValue(Ini.Config.LANG, "Main", "M02103"), MsgBoxStyle.Information, "Warning")
		End If

	End Sub

	'
	'   「クリア」ボタン
	'   
	Private Sub btn_Clear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Clear.Click

		If M_HEADER_DataGridView_inputCheck() = True Then
			If MessageBox.Show(Ini.ReadValue(Ini.Config.LANG, "Main", "W02116"), "yes", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then
				Exit Sub
			End If
		End If

		Try
			search_BASE.SelectedIndex = 0

			'search_WORD.Clear()
			'search_HAICHI_KIJYUN.SelectedIndex = 0
			'search_SEARCH_SELECT.SelectedIndex = 0

			search_STATUS.SelectedIndex = 0
			'search_W_O.Clear()
			'search_REASE_DATE.Clear()
			search_SYAIN_CD.Clear()

			SearchData(False)
		Catch ex As Exception
			Helper.Log("btn_Clear_Click", ex)
		End Try

	End Sub

	'
	'   「戻る」ボタン
	'   
	Private Sub btn_GoBack_Click(sender As System.Object, e As System.EventArgs) Handles btn_GoBack.Click
		Me.Close()
	End Sub

	'
	'   「更新」ボタン
	'   
	Private Sub btn_Update_Click(sender As System.Object, e As System.EventArgs) Handles btn_Update.Click

		If M_HEADER_DataGridView_inputCheck() = False Then
			MsgBox(Ini.ReadValue(Ini.Config.LANG, "Main", "M02113"), MsgBoxStyle.Information, "Warning")
			Exit Sub
		End If


		' <param name="gid">GID</param>
		' <param name="REASE_FLG">貸出状態(0:貸出/1:返却)</param>
		' <param name="w_o">W/O</param>
		' <param name="REASE_DATE">貸出日</param>
		' <param name="syain_cd">貸出社員番号</param>
		' <param name="return_date">返却日</param>

		Dim gid As String
		'Dim REASE_FLG As String
		Dim w_o As String
		Dim REASE_DATE As String
		Dim syain_cd As String
		Dim return_date As String

		Try
			For i As Integer = 0 To M_HEADER_DataGridView.Rows.Count - 1
				If M_HEADER_DataGridView.Rows(i).Cells("DG_INPUT").Value = 1 Then

					'gid
					If IsDBNull(M_HEADER_DataGridView.Rows(i).Cells("DG_GID").Value) = False Then
						gid = M_HEADER_DataGridView.Rows(i).Cells("DG_GID").Value
					Else
						Exit For
					End If

					'REASE_FLG
					'If IsDBNull(M_HEADER_DataGridView.Rows(i).Cells("DG_REASE_FLG").Value) = False Then
					'	REASE_FLG = IIf(M_HEADER_DataGridView.Rows(i).Cells("DG_REASE_FLG").Value = "貸出", "0", "1")
					'Else
					'	REASE_FLG = ""
					'End If

					'w_o
					If IsDBNull(M_HEADER_DataGridView.Rows(i).Cells("DG_W_O").Value) = False Then
						w_o = M_HEADER_DataGridView.Rows(i).Cells("DG_W_O").Value
					Else
						w_o = ""
					End If

					'REASE_DATE
					If IsDBNull(M_HEADER_DataGridView.Rows(i).Cells("DG_REASE_DATE").Value) = False Then
						REASE_DATE = M_HEADER_DataGridView.Rows(i).Cells("DG_REASE_DATE").Value.ToString
					Else
						REASE_DATE = ""
					End If

					'syain_cd
					If IsNumeric(M_HEADER_DataGridView.Rows(i).Cells("DG_SYAIN_CD").Value) = True Then
						syain_cd = M_HEADER_DataGridView.Rows(i).Cells("DG_SYAIN_CD").Value
					Else
						syain_cd = ""
					End If

					'return_date
					If IsDBNull(M_HEADER_DataGridView.Rows(i).Cells("DG_RETURN_DATE").Value) = False Then
						return_date = M_HEADER_DataGridView.Rows(i).Cells("DG_RETURN_DATE").Value.ToString
					Else
						return_date = ""
					End If

					'If ReaseStatusHelper.UpdateHTData(gid, REASE_FLG, w_o, REASE_DATE, syain_cd, return_date) = False Then
					'	Exit Sub
					'End If
				End If
			Next

			MsgBox(Ini.ReadValue(Ini.Config.LANG, "Main", "M02109"), MsgBoxStyle.Information, "Information")
			SearchData(True)

		Catch ex As Exception

		End Try

	End Sub

	'
	'   「削除」ボタン
	'
	Private Sub btn_Del_Click(sender As System.Object, e As System.EventArgs) Handles btn_Del.Click

		If M_HEADER_DataGridView.Rows.Count <= 0 Then Exit Sub
		If M_HEADER_DataGridView.SelectedRows.Count <= 0 Then Exit Sub

		If MessageBox.Show(Ini.ReadValue(Ini.Config.LANG, "Main", "M02118"), "yes", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then '削除確認

			Try
				Dim gid As String
				If IsDBNull(M_HEADER_DataGridView.SelectedRows(0).Cells("LEND_NO").Value) = False Then
					gid = M_HEADER_DataGridView.SelectedRows(0).Cells("LEND_NO").Value
				Else
					Exit Sub
				End If

				If ReaseStatusHelper.DeleteHTData(gid) = False Then
					Exit Sub
				End If

				SearchData(True)
			Catch ex As Exception

			End Try


		End If

	End Sub

	'
	'日付入力時値編集
	'
	Private Sub M_HEADER_DataGridView_CellParsing(sender As System.Object, e As System.Windows.Forms.DataGridViewCellParsingEventArgs) Handles M_HEADER_DataGridView.CellParsing
		If e.ColumnIndex = dg_REASE_DATE_no Or e.ColumnIndex = dg_return_date_no Then
			If IsDate(e.Value) = False Then
				If SetDateformat(IIf(e.ColumnIndex = dg_REASE_DATE_no, 0, 1), e.Value) = True Then
					e.ParsingApplied = True
					M_HEADER_DataGridView.Rows(e.RowIndex).Cells(e.ColumnIndex).Value = CDate(e.Value)
				Else
					M_HEADER_DataGridView.Rows(e.RowIndex).Cells(e.ColumnIndex).Value = DBNull.Value
				End If
			End If
		End If
	End Sub

	'
	'日付入力時エラーメッセージ
	'
	Private Sub M_HEADER_DataGridView_DataError(sender As System.Object, e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles M_HEADER_DataGridView.DataError
		If (e.ColumnIndex = dg_REASE_DATE_no Or e.ColumnIndex = dg_return_date_no) And Not (e.Exception Is Nothing) Then
			If IsDBNull(M_HEADER_DataGridView.Rows(e.RowIndex).Cells(e.ColumnIndex).Value) = True Then
				MsgBox(Ini.ReadValue(Ini.Config.LANG, "Main", IIf(e.ColumnIndex = dg_REASE_DATE_no, "M02123", "M02124")), MsgBoxStyle.Information, "Warning")
			Else
				e.Cancel = False
			End If
		End If
	End Sub

	'
	'日付型入力フォーマット編集
	'
	Private Function SetDateformat(formatType As Integer, ByRef word As Object) As Boolean

		Try
			Select Case formatType
				Case 0
					If word.ToString.Length = 8 Then
						word = Format(CInt(word), "0000/00/00")
						Return True
					End If
				Case 1
					If word.ToString.Length = 8 Then
						word = Format(CInt(word), "0000/00/00")
						Return True
					ElseIf word.ToString.Length = 12 Then
						word = Format(Int(word), "0000/00/00 00:00")
						Return True
					End If
				Case Else
					word = DBNull.Value
			End Select
		Catch ex As Exception
			word = DBNull.Value
		End Try

		Return False
	End Function



	Private Sub ReaseHistory_Click(sender As System.Object, e As System.EventArgs) Handles btn_LendHistory.Click
        Dim rsh As LendingStatusHistory = New LendingStatusHistory
        'rsh.ShowDialog()
        rsh.Show()
	End Sub

    Private Sub M_HEADER_DataGridView_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles M_HEADER_DataGridView.CellContentClick

    End Sub

    ''' <summary>
    ''' 行追加処理
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btn_RowAdd_Click(sender As Object, e As EventArgs) Handles btn_RowAdd.Click
        '2018/08/31 一行追加
        'M_HEADER_DataGridView.Rows.Add()
        lenddt.Rows.Add()
        M_HEADER_DataGridView.DataSource = lenddt
        '2018/08/31 追加した行は全て編集可能にする
        For Each cell As DataGridViewCell In M_HEADER_DataGridView.Rows(M_HEADER_DataGridView.Rows.Count - 1).Cells
            cell.ReadOnly = False
        Next
    End Sub

    ''' <summary>
    ''' 保存処理
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btn_Save_Click(sender As Object, e As EventArgs) Handles btn_Save.Click
        '2018/08/31 STATUSを反映
        Dim orcl As New Oracle()
        Try

            Dim sb As New StringBuilder
            Dim dt As New DataTable
            Dim dt2 As New DataTable
            Dim dt3 As New DataTable

            'グリッド行数分繰り返し
            For Each row As DataGridViewRow In M_HEADER_DataGridView.Rows
                '2018/09/03 グリッド上には分までしか入力できないので秒を加工
                Dim workdate As String
                workdate = row.Cells("DG_REASE_DATE").Value + ":00"
                Dim workdate2 As String
                workdate2 = row.Cells("DG_REASE_DATE").Value + ":59"
                sb.Clear()
                sb.Append("select tl.lend_no, tl.lent_date, tl.kanri_no, tl.usage, tl.lent_by, mh.EQUIPMENT_FLG, mh.HOZEN_FLG, mh.KOUSEI_FLG from t_lend tl ")
                sb.Append(" left join m_header mh on mh.kanri_no = tl.kanri_no ")
                sb.Append(" where tl.kanri_no = '" & row.Cells("DG_KANRI_NO").Value.ToString & "' ")
                sb.Append(" and tl.lent_date between TO_DATE('" & workdate & "', 'YYYY/MM/DD HH24/MI/SS')  ")
                sb.Append(" and TO_DATE('" & workdate2 & "', 'YYYY/MM/DD HH24/MI/SS')  ")
                sb.Append(" and tl.lend_no = '" & row.Cells("lend_no").Value & "' ")
                dt = orcl.DataTable(sb.ToString)
                '2018/08/31 該当するデータが存在する場合、更新処理を行う
                If dt.Rows.Count > 0 Then
                    '2018/08/31 保全設備、工具、計測器のどれに属するかで更新するテーブルが異なる
                    If dt.Rows(0)("EQUIPMENT_FLG") = "Y" Then
                        '2018/09/03 t_lendと合わせて更新
                        sb.Clear()
                        sb.Append(" update t_lend set usage = '" & row.Cells("DG_USAGE").Value.ToString & "'")
                        sb.Append(" where kanri_no = '" & row.Cells("DG_KANRI_NO").Value.ToString & "' ")
                        If orcl.ExecuteNonQuery(sb.ToString) < 1 Then
                            Throw New Exception("更新に失敗しました。")
                            Exit Sub
                        End If
                        sb.Clear()
                        sb.Append(" update M_TOOL set status = '" & row.Cells("DG_USAGE").Value.ToString & "'")
                        sb.Append(" where kanri_no = '" & row.Cells("DG_KANRI_NO").Value.ToString & "' ")
                        If orcl.ExecuteNonQuery(sb.ToString) < 1 Then
                            Throw New Exception("更新に失敗しました。")
                            Exit Sub
                        End If
                    ElseIf dt.Rows(0)("HOZEN_FLG") = "Y" Then
                        sb.Clear()
                        sb.Append(" update t_lend set usage = '" & row.Cells("DG_USAGE").Value.ToString & "'")
                        sb.Append(" where kanri_no = '" & row.Cells("DG_KANRI_NO").Value.ToString & "' ")
                        If orcl.ExecuteNonQuery(sb.ToString) < 1 Then
                            Throw New Exception("更新に失敗しました。")
                            Exit Sub
                        End If
                        '2018/09/03 t_lendと合わせて更新
                        sb.Clear()
                        sb.Append(" update m_hozen set status = '" & row.Cells("DG_USAGE").Value.ToString & "'")
                        sb.Append(" where kanri_no = '" & row.Cells("DG_KANRI_NO").Value.ToString & "' ")
                        If orcl.ExecuteNonQuery(sb.ToString) < 1 Then
                            Throw New Exception("更新に失敗しました。")
                            Exit Sub
                        End If
                    ElseIf dt.Rows(0)("KOUSEI_FLG") = "Y" Then
                        sb.Clear()
                        sb.Append(" update t_lend set usage = '" & row.Cells("DG_USAGE").Value.ToString & "'")
                        sb.Append(" where kanri_no = '" & row.Cells("DG_KANRI_NO").Value.ToString & "' ")
                        If orcl.ExecuteNonQuery(sb.ToString) < 1 Then
                            Throw New Exception("更新に失敗しました。")
                            Exit Sub
                        End If
                        '2018/09/03 t_lendと合わせて更新
                        sb.Clear()
                        sb.Append(" update m_pme set status = '" & row.Cells("DG_USAGE").Value.ToString & "'")
                        sb.Append(" where kanri_no = '" & row.Cells("DG_KANRI_NO").Value.ToString & "' ")
                        If orcl.ExecuteNonQuery(sb.ToString) < 1 Then
                            Throw New Exception("更新に失敗しました。")
                            Exit Sub
                        End If
                    End If
                Else
                    sb.Clear()
                    sb.Append(" select * from m_header ")
                    sb.Append(" where kanri_no = '" & row.Cells("DG_KANRI_NO").Value.ToString & "' ")
                    dt2 = orcl.DataTable(sb.ToString)
                    '2018/08/31 まずm_headerを確認し対応する管理番号がない場合は処理終了
                    If dt2.Rows.Count > 0 Then
                        '2018/09/03 現在の貸出中DBの管理番号と重複していないか確認
                        sb.Clear()
                        sb.Append(" select t_lend.kanri_no from t_lend ")
                        sb.Append(" INNER Join gse.M_HEADER ")
                        sb.Append(" ON T_LEND.KANRI_NO = M_HEADER.KANRI_NO ")
                        sb.Append(" WHERE T_LEND.RETURNED_DATE IS NULL ")
                        sb.Append(" And M_HEADER.KUBUN_01 <> '車両' ")
                        sb.Append(" And T_LEND.kanri_no = '" & row.Cells("DG_KANRI_NO").Value.ToString & "' ")
                        dt3 = orcl.DataTable(sb.ToString)
                        If dt3.Rows.Count > 0 Then
                            '2018/09/03 重複している場合は追加不可
                            MessageBox.Show("この管理番号は現在返却されていない為使用できません。", "設備管理", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                            Exit Sub
                        End If
                        '2018/09/03 貸出日加工用変数
                        Dim lendno As String
                        Dim lenddatetime As String
                        Dim dttime As Date
                        '2018/09/03 貸出日も入力必須
                        If row.Cells("DG_REASE_DATE").Value Is DBNull.Value Then
                            MessageBox.Show("貸出日が未入力です。", "設備管理", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                            row.Cells("DG_REASE_DATE").Style.BackColor = Color.Red
                            Exit Sub
                        ElseIf Not Date.TryParse(row.Cells("DG_REASE_DATE").Value, dttime) Then
                            MessageBox.Show("貸出日は「yyyy/mm/dd hh:mm」の型式で入力してください。", "設備管理", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                            row.Cells("DG_REASE_DATE").Style.BackColor = Color.Red
                            Exit Sub
                        End If
                        '2018/09/03 lend_noは貸出日から割り出す
                        lendno = dttime.ToString.Replace("/", "")
                        lendno = lendno.Remove(0, 2)
                        lendno = lendno.Replace(":", "")
                        lendno = lendno.Replace(" ", "")
                        If lendno.Length = 11 Then
                            lendno = lendno.Insert(6, "0")
                        End If
                        lenddatetime = dttime.ToString
                        '2018/09/03 insertする
                        sb.Clear()
                        sb.Append(" insert into t_lend ")
                        sb.Append(" ( lend_no, lent_date, kanri_no, usage, lent_by) ")
                        sb.Append(" values('" & lendno & "', ")
                        sb.Append(" TO_DATE('" & lenddatetime & "', 'YYYY/MM/DD HH24/MI/SS') , ")
                        sb.Append(" '" & row.Cells("DG_KANRI_NO").Value & "' , ")
                        sb.Append(" '" & row.Cells("DG_USAGE").Value & "' , ")
                        sb.Append(" '" & row.Cells("DG_SYAIN_CD").Value & "' ) ")
                        If orcl.ExecuteNonQuery(sb.ToString) < 1 Then
                            Throw New Exception("更新に失敗しました。")
                            Exit Sub
                        End If
                        '2018/09/03 t_lendと合わせて更新
                        If dt2.Rows(0)("EQUIPMENT_FLG") = "Y" Then
                            sb.Clear()
                            sb.Append(" update M_TOOL set status = '" & row.Cells("DG_USAGE").Value.ToString & "'")
                            sb.Append(" where kanri_no = '" & row.Cells("DG_KANRI_NO").Value.ToString & "' ")
                            If orcl.ExecuteNonQuery(sb.ToString) < 1 Then
                                Throw New Exception("更新に失敗しました。")
                                Exit Sub
                            End If
                        ElseIf dt2.Rows(0)("HOZEN_FLG") = "Y" Then
                            sb.Clear()
                            sb.Append(" update m_hozen set status = '" & row.Cells("DG_USAGE").Value.ToString & "'")
                            sb.Append(" where kanri_no = '" & row.Cells("DG_KANRI_NO").Value.ToString & "' ")
                            If orcl.ExecuteNonQuery(sb.ToString) < 1 Then
                                Throw New Exception("更新に失敗しました。")
                                Exit Sub
                            End If
                        ElseIf dt2.Rows(0)("KOUSEI_FLG") = "Y" Then
                            sb.Clear()
                            sb.Append(" update m_pme set status = '" & row.Cells("DG_USAGE").Value.ToString & "'")
                            sb.Append(" where kanri_no = '" & row.Cells("DG_KANRI_NO").Value.ToString & "' ")
                            If orcl.ExecuteNonQuery(sb.ToString) < 1 Then
                                Throw New Exception("更新に失敗しました。")
                                Exit Sub
                            End If
                        End If
                    Else
                        MessageBox.Show("管理番号が未入力か、入力された管理番号は存在しません。", "設備管理", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        row.Cells("DG_KANRI_NO").Style.BackColor = Color.Red
                        Exit Sub
                    End If
                End If
            Next
            MessageBox.Show("保存が完了しました！", "設備管理", MessageBoxButtons.OK, MessageBoxIcon.Information)
            '2018/09/04 保存後はデータ再整列する
            SearchData(False)
        Catch ex As Exception
            Helper.MsgBoxAndLog(Ini.ReadValue(Ini.Config.LANG, "System", "E00001"), ex)
        Finally
            orcl.Close()
        End Try


    End Sub
End Class

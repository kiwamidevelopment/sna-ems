﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class LendingStatusHistory
	Inherits System.Windows.Forms.Form

	'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
	<System.Diagnostics.DebuggerNonUserCode()>
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		Try
			If disposing AndAlso components IsNot Nothing Then
				components.Dispose()
			End If
		Finally
			MyBase.Dispose(disposing)
		End Try
	End Sub

	'Windows フォーム デザイナーで必要です。
	Private components As System.ComponentModel.IContainer

	'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
	'Windows フォーム デザイナーを使用して変更できます。  
	'コード エディターを使って変更しないでください。
	<System.Diagnostics.DebuggerStepThrough()>
	Private Sub InitializeComponent()
        Me.dgReaseHistory = New System.Windows.Forms.DataGridView()
        Me.LendDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ReturnDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.KanriNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BunruiNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MEISYOU = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MODEL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Kikaku = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BASE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.wo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LEND_USAGE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LentBy = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ReturnBy = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btn_Search = New System.Windows.Forms.Button()
        Me.btn_Clear = New System.Windows.Forms.Button()
        Me.btn_Export = New System.Windows.Forms.Button()
        Me.lbl_SEARCH_BASE = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.searchKanriNo = New System.Windows.Forms.TextBox()
        Me.searchBunrui = New System.Windows.Forms.TextBox()
        Me.search_BASE = New System.Windows.Forms.ComboBox()
        Me.searchWo = New System.Windows.Forms.TextBox()
        Me.searchName = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.searchTC = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.LendDateFrom = New System.Windows.Forms.DateTimePicker()
        Me.LendDateTo = New System.Windows.Forms.DateTimePicker()
        CType(Me.dgReaseHistory, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgReaseHistory
        '
        Me.dgReaseHistory.AllowUserToAddRows = False
        Me.dgReaseHistory.AllowUserToDeleteRows = False
        Me.dgReaseHistory.AllowUserToResizeColumns = False
        Me.dgReaseHistory.AllowUserToResizeRows = False
        Me.dgReaseHistory.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgReaseHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgReaseHistory.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.LendDate, Me.ReturnDate, Me.KanriNo, Me.BunruiNo, Me.MEISYOU, Me.MODEL, Me.Kikaku, Me.BASE, Me.wo, Me.tc, Me.LEND_USAGE, Me.LentBy, Me.ReturnBy})
        Me.dgReaseHistory.Location = New System.Drawing.Point(12, 62)
        Me.dgReaseHistory.Name = "dgReaseHistory"
        Me.dgReaseHistory.ReadOnly = True
        Me.dgReaseHistory.RowTemplate.Height = 21
        Me.dgReaseHistory.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgReaseHistory.Size = New System.Drawing.Size(1255, 422)
        Me.dgReaseHistory.TabIndex = 0
        '
        'LendDate
        '
        Me.LendDate.DataPropertyName = "LENT_DATE"
        Me.LendDate.HeaderText = "貸出日"
        Me.LendDate.Name = "LendDate"
        Me.LendDate.ReadOnly = True
        '
        'ReturnDate
        '
        Me.ReturnDate.DataPropertyName = "RETURNED_DATE"
        Me.ReturnDate.HeaderText = "返却日"
        Me.ReturnDate.Name = "ReturnDate"
        Me.ReturnDate.ReadOnly = True
        '
        'KanriNo
        '
        Me.KanriNo.DataPropertyName = "KANRI_NO"
        Me.KanriNo.HeaderText = "管理番号"
        Me.KanriNo.Name = "KanriNo"
        Me.KanriNo.ReadOnly = True
        '
        'BunruiNo
        '
        Me.BunruiNo.DataPropertyName = "BUNRUI_01"
        Me.BunruiNo.HeaderText = "分類番号"
        Me.BunruiNo.Name = "BunruiNo"
        Me.BunruiNo.ReadOnly = True
        '
        'MEISYOU
        '
        Me.MEISYOU.DataPropertyName = "MEISYOU"
        Me.MEISYOU.HeaderText = "名称"
        Me.MEISYOU.Name = "MEISYOU"
        Me.MEISYOU.ReadOnly = True
        Me.MEISYOU.Width = 200
        '
        'MODEL
        '
        Me.MODEL.DataPropertyName = "MODEL"
        Me.MODEL.HeaderText = "型式"
        Me.MODEL.Name = "MODEL"
        Me.MODEL.ReadOnly = True
        '
        'Kikaku
        '
        Me.Kikaku.DataPropertyName = "KIKAKU"
        Me.Kikaku.HeaderText = "規格"
        Me.Kikaku.Name = "Kikaku"
        Me.Kikaku.ReadOnly = True
        '
        'BASE
        '
        Me.BASE.DataPropertyName = "BASE"
        Me.BASE.HeaderText = "配置基地"
        Me.BASE.Name = "BASE"
        Me.BASE.ReadOnly = True
        '
        'wo
        '
        Me.wo.DataPropertyName = "WO"
        Me.wo.HeaderText = "W/O"
        Me.wo.Name = "wo"
        Me.wo.ReadOnly = True
        '
        'tc
        '
        Me.tc.DataPropertyName = "tc"
        Me.tc.HeaderText = "T/C"
        Me.tc.Name = "tc"
        Me.tc.ReadOnly = True
        '
        'LEND_USAGE
        '
        Me.LEND_USAGE.DataPropertyName = "USAGE"
        Me.LEND_USAGE.HeaderText = "使用方法"
        Me.LEND_USAGE.Name = "LEND_USAGE"
        Me.LEND_USAGE.ReadOnly = True
        '
        'LentBy
        '
        Me.LentBy.DataPropertyName = "LENT_BY"
        Me.LentBy.HeaderText = "貸出者"
        Me.LentBy.Name = "LentBy"
        Me.LentBy.ReadOnly = True
        '
        'ReturnBy
        '
        Me.ReturnBy.DataPropertyName = "RETURNED_BY"
        Me.ReturnBy.HeaderText = "返却者"
        Me.ReturnBy.Name = "ReturnBy"
        Me.ReturnBy.ReadOnly = True
        Me.ReturnBy.Visible = False
        '
        'btn_Search
        '
        Me.btn_Search.Location = New System.Drawing.Point(924, 30)
        Me.btn_Search.Name = "btn_Search"
        Me.btn_Search.Size = New System.Drawing.Size(77, 26)
        Me.btn_Search.TabIndex = 8
        Me.btn_Search.Text = "検索"
        '
        'btn_Clear
        '
        Me.btn_Clear.Location = New System.Drawing.Point(1023, 30)
        Me.btn_Clear.Name = "btn_Clear"
        Me.btn_Clear.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.btn_Clear.Size = New System.Drawing.Size(71, 26)
        Me.btn_Clear.TabIndex = 1032
        Me.btn_Clear.Text = "クリア"
        '
        'btn_Export
        '
        Me.btn_Export.Location = New System.Drawing.Point(1141, 30)
        Me.btn_Export.Name = "btn_Export"
        Me.btn_Export.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.btn_Export.Size = New System.Drawing.Size(97, 25)
        Me.btn_Export.TabIndex = 1033
        Me.btn_Export.Text = "エクセル出力"
        '
        'lbl_SEARCH_BASE
        '
        Me.lbl_SEARCH_BASE.Location = New System.Drawing.Point(12, 9)
        Me.lbl_SEARCH_BASE.Name = "lbl_SEARCH_BASE"
        Me.lbl_SEARCH_BASE.Size = New System.Drawing.Size(59, 23)
        Me.lbl_SEARCH_BASE.TabIndex = 1034
        Me.lbl_SEARCH_BASE.Text = "管理番号"
        Me.lbl_SEARCH_BASE.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(195, 11)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(59, 23)
        Me.Label1.TabIndex = 1034
        Me.Label1.Text = "分類番号"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(386, 9)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(59, 23)
        Me.Label2.TabIndex = 1034
        Me.Label2.Text = "名称"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(722, 8)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(59, 23)
        Me.Label3.TabIndex = 1034
        Me.Label3.Text = "配置基地"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(-10, 39)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(81, 23)
        Me.Label4.TabIndex = 1034
        Me.Label4.Text = "Work Order"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(364, 39)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(81, 23)
        Me.Label5.TabIndex = 1034
        Me.Label5.Text = "貸出日"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'searchKanriNo
        '
        Me.searchKanriNo.ImeMode = System.Windows.Forms.ImeMode.Disable
        Me.searchKanriNo.Location = New System.Drawing.Point(77, 11)
        Me.searchKanriNo.MaxLength = 5
        Me.searchKanriNo.Name = "searchKanriNo"
        Me.searchKanriNo.Size = New System.Drawing.Size(67, 19)
        Me.searchKanriNo.TabIndex = 1210
        Me.searchKanriNo.Tag = ""
        '
        'searchBunrui
        '
        Me.searchBunrui.ImeMode = System.Windows.Forms.ImeMode.Disable
        Me.searchBunrui.Location = New System.Drawing.Point(260, 13)
        Me.searchBunrui.MaxLength = 5
        Me.searchBunrui.Name = "searchBunrui"
        Me.searchBunrui.Size = New System.Drawing.Size(94, 19)
        Me.searchBunrui.TabIndex = 1210
        Me.searchBunrui.Tag = ""
        '
        'search_BASE
        '
        Me.search_BASE.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.search_BASE.FormattingEnabled = True
        Me.search_BASE.Location = New System.Drawing.Point(787, 9)
        Me.search_BASE.Name = "search_BASE"
        Me.search_BASE.Size = New System.Drawing.Size(66, 20)
        Me.search_BASE.TabIndex = 1211
        '
        'searchWo
        '
        Me.searchWo.ImeMode = System.Windows.Forms.ImeMode.Disable
        Me.searchWo.Location = New System.Drawing.Point(77, 39)
        Me.searchWo.MaxLength = 5
        Me.searchWo.Name = "searchWo"
        Me.searchWo.Size = New System.Drawing.Size(94, 19)
        Me.searchWo.TabIndex = 1210
        Me.searchWo.Tag = ""
        '
        'searchName
        '
        Me.searchName.ImeMode = System.Windows.Forms.ImeMode.Disable
        Me.searchName.Location = New System.Drawing.Point(451, 11)
        Me.searchName.MaxLength = 50
        Me.searchName.Name = "searchName"
        Me.searchName.Size = New System.Drawing.Size(256, 19)
        Me.searchName.TabIndex = 1210
        Me.searchName.Tag = ""
        '
        'Label6
        '
        Me.Label6.Location = New System.Drawing.Point(567, 39)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(31, 23)
        Me.Label6.TabIndex = 1034
        Me.Label6.Text = "～"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'searchTC
        '
        Me.searchTC.ImeMode = System.Windows.Forms.ImeMode.Disable
        Me.searchTC.Location = New System.Drawing.Point(260, 41)
        Me.searchTC.MaxLength = 5
        Me.searchTC.Name = "searchTC"
        Me.searchTC.Size = New System.Drawing.Size(94, 19)
        Me.searchTC.TabIndex = 1214
        Me.searchTC.Tag = ""
        '
        'Label7
        '
        Me.Label7.Location = New System.Drawing.Point(173, 41)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(81, 23)
        Me.Label7.TabIndex = 1213
        Me.Label7.Text = "T/C"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'LendDateFrom
        '
        Me.LendDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.LendDateFrom.Location = New System.Drawing.Point(451, 41)
        Me.LendDateFrom.Name = "LendDateFrom"
        Me.LendDateFrom.Size = New System.Drawing.Size(120, 19)
        Me.LendDateFrom.TabIndex = 1215
        '
        'LendDateTo
        '
        Me.LendDateTo.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.LendDateTo.Location = New System.Drawing.Point(594, 41)
        Me.LendDateTo.Name = "LendDateTo"
        Me.LendDateTo.Size = New System.Drawing.Size(113, 19)
        Me.LendDateTo.TabIndex = 1216
        '
        'LendingStatusHistory
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1284, 568)
        Me.Controls.Add(Me.LendDateTo)
        Me.Controls.Add(Me.LendDateFrom)
        Me.Controls.Add(Me.searchTC)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.search_BASE)
        Me.Controls.Add(Me.searchBunrui)
        Me.Controls.Add(Me.searchName)
        Me.Controls.Add(Me.searchWo)
        Me.Controls.Add(Me.searchKanriNo)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lbl_SEARCH_BASE)
        Me.Controls.Add(Me.btn_Export)
        Me.Controls.Add(Me.btn_Clear)
        Me.Controls.Add(Me.btn_Search)
        Me.Controls.Add(Me.dgReaseHistory)
        Me.Name = "LendingStatusHistory"
        Me.Text = "貸出状況履歴"
        CType(Me.dgReaseHistory, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgReaseHistory As System.Windows.Forms.DataGridView
	Friend WithEvents btn_Search As System.Windows.Forms.Button
	Friend WithEvents btn_Clear As System.Windows.Forms.Button
	Friend WithEvents btn_Export As System.Windows.Forms.Button
	Friend WithEvents lbl_SEARCH_BASE As Label
	Friend WithEvents Label1 As Label
	Friend WithEvents Label2 As Label
	Friend WithEvents Label3 As Label
	Friend WithEvents Label4 As Label
	Friend WithEvents Label5 As Label
	Friend WithEvents searchKanriNo As TextBox
	Friend WithEvents searchBunrui As TextBox
	Friend WithEvents search_BASE As ComboBox
	Friend WithEvents searchWo As TextBox
	Friend WithEvents searchName As TextBox
	Friend WithEvents Label6 As Label
	Friend WithEvents LendDate As DataGridViewTextBoxColumn
	Friend WithEvents ReturnDate As DataGridViewTextBoxColumn
	Friend WithEvents KanriNo As DataGridViewTextBoxColumn
	Friend WithEvents BunruiNo As DataGridViewTextBoxColumn
	Friend WithEvents MEISYOU As DataGridViewTextBoxColumn
	Friend WithEvents MODEL As DataGridViewTextBoxColumn
	Friend WithEvents Kikaku As DataGridViewTextBoxColumn
	Friend WithEvents BASE As DataGridViewTextBoxColumn
	Friend WithEvents wo As DataGridViewTextBoxColumn
	Friend WithEvents tc As DataGridViewTextBoxColumn
	Friend WithEvents LEND_USAGE As DataGridViewTextBoxColumn
	Friend WithEvents LentBy As DataGridViewTextBoxColumn
	Friend WithEvents ReturnBy As DataGridViewTextBoxColumn
	Friend WithEvents searchTC As TextBox
	Friend WithEvents Label7 As Label
	Friend WithEvents LendDateFrom As DateTimePicker
	Friend WithEvents LendDateTo As DateTimePicker
End Class

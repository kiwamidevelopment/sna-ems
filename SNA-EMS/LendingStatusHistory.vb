﻿Imports Microsoft.Office.Interop
Imports System.Runtime.InteropServices
Imports System.Data

Public Class LendingStatusHistory
	Private sql As String
	Private Sub ReaseStatusHistory_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
		dgReaseHistory.AutoGenerateColumns = False
		search_BASE.DataSource = ParameterRepository.GetParameters("BASE", Nothing)
		search_BASE.DisplayMember = "PARAMETER_VALUE"
		search_BASE.ValueMember = "PARAMETER_VALUE"

		'貸出日検索条件の初期値を1年前～当日とする
		LendDateFrom.Value = Date.Today.AddYears(-1)
		LendDateTo.Value = Date.Today

		dgReaseHistory.DataSource = search()

	End Sub

	Private Sub btn_Search_Click(sender As System.Object, e As System.EventArgs) Handles btn_Search.Click
		dgReaseHistory.DataSource = search()
	End Sub

	Private Sub btn_Clear_Click(sender As System.Object, e As System.EventArgs) Handles btn_Clear.Click

		searchKanriNo.Text = ""
		searchBunrui.Text = ""
		searchName.Text = ""
		searchWo.Text = ""
		searchTC.Text = ""
		'貸出日検索条件の初期値を1年前～当日とする
		LendDateFrom.Value = Date.Now.AddYears(-1)
		LendDateTo.Value = Date.Now
		search_BASE.Text = ""

		dgReaseHistory.DataSource = Nothing
	End Sub

	Private Sub btn_Export_Click(sender As System.Object, e As System.EventArgs) Handles btn_Export.Click
		Dim objExcel As Excel.Application = Nothing
		Dim objWorkBook As Excel.Workbook = Nothing
		objExcel = New Excel.Application
		objWorkBook = objExcel.Workbooks.Add

		' DataGridViewのセルのデータ取得
		Dim v As String(,) = New String(
			dgReaseHistory.Rows.Count - 1, dgReaseHistory.Columns.Count - 1) {}
		For r As Integer = 0 To dgReaseHistory.Rows.Count - 1
			For c As Integer = 0 To dgReaseHistory.Columns.Count - 1
				Dim dt As String = ""
				If dgReaseHistory.Rows(r).Cells(c).Value _
					Is Nothing = False Then
					dt = dgReaseHistory.Rows(r).Cells(c).Value.ToString()
				End If
				v(r, c) = dt
			Next
		Next
		' EXCELにデータ転送
		Dim ran As String = "A2:" &
			Chr(Asc("A") + dgReaseHistory.Columns.Count - 1) & dgReaseHistory.Rows.Count
		objWorkBook.Sheets(1).Range(ran) = v

		' エクセル表示
		objExcel.Visible = True

		' EXCEL解放
		Marshal.ReleaseComObject(objWorkBook)
		Marshal.ReleaseComObject(objExcel)
		objWorkBook = Nothing
		objExcel = Nothing
	End Sub


	Private Function search() As DataTable

		Dim headerTable As DataTable = MasterRepository.GetLendingWOStatus(search_BASE.Text, searchKanriNo.Text, searchBunrui.Text, searchName.Text, searchWo.Text, searchTC.Text, LendDateFrom.Value, LendDateTo.Value)
		Return headerTable
	End Function


End Class
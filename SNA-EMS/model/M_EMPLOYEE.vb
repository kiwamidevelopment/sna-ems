﻿'
'   クラス：M_EMPLOYEE
'   処理　：システムの利用者情報
'
Public Class M_EMPLOYEE
    Public Sub New()

    End Sub
    Private _EMP_NO As String
    Public Property EMP_NO() As String
        Get
            Return _EMP_NO
        End Get
        Set(ByVal value As String)
            _EMP_NO = value
        End Set
    End Property

    Private _EMP_NAME As String
    Public Property EMP_NAME() As String
        Get
            Return _EMP_NAME
        End Get
        Set(ByVal value As String)
            _EMP_NAME = value
        End Set
    End Property

    Private _EMP_PSWD As String
    Public Property EMP_PSWD() As String
        Get
            Return _EMP_PSWD
        End Get
        Set(ByVal value As String)
            _EMP_PSWD = value
        End Set
    End Property

    Private _EMP_DESC As String
    Public Property EMP_DESC() As String
        Get
            Return _EMP_DESC
        End Get
        Set(ByVal value As String)
            _EMP_DESC = value
        End Set
    End Property

    '   権限：システム
    '   履歴：2014/08/31 追加 D.LI
    Private _AX_MST As String
    Public Property AX_MST() As String
        Get
            Return _AX_MST
        End Get
        Set(ByVal value As String)
            _AX_MST = value
        End Set
    End Property

    '   権限：校正表
    '   履歴：2014/08/31 追加 D.LI
    Private _AX_VAD As String
    Public Property AX_VAD() As String
        Get
            Return _AX_VAD
        End Get
        Set(ByVal value As String)
            _AX_VAD = value
        End Set
    End Property

    '   権限：帳票印刷
    '   履歴：今後の利用    2014/08/31 追加 D.LI
    Private _AX_PRT As String
    Public Property AX_PRT() As String
        Get
            Return _AX_PRT
        End Get
        Set(ByVal value As String)
            _AX_PRT = value
        End Set
    End Property

    '   権限：帳票表示
    '   履歴：今後の利用    2014/08/31 追加 D.LI
    Private _AX_RPT As String
    Public Property AX_RPT() As String
        Get
            Return _AX_RPT
        End Get
        Set(ByVal value As String)
            _AX_RPT = value
        End Set
    End Property

    '   権限：権限モデル
    '   履歴：今後の利用    2014/11/14 追加 D.LI
    Private _AX_KOSE As String
    Public Property AX_KOSE() As String
        Get
            Return _AX_KOSE
        End Get
        Set(ByVal value As String)
            _AX_KOSE = value
        End Set
    End Property

    '   権限：その他
    '   履歴：今後の利用    2014/08/31 追加 D.LI
    Private _AX_OTH As String
    Public Property AX_OTH() As String
        Get
            Return _AX_OTH
        End Get
        Set(ByVal value As String)
            _AX_OTH = value
        End Set
    End Property

    '   権限：その他
    '   履歴：今後の利用    2014/08/31 追加 D.LI
    Private _ROLE As String
    Public Property ROLE() As String
        Get
            Return _ROLE
        End Get
        Set(ByVal value As String)
            _ROLE = value
        End Set
    End Property

End Class

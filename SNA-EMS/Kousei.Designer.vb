﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Kousei
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Kousei))
        Me.lbl_MEISYOU = New System.Windows.Forms.Label()
        Me.M_HEADER_MEISYOU = New System.Windows.Forms.TextBox()
        Me.lbl_KANRI_NO = New System.Windows.Forms.Label()
        Me.M_HEADER_KANRI_NO = New System.Windows.Forms.TextBox()
        Me.lbl_BUNRUI_01 = New System.Windows.Forms.Label()
        Me.M_HEADER_BUNRUI_01 = New System.Windows.Forms.TextBox()
        Me.M_HEADER_MODEL = New System.Windows.Forms.TextBox()
        Me.lbl_MODEL = New System.Windows.Forms.Label()
        Me.M_HEADER_MFG_NO = New System.Windows.Forms.TextBox()
        Me.lbl_MFG_NO = New System.Windows.Forms.Label()
        Me.M_HEADER_MFG = New System.Windows.Forms.TextBox()
        Me.lbl_MFG = New System.Windows.Forms.Label()
        Me.lbl_ATA = New System.Windows.Forms.Label()
        Me.M_PME_ATA = New System.Windows.Forms.TextBox()
        Me.lbl_CREATED_DATE = New System.Windows.Forms.Label()
        Me.M_PME_KOUSEI_SEIDO = New System.Windows.Forms.TextBox()
        Me.lbl_KOUSEI_SEIDO = New System.Windows.Forms.Label()
        Me.M_PME_KOUSEI_INTERVAL = New System.Windows.Forms.TextBox()
        Me.lbl_KOUSEI_INTERVAL = New System.Windows.Forms.Label()
        Me.lbl_BASE = New System.Windows.Forms.Label()
        Me.M_HEADER_BASE = New System.Windows.Forms.TextBox()
        Me.M_HEADER_CREATED_DATE = New System.Windows.Forms.TextBox()
        Me.btn_Update = New System.Windows.Forms.Button()
        Me.btn_New = New System.Windows.Forms.Button()
        Me.btn_Export = New System.Windows.Forms.Button()
        Me.btn_Del = New System.Windows.Forms.Button()
        Me.M_KOUSEI_KANRI_DataGridView = New System.Windows.Forms.DataGridView()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.pgs = New System.Windows.Forms.ProgressBar()
        Me.btn_GoBack = New System.Windows.Forms.Button()
        Me.CHK = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.DG_KOUSEI_DATE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DG_KOUSEI_COMPANY = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DG_JURYOU_CHECK_DATE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DG_JURYOU_KEKKA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DG_JURYOU_KOUSEI_BY = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DG_JURYOU_REMARKS = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DG_TENKEN_CHECK_DATE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DG_TENKEN_KOUSEI_BY = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DG_DUE_DATE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DG_TENKEN_REMARKS = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DG_CHECK_FILE_NAME = New System.Windows.Forms.DataGridViewLinkColumn()
        Me.DG_MODIFIED_DATE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DG_MODIFIED_BY = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DG_CHECK_FILE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.M_KOUSEI_KANRI_DataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lbl_MEISYOU
        '
        Me.lbl_MEISYOU.Location = New System.Drawing.Point(34, 9)
        Me.lbl_MEISYOU.Name = "lbl_MEISYOU"
        Me.lbl_MEISYOU.Size = New System.Drawing.Size(32, 23)
        Me.lbl_MEISYOU.TabIndex = 32
        Me.lbl_MEISYOU.Text = "名称"
        Me.lbl_MEISYOU.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_HEADER_MEISYOU
        '
        Me.M_HEADER_MEISYOU.Enabled = False
        Me.M_HEADER_MEISYOU.Location = New System.Drawing.Point(70, 10)
        Me.M_HEADER_MEISYOU.MaxLength = 20
        Me.M_HEADER_MEISYOU.Name = "M_HEADER_MEISYOU"
        Me.M_HEADER_MEISYOU.Size = New System.Drawing.Size(643, 19)
        Me.M_HEADER_MEISYOU.TabIndex = 31
        Me.M_HEADER_MEISYOU.Tag = "MEISYOU"
        '
        'lbl_KANRI_NO
        '
        Me.lbl_KANRI_NO.Location = New System.Drawing.Point(9, 35)
        Me.lbl_KANRI_NO.Name = "lbl_KANRI_NO"
        Me.lbl_KANRI_NO.Size = New System.Drawing.Size(57, 23)
        Me.lbl_KANRI_NO.TabIndex = 34
        Me.lbl_KANRI_NO.Text = "管理番号"
        Me.lbl_KANRI_NO.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_HEADER_KANRI_NO
        '
        Me.M_HEADER_KANRI_NO.Enabled = False
        Me.M_HEADER_KANRI_NO.Location = New System.Drawing.Point(70, 36)
        Me.M_HEADER_KANRI_NO.MaxLength = 20
        Me.M_HEADER_KANRI_NO.Name = "M_HEADER_KANRI_NO"
        Me.M_HEADER_KANRI_NO.Size = New System.Drawing.Size(137, 19)
        Me.M_HEADER_KANRI_NO.TabIndex = 33
        Me.M_HEADER_KANRI_NO.Tag = "KANRI_NO"
        '
        'lbl_BUNRUI_01
        '
        Me.lbl_BUNRUI_01.Location = New System.Drawing.Point(209, 38)
        Me.lbl_BUNRUI_01.Name = "lbl_BUNRUI_01"
        Me.lbl_BUNRUI_01.Size = New System.Drawing.Size(59, 17)
        Me.lbl_BUNRUI_01.TabIndex = 216
        Me.lbl_BUNRUI_01.Text = "分類番号"
        Me.lbl_BUNRUI_01.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_HEADER_BUNRUI_01
        '
        Me.M_HEADER_BUNRUI_01.Enabled = False
        Me.M_HEADER_BUNRUI_01.Location = New System.Drawing.Point(270, 36)
        Me.M_HEADER_BUNRUI_01.MaxLength = 20
        Me.M_HEADER_BUNRUI_01.Name = "M_HEADER_BUNRUI_01"
        Me.M_HEADER_BUNRUI_01.Size = New System.Drawing.Size(228, 19)
        Me.M_HEADER_BUNRUI_01.TabIndex = 215
        Me.M_HEADER_BUNRUI_01.Tag = "BUNRUI_01"
        '
        'M_HEADER_MODEL
        '
        Me.M_HEADER_MODEL.Enabled = False
        Me.M_HEADER_MODEL.Location = New System.Drawing.Point(567, 36)
        Me.M_HEADER_MODEL.MaxLength = 30
        Me.M_HEADER_MODEL.Name = "M_HEADER_MODEL"
        Me.M_HEADER_MODEL.Size = New System.Drawing.Size(146, 19)
        Me.M_HEADER_MODEL.TabIndex = 218
        '
        'lbl_MODEL
        '
        Me.lbl_MODEL.Location = New System.Drawing.Point(528, 35)
        Me.lbl_MODEL.Name = "lbl_MODEL"
        Me.lbl_MODEL.Size = New System.Drawing.Size(32, 23)
        Me.lbl_MODEL.TabIndex = 217
        Me.lbl_MODEL.Text = "型式"
        Me.lbl_MODEL.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_HEADER_MFG_NO
        '
        Me.M_HEADER_MFG_NO.Enabled = False
        Me.M_HEADER_MFG_NO.Location = New System.Drawing.Point(567, 61)
        Me.M_HEADER_MFG_NO.MaxLength = 30
        Me.M_HEADER_MFG_NO.Name = "M_HEADER_MFG_NO"
        Me.M_HEADER_MFG_NO.Size = New System.Drawing.Size(146, 19)
        Me.M_HEADER_MFG_NO.TabIndex = 1171
        Me.M_HEADER_MFG_NO.Tag = "BUNRUI_02"
        '
        'lbl_MFG_NO
        '
        Me.lbl_MFG_NO.Location = New System.Drawing.Point(504, 59)
        Me.lbl_MFG_NO.Name = "lbl_MFG_NO"
        Me.lbl_MFG_NO.Size = New System.Drawing.Size(56, 23)
        Me.lbl_MFG_NO.TabIndex = 1169
        Me.lbl_MFG_NO.Text = "製造番号"
        Me.lbl_MFG_NO.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_HEADER_MFG
        '
        Me.M_HEADER_MFG.Enabled = False
        Me.M_HEADER_MFG.Location = New System.Drawing.Point(70, 61)
        Me.M_HEADER_MFG.MaxLength = 50
        Me.M_HEADER_MFG.Name = "M_HEADER_MFG"
        Me.M_HEADER_MFG.Size = New System.Drawing.Size(428, 19)
        Me.M_HEADER_MFG.TabIndex = 1172
        '
        'lbl_MFG
        '
        Me.lbl_MFG.Location = New System.Drawing.Point(1, 59)
        Me.lbl_MFG.Name = "lbl_MFG"
        Me.lbl_MFG.Size = New System.Drawing.Size(65, 23)
        Me.lbl_MFG.TabIndex = 1170
        Me.lbl_MFG.Text = "製造会社"
        Me.lbl_MFG.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lbl_ATA
        '
        Me.lbl_ATA.Location = New System.Drawing.Point(123, 86)
        Me.lbl_ATA.Name = "lbl_ATA"
        Me.lbl_ATA.Size = New System.Drawing.Size(35, 23)
        Me.lbl_ATA.TabIndex = 1173
        Me.lbl_ATA.Text = "ATA"
        Me.lbl_ATA.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_PME_ATA
        '
        Me.M_PME_ATA.Enabled = False
        Me.M_PME_ATA.Location = New System.Drawing.Point(162, 87)
        Me.M_PME_ATA.MaxLength = 2
        Me.M_PME_ATA.Name = "M_PME_ATA"
        Me.M_PME_ATA.Size = New System.Drawing.Size(45, 19)
        Me.M_PME_ATA.TabIndex = 1174
        Me.M_PME_ATA.Tag = "NUMBER"
        Me.M_PME_ATA.Text = "0"
        Me.M_PME_ATA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lbl_CREATED_DATE
        '
        Me.lbl_CREATED_DATE.Location = New System.Drawing.Point(211, 86)
        Me.lbl_CREATED_DATE.Name = "lbl_CREATED_DATE"
        Me.lbl_CREATED_DATE.Size = New System.Drawing.Size(48, 23)
        Me.lbl_CREATED_DATE.TabIndex = 1182
        Me.lbl_CREATED_DATE.Text = "登録日"
        Me.lbl_CREATED_DATE.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_PME_KOUSEI_SEIDO
        '
        Me.M_PME_KOUSEI_SEIDO.Enabled = False
        Me.M_PME_KOUSEI_SEIDO.Location = New System.Drawing.Point(420, 87)
        Me.M_PME_KOUSEI_SEIDO.MaxLength = 20
        Me.M_PME_KOUSEI_SEIDO.Name = "M_PME_KOUSEI_SEIDO"
        Me.M_PME_KOUSEI_SEIDO.Size = New System.Drawing.Size(78, 19)
        Me.M_PME_KOUSEI_SEIDO.TabIndex = 1185
        Me.M_PME_KOUSEI_SEIDO.Tag = ""
        Me.M_PME_KOUSEI_SEIDO.Text = "123.45"
        Me.M_PME_KOUSEI_SEIDO.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lbl_KOUSEI_SEIDO
        '
        Me.lbl_KOUSEI_SEIDO.Location = New System.Drawing.Point(344, 86)
        Me.lbl_KOUSEI_SEIDO.Name = "lbl_KOUSEI_SEIDO"
        Me.lbl_KOUSEI_SEIDO.Size = New System.Drawing.Size(78, 23)
        Me.lbl_KOUSEI_SEIDO.TabIndex = 1184
        Me.lbl_KOUSEI_SEIDO.Text = "校正精度範囲"
        Me.lbl_KOUSEI_SEIDO.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_PME_KOUSEI_INTERVAL
        '
        Me.M_PME_KOUSEI_INTERVAL.Enabled = False
        Me.M_PME_KOUSEI_INTERVAL.Location = New System.Drawing.Point(567, 87)
        Me.M_PME_KOUSEI_INTERVAL.MaxLength = 20
        Me.M_PME_KOUSEI_INTERVAL.Name = "M_PME_KOUSEI_INTERVAL"
        Me.M_PME_KOUSEI_INTERVAL.Size = New System.Drawing.Size(146, 19)
        Me.M_PME_KOUSEI_INTERVAL.TabIndex = 1187
        '
        'lbl_KOUSEI_INTERVAL
        '
        Me.lbl_KOUSEI_INTERVAL.Location = New System.Drawing.Point(504, 86)
        Me.lbl_KOUSEI_INTERVAL.Name = "lbl_KOUSEI_INTERVAL"
        Me.lbl_KOUSEI_INTERVAL.Size = New System.Drawing.Size(55, 23)
        Me.lbl_KOUSEI_INTERVAL.TabIndex = 1186
        Me.lbl_KOUSEI_INTERVAL.Text = "校正間隔"
        Me.lbl_KOUSEI_INTERVAL.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lbl_BASE
        '
        Me.lbl_BASE.Location = New System.Drawing.Point(8, 86)
        Me.lbl_BASE.Name = "lbl_BASE"
        Me.lbl_BASE.Size = New System.Drawing.Size(58, 23)
        Me.lbl_BASE.TabIndex = 1188
        Me.lbl_BASE.Text = "配置基地"
        Me.lbl_BASE.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_HEADER_BASE
        '
        Me.M_HEADER_BASE.Enabled = False
        Me.M_HEADER_BASE.Location = New System.Drawing.Point(70, 87)
        Me.M_HEADER_BASE.MaxLength = 20
        Me.M_HEADER_BASE.Name = "M_HEADER_BASE"
        Me.M_HEADER_BASE.Size = New System.Drawing.Size(52, 19)
        Me.M_HEADER_BASE.TabIndex = 1187
        '
        'M_HEADER_CREATED_DATE
        '
        Me.M_HEADER_CREATED_DATE.Enabled = False
        Me.M_HEADER_CREATED_DATE.Location = New System.Drawing.Point(261, 87)
        Me.M_HEADER_CREATED_DATE.MaxLength = 20
        Me.M_HEADER_CREATED_DATE.Name = "M_HEADER_CREATED_DATE"
        Me.M_HEADER_CREATED_DATE.Size = New System.Drawing.Size(78, 19)
        Me.M_HEADER_CREATED_DATE.TabIndex = 1187
        '
        'btn_Update
        '
        Me.btn_Update.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btn_Update.Location = New System.Drawing.Point(504, 424)
        Me.btn_Update.Name = "btn_Update"
        Me.btn_Update.Size = New System.Drawing.Size(65, 21)
        Me.btn_Update.TabIndex = 1191
        Me.btn_Update.Text = "更新"
        '
        'btn_New
        '
        Me.btn_New.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btn_New.Location = New System.Drawing.Point(432, 424)
        Me.btn_New.Name = "btn_New"
        Me.btn_New.Size = New System.Drawing.Size(65, 21)
        Me.btn_New.TabIndex = 1190
        Me.btn_New.Text = "新規"
        '
        'btn_Export
        '
        Me.btn_Export.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btn_Export.Location = New System.Drawing.Point(648, 424)
        Me.btn_Export.Name = "btn_Export"
        Me.btn_Export.Size = New System.Drawing.Size(65, 21)
        Me.btn_Export.TabIndex = 1193
        Me.btn_Export.Text = "一覧表"
        '
        'btn_Del
        '
        Me.btn_Del.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btn_Del.Location = New System.Drawing.Point(576, 424)
        Me.btn_Del.Name = "btn_Del"
        Me.btn_Del.Size = New System.Drawing.Size(65, 21)
        Me.btn_Del.TabIndex = 1194
        Me.btn_Del.Text = "削除"
        '
        'M_KOUSEI_KANRI_DataGridView
        '
        Me.M_KOUSEI_KANRI_DataGridView.AllowUserToAddRows = False
        Me.M_KOUSEI_KANRI_DataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.M_KOUSEI_KANRI_DataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.CHK, Me.DG_KOUSEI_DATE, Me.DG_KOUSEI_COMPANY, Me.DG_JURYOU_CHECK_DATE, Me.DG_JURYOU_KEKKA, Me.DG_JURYOU_KOUSEI_BY, Me.DG_JURYOU_REMARKS, Me.DG_TENKEN_CHECK_DATE, Me.DG_TENKEN_KOUSEI_BY, Me.DG_DUE_DATE, Me.DG_TENKEN_REMARKS, Me.DG_CHECK_FILE_NAME, Me.DG_MODIFIED_DATE, Me.DG_MODIFIED_BY, Me.SID, Me.DG_CHECK_FILE})
        Me.M_KOUSEI_KANRI_DataGridView.Location = New System.Drawing.Point(12, 114)
        Me.M_KOUSEI_KANRI_DataGridView.MultiSelect = False
        Me.M_KOUSEI_KANRI_DataGridView.Name = "M_KOUSEI_KANRI_DataGridView"
        Me.M_KOUSEI_KANRI_DataGridView.ReadOnly = True
        Me.M_KOUSEI_KANRI_DataGridView.RowHeadersVisible = False
        Me.M_KOUSEI_KANRI_DataGridView.RowTemplate.Height = 23
        Me.M_KOUSEI_KANRI_DataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.M_KOUSEI_KANRI_DataGridView.Size = New System.Drawing.Size(701, 297)
        Me.M_KOUSEI_KANRI_DataGridView.TabIndex = 1195
        '
        'Label6
        '
        Me.Label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label6.Location = New System.Drawing.Point(-6, 414)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(730, 3)
        Me.Label6.TabIndex = 1196
        '
        'pgs
        '
        Me.pgs.Location = New System.Drawing.Point(240, 215)
        Me.pgs.MarqueeAnimationSpeed = 1
        Me.pgs.Name = "pgs"
        Me.pgs.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.pgs.Size = New System.Drawing.Size(242, 23)
        Me.pgs.Step = 1
        Me.pgs.TabIndex = 1197
        Me.pgs.Value = 1
        Me.pgs.Visible = False
        '
        'btn_GoBack
        '
        Me.btn_GoBack.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btn_GoBack.Location = New System.Drawing.Point(11, 424)
        Me.btn_GoBack.Name = "btn_GoBack"
        Me.btn_GoBack.Size = New System.Drawing.Size(65, 21)
        Me.btn_GoBack.TabIndex = 1198
        Me.btn_GoBack.Text = "戻る"
        '
        'CHK
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.NullValue = False
        Me.CHK.DefaultCellStyle = DataGridViewCellStyle1
        Me.CHK.FillWeight = 30.0!
        Me.CHK.HeaderText = ""
        Me.CHK.Name = "CHK"
        Me.CHK.ReadOnly = True
        Me.CHK.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.CHK.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.CHK.Width = 30
        '
        'DG_KOUSEI_DATE
        '
        Me.DG_KOUSEI_DATE.DataPropertyName = "KOUSEI_DATE"
        Me.DG_KOUSEI_DATE.HeaderText = "校正実施日"
        Me.DG_KOUSEI_DATE.Name = "DG_KOUSEI_DATE"
        Me.DG_KOUSEI_DATE.ReadOnly = True
        '
        'DG_KOUSEI_COMPANY
        '
        Me.DG_KOUSEI_COMPANY.DataPropertyName = "KOUSEI_COMPANY"
        Me.DG_KOUSEI_COMPANY.FillWeight = 800.0!
        Me.DG_KOUSEI_COMPANY.HeaderText = "校正会社"
        Me.DG_KOUSEI_COMPANY.Name = "DG_KOUSEI_COMPANY"
        Me.DG_KOUSEI_COMPANY.ReadOnly = True
        Me.DG_KOUSEI_COMPANY.Width = 200
        '
        'DG_JURYOU_CHECK_DATE
        '
        Me.DG_JURYOU_CHECK_DATE.DataPropertyName = "JURYOU_CHECK_DATE"
        Me.DG_JURYOU_CHECK_DATE.HeaderText = "受領検査日"
        Me.DG_JURYOU_CHECK_DATE.Name = "DG_JURYOU_CHECK_DATE"
        Me.DG_JURYOU_CHECK_DATE.ReadOnly = True
        '
        'DG_JURYOU_KEKKA
        '
        Me.DG_JURYOU_KEKKA.DataPropertyName = "JURYOU_KEKKA"
        Me.DG_JURYOU_KEKKA.FillWeight = 60.0!
        Me.DG_JURYOU_KEKKA.HeaderText = "合否"
        Me.DG_JURYOU_KEKKA.Name = "DG_JURYOU_KEKKA"
        Me.DG_JURYOU_KEKKA.ReadOnly = True
        Me.DG_JURYOU_KEKKA.Width = 60
        '
        'DG_JURYOU_KOUSEI_BY
        '
        Me.DG_JURYOU_KOUSEI_BY.DataPropertyName = "JURYOU_KOUSEI_BY"
        Me.DG_JURYOU_KOUSEI_BY.HeaderText = "受領者"
        Me.DG_JURYOU_KOUSEI_BY.Name = "DG_JURYOU_KOUSEI_BY"
        Me.DG_JURYOU_KOUSEI_BY.ReadOnly = True
        '
        'DG_JURYOU_REMARKS
        '
        Me.DG_JURYOU_REMARKS.DataPropertyName = "JURYOU_REMARKS"
        Me.DG_JURYOU_REMARKS.FillWeight = 1000.0!
        Me.DG_JURYOU_REMARKS.HeaderText = "備考"
        Me.DG_JURYOU_REMARKS.Name = "DG_JURYOU_REMARKS"
        Me.DG_JURYOU_REMARKS.ReadOnly = True
        '
        'DG_TENKEN_CHECK_DATE
        '
        Me.DG_TENKEN_CHECK_DATE.DataPropertyName = "TENKEN_CHECK_DATE"
        Me.DG_TENKEN_CHECK_DATE.HeaderText = "点検検査日"
        Me.DG_TENKEN_CHECK_DATE.Name = "DG_TENKEN_CHECK_DATE"
        Me.DG_TENKEN_CHECK_DATE.ReadOnly = True
        '
        'DG_TENKEN_KOUSEI_BY
        '
        Me.DG_TENKEN_KOUSEI_BY.DataPropertyName = "TENKEN_KOUSEI_BY"
        Me.DG_TENKEN_KOUSEI_BY.HeaderText = "点検者"
        Me.DG_TENKEN_KOUSEI_BY.Name = "DG_TENKEN_KOUSEI_BY"
        Me.DG_TENKEN_KOUSEI_BY.ReadOnly = True
        '
        'DG_DUE_DATE
        '
        Me.DG_DUE_DATE.DataPropertyName = "DUE_DATE"
        Me.DG_DUE_DATE.HeaderText = "有効期限"
        Me.DG_DUE_DATE.Name = "DG_DUE_DATE"
        Me.DG_DUE_DATE.ReadOnly = True
        '
        'DG_TENKEN_REMARKS
        '
        Me.DG_TENKEN_REMARKS.DataPropertyName = "TENKEN_REMARKS"
        Me.DG_TENKEN_REMARKS.HeaderText = "備考"
        Me.DG_TENKEN_REMARKS.Name = "DG_TENKEN_REMARKS"
        Me.DG_TENKEN_REMARKS.ReadOnly = True
        '
        'DG_CHECK_FILE_NAME
        '
        Me.DG_CHECK_FILE_NAME.DataPropertyName = "CHECK_FILE_NAME"
        Me.DG_CHECK_FILE_NAME.HeaderText = "書類"
        Me.DG_CHECK_FILE_NAME.Name = "DG_CHECK_FILE_NAME"
        Me.DG_CHECK_FILE_NAME.ReadOnly = True
        Me.DG_CHECK_FILE_NAME.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DG_CHECK_FILE_NAME.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'DG_MODIFIED_DATE
        '
        Me.DG_MODIFIED_DATE.DataPropertyName = "MODIFIED_DATE"
        Me.DG_MODIFIED_DATE.HeaderText = "更新日"
        Me.DG_MODIFIED_DATE.Name = "DG_MODIFIED_DATE"
        Me.DG_MODIFIED_DATE.ReadOnly = True
        '
        'DG_MODIFIED_BY
        '
        Me.DG_MODIFIED_BY.DataPropertyName = "MODIFIED_BY"
        Me.DG_MODIFIED_BY.FillWeight = 400.0!
        Me.DG_MODIFIED_BY.HeaderText = "更新入力者"
        Me.DG_MODIFIED_BY.Name = "DG_MODIFIED_BY"
        Me.DG_MODIFIED_BY.ReadOnly = True
        '
        'SID
        '
        Me.SID.DataPropertyName = "SID"
        Me.SID.HeaderText = "SID"
        Me.SID.Name = "SID"
        Me.SID.ReadOnly = True
        Me.SID.Visible = False
        '
        'DG_CHECK_FILE
        '
        Me.DG_CHECK_FILE.DataPropertyName = "CHECK_FILE"
        Me.DG_CHECK_FILE.HeaderText = "CHECK_FILE"
        Me.DG_CHECK_FILE.Name = "DG_CHECK_FILE"
        Me.DG_CHECK_FILE.ReadOnly = True
        Me.DG_CHECK_FILE.Visible = False
        '
        'Kousei
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(723, 452)
        Me.Controls.Add(Me.btn_GoBack)
        Me.Controls.Add(Me.pgs)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.M_KOUSEI_KANRI_DataGridView)
        Me.Controls.Add(Me.btn_Del)
        Me.Controls.Add(Me.btn_Export)
        Me.Controls.Add(Me.btn_Update)
        Me.Controls.Add(Me.btn_New)
        Me.Controls.Add(Me.lbl_BASE)
        Me.Controls.Add(Me.M_HEADER_CREATED_DATE)
        Me.Controls.Add(Me.M_HEADER_BASE)
        Me.Controls.Add(Me.M_PME_KOUSEI_INTERVAL)
        Me.Controls.Add(Me.lbl_KOUSEI_INTERVAL)
        Me.Controls.Add(Me.M_PME_KOUSEI_SEIDO)
        Me.Controls.Add(Me.lbl_KOUSEI_SEIDO)
        Me.Controls.Add(Me.lbl_CREATED_DATE)
        Me.Controls.Add(Me.lbl_ATA)
        Me.Controls.Add(Me.M_PME_ATA)
        Me.Controls.Add(Me.M_HEADER_MFG_NO)
        Me.Controls.Add(Me.lbl_MFG_NO)
        Me.Controls.Add(Me.M_HEADER_MFG)
        Me.Controls.Add(Me.lbl_MFG)
        Me.Controls.Add(Me.M_HEADER_MODEL)
        Me.Controls.Add(Me.lbl_MODEL)
        Me.Controls.Add(Me.lbl_BUNRUI_01)
        Me.Controls.Add(Me.M_HEADER_BUNRUI_01)
        Me.Controls.Add(Me.lbl_KANRI_NO)
        Me.Controls.Add(Me.M_HEADER_KANRI_NO)
        Me.Controls.Add(Me.lbl_MEISYOU)
        Me.Controls.Add(Me.M_HEADER_MEISYOU)
        Me.Font = New System.Drawing.Font("ＭＳ Ｐゴシック", 9.0!)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(739, 490)
        Me.MinimumSize = New System.Drawing.Size(739, 490)
        Me.Name = "Kousei"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "校正計測器管理表"
        CType(Me.M_KOUSEI_KANRI_DataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lbl_MEISYOU As System.Windows.Forms.Label
    Friend WithEvents M_HEADER_MEISYOU As System.Windows.Forms.TextBox
    Friend WithEvents lbl_KANRI_NO As System.Windows.Forms.Label
    Friend WithEvents M_HEADER_KANRI_NO As System.Windows.Forms.TextBox
    Friend WithEvents lbl_BUNRUI_01 As System.Windows.Forms.Label
    Friend WithEvents M_HEADER_BUNRUI_01 As System.Windows.Forms.TextBox
    Friend WithEvents M_HEADER_MODEL As System.Windows.Forms.TextBox
    Friend WithEvents lbl_MODEL As System.Windows.Forms.Label
    Friend WithEvents M_HEADER_MFG_NO As System.Windows.Forms.TextBox
    Friend WithEvents lbl_MFG_NO As System.Windows.Forms.Label
    Friend WithEvents M_HEADER_MFG As System.Windows.Forms.TextBox
    Friend WithEvents lbl_MFG As System.Windows.Forms.Label
    Friend WithEvents lbl_ATA As System.Windows.Forms.Label
    Friend WithEvents M_PME_ATA As System.Windows.Forms.TextBox
    Friend WithEvents lbl_CREATED_DATE As System.Windows.Forms.Label
    Friend WithEvents M_PME_KOUSEI_SEIDO As System.Windows.Forms.TextBox
    Friend WithEvents lbl_KOUSEI_SEIDO As System.Windows.Forms.Label
    Friend WithEvents M_PME_KOUSEI_INTERVAL As System.Windows.Forms.TextBox
    Friend WithEvents lbl_KOUSEI_INTERVAL As System.Windows.Forms.Label
    Friend WithEvents lbl_BASE As System.Windows.Forms.Label
    Friend WithEvents M_HEADER_BASE As System.Windows.Forms.TextBox
    Friend WithEvents M_HEADER_CREATED_DATE As System.Windows.Forms.TextBox
    Friend WithEvents btn_Update As System.Windows.Forms.Button
    Friend WithEvents btn_New As System.Windows.Forms.Button
    Friend WithEvents btn_Export As System.Windows.Forms.Button
    Friend WithEvents btn_Del As System.Windows.Forms.Button
    Friend WithEvents M_KOUSEI_KANRI_DataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents pgs As System.Windows.Forms.ProgressBar
    Friend WithEvents btn_GoBack As System.Windows.Forms.Button
    Friend WithEvents CHK As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents DG_KOUSEI_DATE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DG_KOUSEI_COMPANY As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DG_JURYOU_CHECK_DATE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DG_JURYOU_KEKKA As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DG_JURYOU_KOUSEI_BY As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DG_JURYOU_REMARKS As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DG_TENKEN_CHECK_DATE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DG_TENKEN_KOUSEI_BY As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DG_DUE_DATE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DG_TENKEN_REMARKS As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DG_CHECK_FILE_NAME As System.Windows.Forms.DataGridViewLinkColumn
    Friend WithEvents DG_MODIFIED_DATE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DG_MODIFIED_BY As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DG_CHECK_FILE As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

' 2016-04-26 YAMAOKA EDIT 検索条件の構成を変更
' 2016-05-11 YAMAOKA EDIT HT取込データ管理リスト用の条件を追加
' 2016-08-26 YAMAOKA EDIT 有効期限に時間が表示されないように修正

Imports System.Data
Imports Oracle.ManagedDataAccess.Client

'
'   クラス：MasterRepository
'   処理　：設備のMaster情報の取得を行う
'
Public Class MasterRepository

    '
    '   管理番号の存在有無チェック
    '
    Public Shared Function ExistKanriNo(ByVal kanriNo As String, ByVal gid As String) As Boolean
        Dim oracle As New Oracle()
        Try
            Return oracle.DataTable("SELECT KANRI_NO FROM M_HEADER WHERE KANRI_NO = :KANRI_NO AND GID <> :GID", New OracleParameter(":KANRI_NO", kanriNo), New OracleParameter(":GID", gid)).Rows.Count > 0
        Catch ex As Exception
            Throw ex
        Finally
            oracle.Close()
        End Try
    End Function

    '
    '   管理番号の存在有無チェック
    '
    Public Shared Function ExistKanriNo(ByVal kanriNo As String) As Boolean
        Dim oracle As New Oracle()
        Try
            Return oracle.DataTable("SELECT KANRI_NO FROM M_HEADER WHERE KANRI_NO = :KANRI_NO", New OracleParameter(":KANRI_NO", kanriNo)).Rows.Count > 0
        Catch ex As Exception
            Throw ex
        Finally
            oracle.Close()
        End Try
    End Function

	'
	'   ヘッダー情報の取得
	'
	'kubun_index    …台帳タイプ
	'word           …検索窓
	'select_index   …検索項目
	'base           …基地
	'haichi         …配置基準
	'endCheckDate   …点検期限
	'dueDate        …有効期限
	'REASE_FLG		…(HT取込)貸出状態
	'w_o            …(HT取込)W/O
	'REASE_DATE    …(HT取込)貸出日
	'syain_cd       …(HT取込)貸出社員番号
	'acNo	       …(HT取込)使用機番
	'戻り値         …出力情報
	'
	' 2016-05-11 YAMAOKA EDIT HT取込データ管理リスト用の条件を追加
	'Public Shared Function GetHeaderByModel(ByVal kubun_index As Integer, ByVal word As String, ByVal select_index As Integer, ByVal base As String, ByVal haichi As String, ByVal endCheckDate As String, ByVal dueDate As String) As DataTable
	Public Shared Function GetHeaderByModel(ByVal kubun_index As Integer, ByVal word As String, ByVal select_index As Integer, ByVal base As String, ByVal haichi As String, ByVal endCheckDate As String, ByVal dueDate As String, ByVal REASE_FLG As Integer, ByVal w_o As String, ByVal REASE_DATE As String, ByVal syain_cd As String, Optional ByVal acNo As String = "", Optional ByVal status As String = "") As DataTable

		Dim oracle As New Oracle()
		Try
			Dim sql As String = ""
			Dim where As String = ""

			If Not String.IsNullOrEmpty(word) Then
				Select Case select_index
					Case 1 '管理番号
						where = String.Format("{0} AND UPPER(M_HEADER.KANRI_NO) Like '%{1}%'", where, word.ToUpper())
					Case 2 '分類番号
						where = String.Format("{0} AND UPPER(M_HEADER.BUNRUI_01) Like '%{1}%'", where, word.ToUpper())
					Case 3 '名称
						where = String.Format("{0} AND UPPER(M_HEADER.MEISYOU) Like '%{1}%'", where, word.ToUpper())
					Case 4 '型式
						where = String.Format("{0} AND UPPER(M_HEADER.MODEL) Like '%{1}%'", where, word.ToUpper())
					Case 5 '規格
						where = String.Format("{0} AND UPPER(M_HEADER.KIKAKU) Like '%{1}%'", where, word.ToUpper())
					Case 6 'ﾛｹｰｼｮﾝ
						where = String.Format("{0} AND UPPER(M_HEADER.LOCATION) Like '%{1}%'", where, word.ToUpper())
					Case 7 'Refer No
						where = String.Format("{0} AND UPPER(M_HEADER.REFERENCE) Like '%{1}%'", where, word.ToUpper())
				End Select
			End If

            '基地
            If Not String.IsNullOrEmpty(base) Then
                ' If kubun_index = 0 Then
                where = String.Format("{0} AND ( A.BASE Like '%{1}%' OR A.STATUS Like '%{1}%' )", where, base)
                '  Else
                ' where = String.Format("{0} AND M_HEADER.BASE Like '%{1}%'", where, base)
                ' End If

            End If

            '配置基準
            If Not String.IsNullOrEmpty(haichi) Then
                where = String.Format("{0} AND M_HEADER.HAICHI_KIJYUN Like '%{1}%'", where, haichi)
            End If


            '    If kubun_index = 0 Then

            where = where.Replace("M_HEADER", "A")

            ' End If


            '点検期限
            Dim check_date As String = ""
            Dim check_start As String = ""
            Dim check_end As String = ""

            If (kubun_index = 2) Then
                If SetWhereDateForm(endCheckDate, check_date, check_start, check_end) = False Then
                    MsgBox(Ini.ReadValue(Ini.Config.LANG, "Main", "M02122"), MsgBoxStyle.Information, "Information")
                    Return Nothing
                End If
            End If

            '有効期限
            Dim due_date As String = ""
            Dim due_start As String = ""
            Dim due_end As String = ""

            If (kubun_index = 3 Or kubun_index = 4) Then
                If SetWhereDateForm(dueDate, due_date, due_start, due_end) = False Then
                    MsgBox(Ini.ReadValue(Ini.Config.LANG, "Main", "M02121"), MsgBoxStyle.Information, "Information")
                    Return Nothing
                End If
            End If

            Dim ret_date As String = ""
            Dim ret_start As String = ""
            Dim ret_end As String = ""

            If (kubun_index = 100) Then
                'REASE_FLG  …(HT取込)貸出状態
                If REASE_FLG > 0 Then
                    where = String.Format("{0} AND M_REASE_STATUS.REASE_FLG = '{1}'", where, REASE_FLG - 1)
                End If

                'w_o            …(HT取込)W/O
                If Not String.IsNullOrEmpty(w_o) Then
                    where = String.Format("{0} AND M_REASE_STATUS.W_O Like '%{1}%'", where, w_o)
                End If

                'REASE_DATE    …(HT取込)貸出日
                If SetWhereDateForm(REASE_DATE, ret_date, ret_start, ret_end) = False Then
                    MsgBox(Ini.ReadValue(Ini.Config.LANG, "Main", "M02116"), MsgBoxStyle.Information, "Information")
                    Return Nothing
                End If

                'syain_cd       …(HT取込)貸出社員番号
                If Not String.IsNullOrEmpty(syain_cd) Then
                    where = String.Format("{0} AND M_REASE_STATUS.SYAIN_CD = {1}", where, syain_cd)
                End If
            End If
            Console.WriteLine(kubun_index.ToString)
            Select Case kubun_index
                Case 0 '空白
                    sql = "SELECT rownum as No, A.* FROM " +
                            "(SELECT M_HEADER.KANRI_NO, M_HEADER.BUNRUI_01, M_HEADER.MEISYOU, M_HEADER.MODEL, M_HEADER.KIKAKU, M_HEADER.MFG_NO, M_HEADER.CREATED_DATE, M_HEADER.BASE, M_HEADER.LOCATION, M_HEADER.HAICHI_KIJYUN, M_HEADER.KUBUN_01, M_HEADER.RENTAL_FLG, M_HEADER.SHISAN_FLG, M_HEADER.LEASE_FLG, M_HEADER.UN_NO, M_HEADER.MFG, M_HEADER.SPL, M_HEADER.REFERENCE," +
                                "(CASE " +
                                      "WHEN EQUIPMENT_FLG='Y' THEN  (SELECT STATUS FROM M_TOOL WHERE M_TOOL.GID=M_HEADER.GID)" +
                                      "WHEN HOZEN_FLG='Y' THEN  (SELECT STATUS FROM M_HOZEN WHERE M_HOZEN.GID=M_HEADER.GID)" +
                                      "WHEN KOUSEI_FLG='Y' THEN  (SELECT STATUS FROM M_PME WHERE M_PME.GID=M_HEADER.GID)" +
                                      "Else '' End ) STATUS " +
                                " FROM M_HEADER WHERE 1=1 AND ( M_HEADER.KUBUN_01 <> '車両' or M_HEADER.KUBUN_01 is null) ) A WHERE 1=1 " + where
                Case 1 '施設設備管理基本台帳
                    sql = "Select rownum As No, A.* FROM " +
                            "(Select M_HEADER.KANRI_NO, M_HEADER.BUNRUI_01, M_HEADER.MEISYOU, M_HEADER.MODEL, M_HEADER.KIKAKU, M_HEADER.MFG_NO, M_HEADER.CREATED_DATE, M_HEADER.BASE, M_HEADER.LOCATION, M_HEADER.HAICHI_KIJYUN, M_HEADER.KUBUN_01, M_HEADER.RENTAL_FLG, M_HEADER.SHISAN_FLG, M_HEADER.LEASE_FLG, M_HEADER.UN_NO, M_HEADER.MFG, M_HEADER.SPL, M_HEADER.REFERENCE," +
                                "(Case " +
                                      "When EQUIPMENT_FLG='Y' THEN  (SELECT STATUS FROM M_TOOL WHERE M_TOOL.GID=M_HEADER.GID)" +
                                      "WHEN HOZEN_FLG='Y' THEN  (SELECT STATUS FROM M_HOZEN WHERE M_HOZEN.GID=M_HEADER.GID)" +
                                      "WHEN KOUSEI_FLG='Y' THEN  (SELECT STATUS FROM M_PME WHERE M_PME.GID=M_HEADER.GID)" +
                                      "Else '' End ) STATUS " +
                                    " FROM M_HEADER WHERE M_HEADER.HAIKI_FLG <> 'Y' AND M_HEADER.KUBUN_01 <> '車両' ) A WHERE 1=1 " + where

                    '    " FROM M_HEADER WHERE M_HEADER.HAIKI_FLG <> 'Y' AND M_HEADER.KUBUN_01 <> '車両' " + where + " ) A "

                Case 2 '設備管理台帳
                    ' 2016-04-26 YAMAOKA EDIT 検索条件の構成を変更 START
                    If Not String.IsNullOrEmpty(check_date) Then
                        '2018/06/21 SQLが誤っていたので修正
                        'where = String.Format("{0} AND A.END_CHECK_DATE >= to_date('{1}','YYYY-MM-DD HH24:MI:SS') AND A.END_CHECK_DATE <= to_date('{2}','YYYY-MM-DD HH24:MI:SS')", where, check_date + " 00:00:00", check_date + " 23:59:59")
                        '2018/07/25 期限＜＝本日を対象とするよう変更
                        where = String.Format("{0} AND A.END_CHECK_DATE <= to_date('{1}','YYYY-MM-DD HH24:MI:SS')", where, check_date + " 23:59:59")

                    ElseIf Not String.IsNullOrEmpty(check_start) And Not String.IsNullOrEmpty(check_end) Then

                        where = String.Format("{0} AND A.END_CHECK_DATE >= to_date('{1}','yyyy-mm-dd') AND A.END_CHECK_DATE <= to_date('{2}','yyyy-mm-dd')", where, check_start, check_end)

                    End If
                    ' 2016-04-26 YAMAOKA EDIT 検索条件の構成を変更 END

                    sql = "SELECT rownum as No, A.* FROM " +
                          "(SELECT M_HEADER.KANRI_NO, M_HEADER.BUNRUI_01, M_HEADER.MEISYOU, M_HEADER.MODEL, M_HEADER.KIKAKU, M_HEADER.MFG_NO, M_HEADER.CREATED_DATE, M_HEADER.BASE, M_HEADER.LOCATION, M_HEADER.HAICHI_KIJYUN, M_HEADER.KUBUN_01, M_HOZEN.STATUS, M_HEADER.REFERENCE, M_HOZEN.CHECK_INTERVAL, M_HOZEN.CHECK_DATE, M_HOZEN.NEXT_CHECK_DATE, M_HOZEN.END_CHECK_DATE, M_HOZEN.CHECK_COMPANY FROM M_HOZEN LEFT JOIN M_HEADER ON M_HEADER.GID=M_HOZEN.GID WHERE M_HEADER.HAIKI_FLG <> 'Y' AND M_HEADER.KUBUN_01 <> '車両' ) A WHERE 1=1 " + where
                    '"(SELECT M_HEADER.KANRI_NO, M_HEADER.BUNRUI_01, M_HEADER.MEISYOU, M_HEADER.MODEL, M_HEADER.KIKAKU, M_HEADER.MFG_NO, M_HEADER.CREATED_DATE, M_HEADER.BASE, M_HEADER.LOCATION, M_HEADER.HAICHI_KIJYUN, M_HEADER.KUBUN_01, M_HOZEN.STATUS, M_HOZEN.CHECK_INTERVAL, M_HOZEN.CHECK_DATE, M_HOZEN.NEXT_CHECK_DATE, M_HOZEN.END_CHECK_DATE, M_HOZEN.CHECK_COMPANY FROM M_HOZEN LEFT JOIN M_HEADER ON M_HEADER.GID=M_HOZEN.GID WHERE M_HEADER.HAIKI_FLG <> 'Y' " + where + ") A "
                Case 3, 4 '計測器管理台帳
                    If Not String.IsNullOrEmpty(due_date) Then

                        '2018/07/25 期限＜＝本日を対象とするよう変更
                        where = String.Format("{0} AND A.DUE_DATE <= to_date('{1}','yyyy-mm-dd')", where, due_date)

                    ElseIf Not String.IsNullOrEmpty(due_start) And Not String.IsNullOrEmpty(due_end) Then

                        where = String.Format("{0} AND A.DUE_DATE >= to_date('{1}','yyyy-mm-dd') AND A.DUE_DATE <= to_date('{2}','yyyy-mm-dd')", where, due_start, due_end)

                    End If

                    '2016-08-26 YAMAOKA EDIT 有効期限に時間が表示されないように修正 START
                    'sql = "SELECT rownum as No, A.* FROM " +
                    '      "(SELECT M_HEADER.KANRI_NO, M_HEADER.BUNRUI_01, M_HEADER.MEISYOU, M_HEADER.MODEL, M_HEADER.KIKAKU, M_HEADER.MFG_NO, M_HEADER.CREATED_DATE, M_HEADER.BASE, M_HEADER.LOCATION, M_HEADER.HAICHI_KIJYUN, M_HEADER.KUBUN_01, M_PME.STATUS, M_PME.KOUSEI_INTERVAL, M_PME.KOUSEI_COMPANY, M_PME.KOUSEI_BY_COMPANY, M_PME.KOUSEI_DATE, M_PME.DUE_DATE, M_PME.REMARKS FROM M_PME LEFT JOIN M_HEADER ON M_HEADER.GID=M_PME.GID WHERE M_HEADER.HAIKI_FLG <> 'Y' " +
                    '      where +
                    '      ") A "
                    sql = "SELECT rownum as No, A.* FROM " +
                          "(SELECT M_HEADER.KANRI_NO, M_HEADER.BUNRUI_01, M_HEADER.MEISYOU, M_HEADER.MODEL, M_HEADER.KIKAKU, M_HEADER.MFG_NO, M_HEADER.CREATED_DATE, M_HEADER.BASE, M_HEADER.LOCATION, M_HEADER.HAICHI_KIJYUN, M_HEADER.KUBUN_01, M_PME.STATUS, M_HEADER.REFERENCE, M_PME.KOUSEI_INTERVAL, M_PME.KOUSEI_COMPANY, M_PME.KOUSEI_BY_COMPANY, M_PME.KOUSEI_DATE, to_char(M_PME.DUE_DATE,'yyyy/mm/dd') DUE_DATE , M_PME.REMARKS FROM M_PME LEFT JOIN M_HEADER ON M_HEADER.GID=M_PME.GID WHERE M_HEADER.HAIKI_FLG <> 'Y' AND M_HEADER.KUBUN_01 <> '車両' ) A WHERE 1=1 " + where
                    ' where +
                    ' ") A "
                    '2016-08-26 YAMAOKA EDIT 有効期限に時間が表示されないように修正 END
            End Select

            sql = sql + "ORDER BY A.KANRI_NO"

            Console.WriteLine(sql)

            Return oracle.DataTable(sql)
        Catch ex As Exception
            Throw ex
        Finally
            oracle.Close()
        End Try
    End Function

    Public Shared Function GetLendingWOStatus(ByVal base As String, ByVal kanriNo As String, ByVal bunreui As String, ByVal name As String, ByVal wo As String, ByVal tc As String, ByVal LentDateFrom As Date?, ByVal LentDateTo As Date?) As DataTable

        Dim oracle As New Oracle()
        Dim sql As String = ""
        Dim andWhere As String = ""

        Try

            '基地
            If Not String.IsNullOrEmpty(base) Then
                andWhere = String.Format("{0} AND M_HEADER.BASE = '{1}'", andWhere, base)
            End If

            '管理番号
            If Not String.IsNullOrEmpty(kanriNo) Then
                andWhere = String.Format("{0} AND M_HEADER.KANRI_NO = '{1}'", andWhere, kanriNo)
            End If

            '分類番号
            If Not String.IsNullOrEmpty(bunreui) Then
                andWhere = String.Format("{0} AND M_HEADER.BUNRUI_01 LIKE '%{1}%'", andWhere, bunreui)
            End If

            '名称
            If Not String.IsNullOrEmpty(name) Then
                andWhere = String.Format("{0} AND M_HEADER.MEISYOU LIKE '%{1}%'", andWhere, name)
            End If

            'WO
            If Not String.IsNullOrEmpty(wo) Then
                andWhere = String.Format("{0} AND WO LIKE '{1}%'", andWhere, wo)
            End If

            'TC
            If Not String.IsNullOrEmpty(tc) Then
                andWhere = String.Format("{0} AND TC LIKE '{1}%'", andWhere, tc)
            End If

            'LendDate
            If LentDateFrom IsNot Nothing And LentDateTo IsNot Nothing Then
                andWhere = String.Format("{0} AND LENT_DATE BETWEEN '{1}' AND '{2}'", andWhere, LentDateFrom.Value.ToShortDateString, LentDateTo.Value.AddDays(1).ToShortDateString)

            ElseIf LentDateFrom Is Nothing And LentDateTo IsNot Nothing Then
                'TOのみ指定
                andWhere = String.Format("{0} AND LENT_DATE <= '{1}' ", andWhere, LentDateTo.Value.AddDays(1).ToShortDateString)

            ElseIf LentDateFrom IsNot Nothing And LentDateTo Is Nothing Then
                'FROMのみ指定
                andWhere = String.Format("{0} AND LENT_DATE >= '{1}' ", andWhere, LentDateFrom.Value.ToShortDateString)

            End If

            sql = "SELECT  M_HEADER.KANRI_NO  , M_HEADER.BUNRUI_01  , M_HEADER.MEISYOU  , M_HEADER.MODEL  , M_HEADER.KIKAKU  , M_HEADER.MFG_NO  , M_HEADER.CREATED_DATE  , M_HEADER.BASE  , M_HEADER.KUBUN_01  ,T_LEND.LEND_NO  ,TO_CHAR(T_LEND.LENT_DATE,'yyyy/mm/dd HH24:MI') LENT_DATE  ,T_LEND.KANRI_NO  ,T_LEND.AC_NO  ,T_LEND.USAGE  ,T_LEND.LENT_BY  ,T_LEND.RETURNED_BY  ,TO_CHAR(T_LEND.RETURNED_DATE,'yyyy/mm/dd HH24:MI') RETURNED_DATE  , M_HEADER.GID  ,WO  ,TC FROM  T_LEND  INNER JOIN M_HEADER ON   T_LEND.KANRI_NO = M_HEADER.KANRI_NO  LEFT JOIN T_LEND_WO ON   T_LEND.LEND_NO = T_LEND_WO.LEND_NO WHERE     M_HEADER.KUBUN_01 <> '車両' 	"

            sql += andWhere

            sql = String.Format("{0} ORDER BY T_LEND.LENT_DATE,T_LEND.KANRI_NO ", sql)

            Return oracle.DataTable(sql)
        Catch ex As Exception
            Throw ex
        Finally
            oracle.Close()
        End Try

    End Function



    Public Shared Function GetTabaList() As DataTable

        Dim oracle As New Oracle()
        Dim sql As String = ""
        Try
            '借用設備対象外は対象外
            '個人貸与工具は対象外
            '判断結果

            sql = "SELECT   CASE WHEN READ_LOCATION IS NULL THEN 'No Read' WHEN READ_LOCATION <> LOCATION THEN 'Location Error' WHEN READ_LOCATION = LOCATION THEN '○' ELSE 'ERROR' END HANTEI , A.* FROM (SELECT M_HEADER.KANRI_NO, M_HEADER.BUNRUI_01, M_HEADER.MEISYOU, M_HEADER.MODEL, M_HEADER.KIKAKU, M_HEADER.MFG_NO, M_HEADER.CREATED_DATE, M_HEADER.BASE, M_HEADER.LOCATION, M_HEADER.HAICHI_KIJYUN, M_HEADER.KUBUN_01,M_HEADER.UN_NO, M_HEADER.MFG, M_HEADER.SPL,(CASE WHEN EQUIPMENT_FLG='Y' THEN  (SELECT STATUS FROM M_TOOL WHERE M_TOOL.GID=M_HEADER.GID) WHEN HOZEN_FLG='Y' THEN  (SELECT STATUS FROM M_HOZEN WHERE M_HOZEN.GID=M_HEADER.GID)WHEN KOUSEI_FLG='Y' THEN  (SELECT STATUS FROM M_PME WHERE M_PME.GID=M_HEADER.GID) Else '' End ) STATUS ,T_TANA.READ_LOCATION,T_TANA.COUNT,T_TANA.COUNT_BY,TO_CHAR( T_TANA.COUNT_DATE , 'YYYY/MM/DD') FROM M_HEADER,T_TANA WHERE 1=1 AND M_HEADER.KANRI_NO = T_TANA.KANRI_NO(+) AND M_HEADER.HAIKI_FLG <> 'Y' AND  M_HEADER.KUBUN_01 <> '車両' AND M_HEADER.KUBUN_01 <> '借用設備' AND M_HEADER.LOCATION <> '個人' AND M_HEADER.MEISYOU NOT LIKE '%個人%' ) A WHERE 1=1 ORDER BY A.KANRI_NO "
            Console.WriteLine(sql)
            Return oracle.DataTable(sql)
        Catch ex As Exception
            Throw ex
        Finally
            oracle.Close()
        End Try

    End Function

    Public Shared Function GetLendingStatus(ByVal kubun_index As Integer, ByVal base As String, ByVal syain_cd As String, ByVal acNo As String, ByVal status As String) As DataTable

        Dim oracle As New Oracle()
        Dim sql As String = ""
        Dim andWhere As String = ""

        Try

            '基地
            If Not String.IsNullOrEmpty(base) Then
                andWhere = String.Format("{0} AND M_HEADER.BASE = '{1}'", andWhere, base)
            End If

            'ACNo
            If Not String.IsNullOrEmpty(acNo) Then
                andWhere = String.Format("{0} AND AC_NO LIKE '%{1}%'", andWhere, acNo)
            End If

            'Status
            'If Not String.IsNullOrEmpty(status) Then
            ' andWhere = String.Format("{0} AND ( M_TOOL.STATUS = '{1}' OR M_HOZEN.STATUS = '{1}' OR M_PME.STATUS = '{1}' ) ", andWhere, status)
            ' End If

            'Status
            If Not String.IsNullOrEmpty(status) Then
                andWhere = String.Format("{0} AND T_LEND.USAGE = '{1}' ", andWhere, status)
            End If


            '貸出社員番号
            If Not String.IsNullOrEmpty(syain_cd) Then
                andWhere = String.Format("{0} AND LENT_BY LIKE '{1}%'", andWhere, syain_cd)
            End If


            sql = "SELECT  M_HEADER.KANRI_NO  , M_HEADER.BUNRUI_01  , M_HEADER.MEISYOU  , M_HEADER.MODEL  , M_HEADER.KIKAKU  , M_HEADER.MFG_NO  , M_HEADER.CREATED_DATE  , M_HEADER.BASE  , M_HEADER.LOCATION  , M_HEADER.HAICHI_KIJYUN  , M_HEADER.KUBUN_01  , M_HEADER.RENTAL_FLG  , M_HEADER.SHISAN_FLG  , M_HEADER.LEASE_FLG  , M_HEADER.UN_NO  , M_HEADER.MFG  , M_HEADER.SPL  , (   CASE    WHEN EQUIPMENT_FLG = 'Y'     THEN M_TOOL.STATUS   WHEN HOZEN_FLG = 'Y'     THEN M_HOZEN.STATUS   WHEN KOUSEI_FLG = 'Y'    THEN M_PME.STATUS    Else ''    End  ) STATUS  ,T_LEND.LEND_NO  ,TO_CHAR(T_LEND.LENT_DATE,'yyyy/mm/dd HH24:MI') LENT_DATE  ,T_LEND.KANRI_NO  ,T_LEND.AC_NO  ,T_LEND.USAGE  ,T_LEND.LENT_BY  ,T_LEND.RETURNED_BY  ,TO_CHAR(T_LEND.RETURNED_DATE,'yyyy/mm/dd HH24:MI') RETURNED_DATE  , M_HEADER.GID FROM  T_LEND  INNER JOIN M_HEADER ON   T_LEND.KANRI_NO = M_HEADER.KANRI_NO  LEFT JOIN M_TOOL ON   M_TOOL.GID = M_HEADER.GID  LEFT JOIN M_HOZEN ON   M_HOZEN.GID = M_HEADER.GID  LEFT JOIN M_PME ON   M_PME.GID = M_HEADER.GID WHERE  T_LEND.RETURNED_DATE IS NULL      AND M_HEADER.KUBUN_01 <> '車両' "

            sql += andWhere

            sql = String.Format("{0} ORDER BY T_LEND.USAGE,T_LEND.LENT_DATE,T_LEND.KANRI_NO ", sql)



            Return oracle.DataTable(sql)
        Catch ex As Exception
			Throw ex
		Finally
			oracle.Close()
		End Try
	End Function

	' 2016-04-26 YAMAOKA EDIT 検索条件の構成を変更 START
	'
	'   日付型Where条件の設定
	'   inputDate	…入力データ
	'   setDate	    …Where条件：日付
	'   setStart	…Where条件：日付(start)
	'   setEnd	    …Where条件：日付(end)
	'   戻り値      …出力結果
	'
	Private Shared Function SetWhereDateForm(ByVal inputDate As String, ByRef setDate As String, ByRef setStart As String, ByRef setEnd As String) As Boolean

        Dim work As String
        Dim workDate As Date

        If String.IsNullOrEmpty(inputDate) Then Return True

        Try
            Select Case inputDate.Length
                Case 6
                    work = inputDate + "01"
                    setStart = Format(CInt(work), "0000/00/00")
                    workDate = CDate(setStart)

                    work = inputDate + Format(DateTime.DaysInMonth(workDate.Year, workDate.Month), "00")
                    setEnd = Format(CInt(work), "0000/00/00")
                Case 8
                    setDate = Format(CInt(inputDate), "0000/00/00")
                Case Else
                    Return False
            End Select
        Catch ex As Exception
            Return False
        End Try

        Return True

    End Function
    ' 2016-04-26 YAMAOKA EDIT 検索条件の構成を変更 END


    '
    '   特定設備情報の取得
    '
    Public Shared Function GetAllDataFromDB(ByVal tableName As String, ByVal kanriNo As String) As DataTable
        Dim oracle As New Oracle()
        Try
            Dim sql As String
            sql = "SELECT * FROM " + tableName + " WHERE KANRI_NO = :KANRI_NO"
            '   System.Diagnostics.Debug.WriteLine(" GetHeaderDataReader.sql:" & sql)
            Return oracle.DataTable(sql, New OracleParameter(":KANRI_NO", kanriNo))

        Catch ex As Exception
            Throw ex
        Finally
            oracle.Close()
        End Try
    End Function

    Public Shared Function GetBlobData(ByVal gid As String) As DataTable
        Dim oracle As New Oracle()
        Try
            Return oracle.DataTable("SELECT CERTIFICATE,CERTIFICATE_NAME,IMAGE,IMAGE_NAME FROM M_HEADER WHERE GID = :GID", New OracleParameter(":GID", gid))
        Catch ex As Exception
            Throw ex
        Finally
            oracle.Close()
        End Try
    End Function

    Public Shared Function IsExistFromDB(ByVal tableName As String, ByVal gid As String) As Boolean
        Dim oracle As New Oracle()
        Try
            Return oracle.DataTable("SELECT * FROM " + tableName + " WHERE GID = :GID", New OracleParameter(":GID", gid)).Rows.Count > 0
        Catch ex As Exception
            Throw ex
        Finally
            oracle.Close()
        End Try
    End Function

    '
    '   更新履歴の取得
    '
    Public Shared Function GetAllHistoryDataFromDB(ByVal tableName As String, ByVal gid As String) As DataTable
        Dim oracle As New Oracle()
        Try
            Return oracle.DataTable("SELECT * FROM " + tableName + " WHERE GID = :GID ORDER BY TRAN_DATE ASC", New OracleParameter(":GID", gid))
        Catch ex As Exception
            Throw ex
        Finally
            oracle.Close()
        End Try
    End Function

    '
    '   ヘッダー情報の取得   
    '
    Public Shared Function GetHeaderDataReader(ByVal tableName As String, ByVal kanriNo As String, ByRef oracle As Oracle) As OracleDataReader
        'Dim oracle As New Oracle()
        Try
            Dim sql As String

            sql = "SELECT * FROM " + tableName + " WHERE KANRI_NO = :KANRI_NO"

            '   System.Diagnostics.Debug.WriteLine(" MasterRepostory.sql:" & sql)

            Return oracle.ExecuteReader(sql, New OracleParameter(":KANRI_NO", kanriNo))

        Catch ex As Exception
            Throw ex
        Finally
            'oracle.Close()
        End Try
    End Function

    '
    '   更新履歴の一覧情報の取得
    '
    Public Shared Function GetHistoryDataTable(ByVal tableName As String, ByVal gid As String) As DataTable
        Dim oracle As New Oracle()
        Try
            Dim sql = ""
            If String.IsNullOrEmpty(gid) Then
                sql = "SELECT rownum as No , A.* FROM (SELECT * FROM " + tableName + "_History ORDER BY TRAN_DATE DESC) A"
            Else
                sql = "SELECT rownum as No , A.* FROM (SELECT * FROM " + tableName + "_History WHERE GID='" + gid + "' ORDER BY TRAN_DATE DESC) A"
            End If

            Return oracle.DataTable(sql)
        Catch ex As Exception
            Throw ex
        Finally
            oracle.Close()
        End Try
    End Function

    '
    '   排他更新済みチェック
    '
    Public Shared Function IsUpdated(ByVal kanriNo As String, ByVal gid As String, ByVal MODIFIED_DATE As String) As Boolean
        'Dim oracle As New Oracle()
        Dim RowCount As Integer = 1

        Try
            ' RowCount = oracle.DataTable("SELECT KANRI_NO FROM M_HEADER WHERE KANRI_NO = :KANRI_NO AND GID = :GID AND MODIFIED_DATE = TO_DATE(:MODIFIED_DATE,'YYYY-MM-DD HH24:MI:SS')", New OracleParameter(":KANRI_NO", kanriNo), New OracleParameter(":GID", gid), New OracleParameter(":MODIFIED_DATE", MODIFIED_DATE)).Rows.Count
        Catch ex As Exception
            Throw ex
        Finally
            ' oracle.Close()
        End Try

        If RowCount = 1 Then
            Return False
        Else
            Return True
        End If

    End Function



End Class

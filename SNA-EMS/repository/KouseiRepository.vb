﻿Imports System.Data
Imports Oracle.ManagedDataAccess.Client
Imports System.Text

'
'   クラス：MasterRepository
'   処理　：校正計測器管理表情報の取得を行う
'
Public Class KouseiRepository

    '
    '   校正計測器管理表情報の取得
    '
    Public Shared Function GetAllDataFromDB(ByVal gid As String) As DataTable
        Dim oracle As New Oracle()
        Try
            'GID, SID, KANRI_NO, KOUSEI_DATE, KOUSEI_COMPANY, JURYOU_CHECK_DATE, KEKKA, KOUSEI_BY, DUE_DATE, REMARKS, MODIFIED_DATE, MODIFIED_BY, VCHG

            Return oracle.DataTable("SELECT GID, SID, KANRI_NO, to_char(KOUSEI_DATE,'yyyy/mm/dd') AS KOUSEI_DATE, KOUSEI_COMPANY, to_char(JURYOU_CHECK_DATE,'yyyy/mm/dd') AS JURYOU_CHECK_DATE, JURYOU_KEKKA, JURYOU_KOUSEI_BY, to_char(DUE_DATE,'yyyy/mm/dd') AS DUE_DATE, JURYOU_REMARKS, to_char(TENKEN_CHECK_DATE,'yyyy/mm/dd') AS TENKEN_CHECK_DATE, TENKEN_KEKKA, TENKEN_KOUSEI_BY, TENKEN_REMARKS, CHECK_FILE, CHECK_FILE_NAME, MODIFIED_DATE, MODIFIED_BY, VCHG FROM M_KOUSEI_KANRI WHERE GID = :GID AND VCHG <> 'D' ORDER BY DUE_DATE DESC ", New OracleParameter(":GID", gid))
        Catch ex As Exception
            Throw ex
        Finally
            oracle.Close()
        End Try
    End Function

    Public Shared Function GetDataBySID(ByVal sid As String) As DataTable
        Dim oracle As New Oracle()
        Try
            Return oracle.DataTable("SELECT * FROM M_KOUSEI_KANRI WHERE SID = :SID AND VCHG <> 'D' ORDER BY DUE_DATE DESC ", New OracleParameter(":SID", sid))
        Catch ex As Exception
            Throw ex
        Finally
            oracle.Close()
        End Try
    End Function

    Public Shared Function GetTopDataByGID(ByVal gid As String) As DataTable
        Dim oracle As New Oracle()
        Try
            Return oracle.DataTable("SELECT * FROM ( SELECT * FROM M_KOUSEI_KANRI WHERE GID = :GID AND VCHG <> 'D' ORDER BY DUE_DATE DESC ) WHERE ROWNUM<2 ", New OracleParameter(":GID", gid))
        Catch ex As Exception
            Throw ex
        Finally
            oracle.Close()
        End Try
    End Function

    Public Shared Function Insert(ByVal tableName As String, ByVal paramDict As Dictionary(Of String, Object)) As Boolean
        Dim pair As KeyValuePair(Of String, Object)
        Dim sql As StringBuilder = New StringBuilder()
        Dim sqlp As StringBuilder = New StringBuilder()
        Dim sqlv As StringBuilder = New StringBuilder()
        Dim parameterList As List(Of OracleParameter) = New List(Of OracleParameter)

        'insert into table (...) values (...)
        For Each pair In paramDict
            Dim parameter As OracleParameter  = New OracleParameter(String.Format(":{0}", pair.Key), pair.Value)
            sqlp.AppendFormat(", {0}", pair.Key)
            sqlv.AppendFormat(", :{0}", pair.Key)
            parameterList.Add(parameter)
        Next
        sql.AppendFormat("INSERT INTO {0} ({1}) VALUES ({2})", tableName, sqlp.ToString().Substring(1), sqlv.ToString().Substring(1))
       
        Dim oracle As New Oracle()
        Try
            Dim result = oracle.ExecuteNonQuery(sql.ToString(), parameterList.ToArray)
            Return result > 0
        Catch ex As Exception
            Throw ex
        Finally
            oracle.Close()
        End Try
        Return False
    End Function


    Public Shared Function Update(ByVal tableName As String, ByVal paramDict As Dictionary(Of String, Object), ByVal whereDict As Dictionary(Of String, Object)) As Boolean
        Dim pair As KeyValuePair(Of String, Object)
        Dim sql As StringBuilder = New StringBuilder()
        Dim sqlp As StringBuilder = New StringBuilder()
        Dim sqlw As StringBuilder = New StringBuilder()
        Dim parameterList As List(Of OracleParameter) = New List(Of OracleParameter)

        For Each pair In paramDict
            Dim parameter As OracleParameter
            parameter = New OracleParameter(String.Format(":{0}", pair.Key), pair.Value)
            sqlp.AppendFormat(", {0}=:{0}", pair.Key)
            parameterList.Add(parameter)
        Next

        For Each pair In whereDict
            Dim parameter As OracleParameter
            parameter = New OracleParameter(String.Format(":{0}", pair.Key), pair.Value)
            sqlw.AppendFormat(" AND {0}=:{0} ", pair.Key)
            parameterList.Add(parameter)
        Next

        'update tablename set ... where ..
        sql.AppendFormat("UPDATE {0} SET {1} WHERE 1=1 {2}", tableName, sqlp.ToString().Substring(1), sqlw.ToString().Substring(1))
        Dim oracle As New Oracle()
        Try
            Dim result = oracle.ExecuteNonQuery(sql.ToString(), parameterList.ToArray)
            Return result > 0
        Catch ex As Exception
            Throw ex
        Finally
            oracle.Close()
        End Try
        Return False
    End Function


    Public Shared Function Delete(ByVal tableName As String, ByVal whereSql As String) As Boolean
        Dim oracle As New Oracle()
        Try
            Dim result = oracle.ExecuteNonQuery(String.Format("UPDATE {0} SET VCHG = 'D' WHERE {1}", tableName, whereSql))
            Return result > 0
        Catch ex As Exception
            Throw ex
        Finally
            oracle.Close()
        End Try
        Return False
    End Function


    Public Shared Function GetSEQ() As Integer
        Dim oracle As New Oracle()
        Try
            Dim dt As DataTable = oracle.DataTable("SELECT SEQ_M_KOUSEI_KANRI.Nextval AS Nextval FROM DUAL")
            Return Integer.Parse(dt.Rows.Item(0).Item(0))
        Catch ex As Exception
            Throw ex
        Finally
            oracle.Close()
        End Try
    End Function


End Class

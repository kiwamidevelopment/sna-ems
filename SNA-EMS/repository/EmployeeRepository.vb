﻿Imports Oracle.ManagedDataAccess.Client

'
'   クラス：EmployeeRepository
'   処理　：利用者情報の取得を行う
'
Public Class EmployeeRepository
    
    '
    '   利用者情報の取得
    '
    Public Shared Function GetEmployee(ByVal empNo As String, ByVal empPassword As String, ByVal empKose As Integer) As M_EMPLOYEE
        Dim oracle As New Oracle()
        Try
            Dim sql As String
            ' 2016-04-08 YAMAOKA EDIT 役割とユーザーの関連付け START
            'sql = "SELECT A.EMP_NO, A.EMP_NAME, A.EMP_PSWD, A.EMP_DESC, B.AX_MST, B.AX_VAD, B.AX_RPT, B.AX_PRN, B.AX_KOSE, B.AX_OTH FROM M_EMPLOYEE A, M_EMPLOYEE_AX B WHERE A.EMP_NO = B.EMP_NO AND A.EMP_NO = :EMP_NO AND A.EMP_PSWD = :EMP_PSWD"
            If empKose <> 4 Then
                sql = "SELECT A.EMP_NO, A.EMP_NAME, A.EMP_PSWD, A.EMP_DESC, B.AX_MST, B.AX_VAD, B.AX_RPT, B.AX_PRN, B.AX_KOSE, B.AX_OTH FROM M_EMPLOYEE A, M_EMPLOYEE_AX B WHERE A.EMP_NO = B.EMP_NO AND A.EMP_NO = :EMP_NO AND A.EMP_PSWD = :EMP_PSWD and A.EMP_NO <>'07378' "
            Else
                sql = "SELECT A.EMP_NO, A.EMP_NAME, A.EMP_PSWD, A.EMP_DESC, B.AX_MST, B.AX_VAD, B.AX_RPT, B.AX_PRN, B.AX_KOSE, B.AX_OTH FROM M_EMPLOYEE A, M_EMPLOYEE_AX B WHERE A.EMP_NO = B.EMP_NO AND A.EMP_NO = :EMP_NO AND A.EMP_PSWD = :EMP_PSWD and A.EMP_NO ='07378' "
            End If
            ' 2016-04-08 YAMAOKA EDIT 役割とユーザーの関連付け END
            Dim dr As OracleDataReader = oracle.ExecuteReader(sql, New OracleParameter(":EMP_NO", empNo), New OracleParameter(":EMP_PSWD", empPassword))
            If dr.HasRows = False Then
                Return Nothing
            End If
            Dim employee As M_EMPLOYEE = Helper.DataReaderToEntity(dr, New M_EMPLOYEE)
            Return employee
        Catch ex As Exception
            Throw ex
        Finally
            oracle.Close()
        End Try
        Return Nothing
    End Function

    ''' <summary>
    ''' Add Login Log
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function AddLoginLog(log As LoginLog)
        Dim paramDict As Dictionary(Of String, Object) = New Dictionary(Of String, Object)
        paramDict.Add("EMP_NO", LoginRoler.Employee.EMP_NO)
        paramDict.Add("EMP_NAME", LoginRoler.Employee.EMP_NAME)
        If log = LoginLog.LOGIN_IN Then
            paramDict.Add("ACTION", "LOGIN_IN")
        ElseIf log = LoginLog.LOGIN_OUT Then
            paramDict.Add("ACTION", "LOGIN_OUT")
        End If
        paramDict.Add("TIMESTAMP", Date.Now)
        paramDict.Add("LOGIN_DESC", "")
        KouseiRepository.Insert("M_LOGIN_HISTORY", paramDict)
        Return Nothing
    End Function
End Class

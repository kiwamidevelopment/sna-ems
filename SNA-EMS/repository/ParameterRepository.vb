﻿Imports System.Data

'
'   クラス：ParameterRepository
'   処理　：設備のパラメータ情報の取得を行う
'
Public Class ParameterRepository
    '
    '   パラメータ情報の取得
    '
    Public Shared Function GetParameters(ByVal paramKey As String, ByVal paramVal As String) As DataTable
        Dim oracle As New Oracle()
        Try
            Dim sql As String
            sql = "SELECT * FROM((select '{0}' PARAMETER_KEY, 0 SEQ_NO, '' PARAMETER_VALUE FROM DUAL) UNION ALL(SELECT PARAMETER_KEY,SEQ_NO,PARAMETER_VALUE FROM M_PARAMETER WHERE PARAMETER_KEY = '{0}' AND VCHG='Y'"
            If Not String.IsNullOrEmpty(paramVal) Then
                sql = sql + String.Format(" AND PARAMETER_VALUE LIKE '%{0}%'", paramVal)
            End If
            sql = sql + ")) B ORDER BY B.SEQ_NO"
            Return oracle.DataTable(String.Format(sql, paramKey, paramKey))
        Catch ex As Exception
            Throw ex
        Finally
            oracle.Close()
        End Try
        Return Nothing
    End Function
End Class

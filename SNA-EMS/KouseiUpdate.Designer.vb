﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class KouseiUpdate
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(KouseiUpdate))
        Me.btn_Cancel = New System.Windows.Forms.Button()
        Me.btn_Insert = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.lbl_BUNRUI_01 = New System.Windows.Forms.Label()
        Me.M_HEADER_BUNRUI_01 = New System.Windows.Forms.TextBox()
        Me.lbl_KANRI_NO = New System.Windows.Forms.Label()
        Me.M_HEADER_KANRI_NO = New System.Windows.Forms.TextBox()
        Me.KOUSEI_DATE = New System.Windows.Forms.DateTimePicker()
        Me.lbl_KOUSEI_DATE = New System.Windows.Forms.Label()
        Me.lbl_KOUSEI_COMPANY = New System.Windows.Forms.Label()
        Me.KOUSEI_COMPANY = New System.Windows.Forms.TextBox()
        Me.lbl_DUE_DATE = New System.Windows.Forms.Label()
        Me.lbl_MODIFIED_DATE = New System.Windows.Forms.Label()
        Me.MODIFIED_BY = New System.Windows.Forms.TextBox()
        Me.lbl_MODIFIED_BY = New System.Windows.Forms.Label()
        Me.DUE_DATE = New System.Windows.Forms.DateTimePicker()
        Me.btn_Update = New System.Windows.Forms.Button()
        Me.MODIFIED_DATE = New System.Windows.Forms.DateTimePicker()
        Me.gpx_JURYOU_GROUP = New System.Windows.Forms.GroupBox()
        Me.lbl_JURYOU_KEKKA_N = New System.Windows.Forms.RadioButton()
        Me.lbl_JURYOU_KEKKA_Y = New System.Windows.Forms.RadioButton()
        Me.JURYOU_REMARKS = New System.Windows.Forms.TextBox()
        Me.lbl_JURYOU_REMARKS = New System.Windows.Forms.Label()
        Me.lbl_JURYOU_KOUSEI_BY = New System.Windows.Forms.Label()
        Me.lbl_JURYOU_KEKKA = New System.Windows.Forms.Label()
        Me.lbl_JURYOU_CHECK_DATE = New System.Windows.Forms.Label()
        Me.JURYOU_KOUSEI_BY = New System.Windows.Forms.TextBox()
        Me.JURYOU_CHECK_DATE = New System.Windows.Forms.DateTimePicker()
        Me.gpx_TENKEN_GROUP = New System.Windows.Forms.GroupBox()
        Me.lbl_TENKEN_KEKKA_N = New System.Windows.Forms.RadioButton()
        Me.TENKEN_REMARKS = New System.Windows.Forms.TextBox()
        Me.lbl_TENKEN_KEKKA_Y = New System.Windows.Forms.RadioButton()
        Me.lbl_TENKEN_REMARKS = New System.Windows.Forms.Label()
        Me.lbl_TENKEN_KOUSEI_BY = New System.Windows.Forms.Label()
        Me.lbl_TENKEN_KEKKA = New System.Windows.Forms.Label()
        Me.lbl_TENKEN_CHECK_DATE = New System.Windows.Forms.Label()
        Me.TENKEN_KOUSEI_BY = New System.Windows.Forms.TextBox()
        Me.TENKEN_CHECK_DATE = New System.Windows.Forms.DateTimePicker()
        Me.btn_FileClear = New System.Windows.Forms.Button()
        Me.btn_File = New System.Windows.Forms.Button()
        Me.CHECK_FILE = New System.Windows.Forms.TextBox()
        Me.lbl_CHECK_FILE = New System.Windows.Forms.Label()
        Me.btn_FileDownload = New System.Windows.Forms.Button()
        Me.btn_TENKEN_IN = New System.Windows.Forms.Button()
        Me.gpx_JURYOU_GROUP.SuspendLayout()
        Me.gpx_TENKEN_GROUP.SuspendLayout()
        Me.SuspendLayout()
        '
        'btn_Cancel
        '
        Me.btn_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btn_Cancel.Location = New System.Drawing.Point(24, 475)
        Me.btn_Cancel.Name = "btn_Cancel"
        Me.btn_Cancel.Size = New System.Drawing.Size(79, 21)
        Me.btn_Cancel.TabIndex = 201
        Me.btn_Cancel.Text = "キャンセル"
        '
        'btn_Insert
        '
        Me.btn_Insert.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btn_Insert.Location = New System.Drawing.Point(398, 475)
        Me.btn_Insert.Name = "btn_Insert"
        Me.btn_Insert.Size = New System.Drawing.Size(65, 21)
        Me.btn_Insert.TabIndex = 1011
        Me.btn_Insert.Text = "登録"
        Me.btn_Insert.Visible = False
        '
        'Label6
        '
        Me.Label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label6.Location = New System.Drawing.Point(-5, 469)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(480, 3)
        Me.Label6.TabIndex = 1010
        '
        'lbl_BUNRUI_01
        '
        Me.lbl_BUNRUI_01.Location = New System.Drawing.Point(227, 12)
        Me.lbl_BUNRUI_01.Name = "lbl_BUNRUI_01"
        Me.lbl_BUNRUI_01.Size = New System.Drawing.Size(59, 17)
        Me.lbl_BUNRUI_01.TabIndex = 1016
        Me.lbl_BUNRUI_01.Text = "分類番号"
        Me.lbl_BUNRUI_01.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_HEADER_BUNRUI_01
        '
        Me.M_HEADER_BUNRUI_01.Enabled = False
        Me.M_HEADER_BUNRUI_01.Location = New System.Drawing.Point(288, 10)
        Me.M_HEADER_BUNRUI_01.MaxLength = 20
        Me.M_HEADER_BUNRUI_01.Name = "M_HEADER_BUNRUI_01"
        Me.M_HEADER_BUNRUI_01.Size = New System.Drawing.Size(175, 19)
        Me.M_HEADER_BUNRUI_01.TabIndex = 1014
        Me.M_HEADER_BUNRUI_01.Tag = "BUNRUI_01"
        '
        'lbl_KANRI_NO
        '
        Me.lbl_KANRI_NO.Location = New System.Drawing.Point(21, 9)
        Me.lbl_KANRI_NO.Name = "lbl_KANRI_NO"
        Me.lbl_KANRI_NO.Size = New System.Drawing.Size(57, 23)
        Me.lbl_KANRI_NO.TabIndex = 1014
        Me.lbl_KANRI_NO.Text = "管理番号"
        Me.lbl_KANRI_NO.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_HEADER_KANRI_NO
        '
        Me.M_HEADER_KANRI_NO.Enabled = False
        Me.M_HEADER_KANRI_NO.Location = New System.Drawing.Point(82, 10)
        Me.M_HEADER_KANRI_NO.MaxLength = 20
        Me.M_HEADER_KANRI_NO.Name = "M_HEADER_KANRI_NO"
        Me.M_HEADER_KANRI_NO.Size = New System.Drawing.Size(137, 19)
        Me.M_HEADER_KANRI_NO.TabIndex = 1013
        Me.M_HEADER_KANRI_NO.Tag = "KANRI_NO"
        '
        'KOUSEI_DATE
        '
        Me.KOUSEI_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.KOUSEI_DATE.Location = New System.Drawing.Point(82, 46)
        Me.KOUSEI_DATE.Name = "KOUSEI_DATE"
        Me.KOUSEI_DATE.ShowCheckBox = True
        Me.KOUSEI_DATE.Size = New System.Drawing.Size(133, 19)
        Me.KOUSEI_DATE.TabIndex = 1015
        '
        'lbl_KOUSEI_DATE
        '
        Me.lbl_KOUSEI_DATE.Location = New System.Drawing.Point(12, 45)
        Me.lbl_KOUSEI_DATE.Name = "lbl_KOUSEI_DATE"
        Me.lbl_KOUSEI_DATE.Size = New System.Drawing.Size(66, 23)
        Me.lbl_KOUSEI_DATE.TabIndex = 1018
        Me.lbl_KOUSEI_DATE.Text = "校正実施日"
        Me.lbl_KOUSEI_DATE.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lbl_KOUSEI_COMPANY
        '
        Me.lbl_KOUSEI_COMPANY.Location = New System.Drawing.Point(19, 73)
        Me.lbl_KOUSEI_COMPANY.Name = "lbl_KOUSEI_COMPANY"
        Me.lbl_KOUSEI_COMPANY.Size = New System.Drawing.Size(59, 17)
        Me.lbl_KOUSEI_COMPANY.TabIndex = 1020
        Me.lbl_KOUSEI_COMPANY.Text = "校正会社"
        Me.lbl_KOUSEI_COMPANY.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'KOUSEI_COMPANY
        '
        Me.KOUSEI_COMPANY.Location = New System.Drawing.Point(82, 71)
        Me.KOUSEI_COMPANY.MaxLength = 50
        Me.KOUSEI_COMPANY.Name = "KOUSEI_COMPANY"
        Me.KOUSEI_COMPANY.Size = New System.Drawing.Size(381, 19)
        Me.KOUSEI_COMPANY.TabIndex = 1016
        Me.KOUSEI_COMPANY.Tag = ""
        '
        'lbl_DUE_DATE
        '
        Me.lbl_DUE_DATE.Location = New System.Drawing.Point(19, 98)
        Me.lbl_DUE_DATE.Name = "lbl_DUE_DATE"
        Me.lbl_DUE_DATE.Size = New System.Drawing.Size(59, 17)
        Me.lbl_DUE_DATE.TabIndex = 1020
        Me.lbl_DUE_DATE.Text = "有効期限"
        Me.lbl_DUE_DATE.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lbl_MODIFIED_DATE
        '
        Me.lbl_MODIFIED_DATE.Location = New System.Drawing.Point(19, 417)
        Me.lbl_MODIFIED_DATE.Name = "lbl_MODIFIED_DATE"
        Me.lbl_MODIFIED_DATE.Size = New System.Drawing.Size(59, 17)
        Me.lbl_MODIFIED_DATE.TabIndex = 1020
        Me.lbl_MODIFIED_DATE.Text = "更新日"
        Me.lbl_MODIFIED_DATE.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'MODIFIED_BY
        '
        Me.MODIFIED_BY.Enabled = False
        Me.MODIFIED_BY.Location = New System.Drawing.Point(82, 441)
        Me.MODIFIED_BY.MaxLength = 20
        Me.MODIFIED_BY.Name = "MODIFIED_BY"
        Me.MODIFIED_BY.Size = New System.Drawing.Size(137, 19)
        Me.MODIFIED_BY.TabIndex = 1032
        Me.MODIFIED_BY.Tag = ""
        '
        'lbl_MODIFIED_BY
        '
        Me.lbl_MODIFIED_BY.Location = New System.Drawing.Point(12, 443)
        Me.lbl_MODIFIED_BY.Name = "lbl_MODIFIED_BY"
        Me.lbl_MODIFIED_BY.Size = New System.Drawing.Size(66, 19)
        Me.lbl_MODIFIED_BY.TabIndex = 1020
        Me.lbl_MODIFIED_BY.Text = "更新入力者"
        Me.lbl_MODIFIED_BY.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'DUE_DATE
        '
        Me.DUE_DATE.Enabled = False
        Me.DUE_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DUE_DATE.Location = New System.Drawing.Point(82, 96)
        Me.DUE_DATE.Name = "DUE_DATE"
        Me.DUE_DATE.ShowCheckBox = True
        Me.DUE_DATE.Size = New System.Drawing.Size(133, 19)
        Me.DUE_DATE.TabIndex = 1017
        '
        'btn_Update
        '
        Me.btn_Update.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btn_Update.Location = New System.Drawing.Point(398, 475)
        Me.btn_Update.Name = "btn_Update"
        Me.btn_Update.Size = New System.Drawing.Size(65, 21)
        Me.btn_Update.TabIndex = 201
        Me.btn_Update.Text = "更新"
        Me.btn_Update.Visible = False
        '
        'MODIFIED_DATE
        '
        Me.MODIFIED_DATE.Enabled = False
        Me.MODIFIED_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.MODIFIED_DATE.Location = New System.Drawing.Point(82, 415)
        Me.MODIFIED_DATE.Name = "MODIFIED_DATE"
        Me.MODIFIED_DATE.Size = New System.Drawing.Size(133, 19)
        Me.MODIFIED_DATE.TabIndex = 1031
        '
        'gpx_JURYOU_GROUP
        '
        Me.gpx_JURYOU_GROUP.Controls.Add(Me.lbl_JURYOU_KEKKA_N)
        Me.gpx_JURYOU_GROUP.Controls.Add(Me.lbl_JURYOU_KEKKA_Y)
        Me.gpx_JURYOU_GROUP.Controls.Add(Me.JURYOU_REMARKS)
        Me.gpx_JURYOU_GROUP.Controls.Add(Me.lbl_JURYOU_REMARKS)
        Me.gpx_JURYOU_GROUP.Controls.Add(Me.lbl_JURYOU_KOUSEI_BY)
        Me.gpx_JURYOU_GROUP.Controls.Add(Me.lbl_JURYOU_KEKKA)
        Me.gpx_JURYOU_GROUP.Controls.Add(Me.lbl_JURYOU_CHECK_DATE)
        Me.gpx_JURYOU_GROUP.Controls.Add(Me.JURYOU_KOUSEI_BY)
        Me.gpx_JURYOU_GROUP.Controls.Add(Me.JURYOU_CHECK_DATE)
        Me.gpx_JURYOU_GROUP.Location = New System.Drawing.Point(9, 118)
        Me.gpx_JURYOU_GROUP.Name = "gpx_JURYOU_GROUP"
        Me.gpx_JURYOU_GROUP.Size = New System.Drawing.Size(454, 131)
        Me.gpx_JURYOU_GROUP.TabIndex = 1021
        Me.gpx_JURYOU_GROUP.TabStop = False
        Me.gpx_JURYOU_GROUP.Text = "受領"
        '
        'lbl_JURYOU_KEKKA_N
        '
        Me.lbl_JURYOU_KEKKA_N.AutoSize = True
        Me.lbl_JURYOU_KEKKA_N.Checked = True
        Me.lbl_JURYOU_KEKKA_N.Location = New System.Drawing.Point(131, 47)
        Me.lbl_JURYOU_KEKKA_N.Name = "lbl_JURYOU_KEKKA_N"
        Me.lbl_JURYOU_KEKKA_N.Size = New System.Drawing.Size(59, 16)
        Me.lbl_JURYOU_KEKKA_N.TabIndex = 1031
        Me.lbl_JURYOU_KEKKA_N.TabStop = True
        Me.lbl_JURYOU_KEKKA_N.Text = "不合格"
        Me.lbl_JURYOU_KEKKA_N.UseVisualStyleBackColor = True
        '
        'lbl_JURYOU_KEKKA_Y
        '
        Me.lbl_JURYOU_KEKKA_Y.AutoSize = True
        Me.lbl_JURYOU_KEKKA_Y.Location = New System.Drawing.Point(77, 47)
        Me.lbl_JURYOU_KEKKA_Y.Name = "lbl_JURYOU_KEKKA_Y"
        Me.lbl_JURYOU_KEKKA_Y.Size = New System.Drawing.Size(47, 16)
        Me.lbl_JURYOU_KEKKA_Y.TabIndex = 1030
        Me.lbl_JURYOU_KEKKA_Y.Text = "合格"
        Me.lbl_JURYOU_KEKKA_Y.UseVisualStyleBackColor = True
        '
        'JURYOU_REMARKS
        '
        Me.JURYOU_REMARKS.Location = New System.Drawing.Point(72, 94)
        Me.JURYOU_REMARKS.MaxLength = 500
        Me.JURYOU_REMARKS.Multiline = True
        Me.JURYOU_REMARKS.Name = "JURYOU_REMARKS"
        Me.JURYOU_REMARKS.Size = New System.Drawing.Size(376, 32)
        Me.JURYOU_REMARKS.TabIndex = 1021
        '
        'lbl_JURYOU_REMARKS
        '
        Me.lbl_JURYOU_REMARKS.Location = New System.Drawing.Point(9, 94)
        Me.lbl_JURYOU_REMARKS.Name = "lbl_JURYOU_REMARKS"
        Me.lbl_JURYOU_REMARKS.Size = New System.Drawing.Size(59, 17)
        Me.lbl_JURYOU_REMARKS.TabIndex = 1025
        Me.lbl_JURYOU_REMARKS.Text = "備考"
        Me.lbl_JURYOU_REMARKS.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lbl_JURYOU_KOUSEI_BY
        '
        Me.lbl_JURYOU_KOUSEI_BY.Location = New System.Drawing.Point(9, 71)
        Me.lbl_JURYOU_KOUSEI_BY.Name = "lbl_JURYOU_KOUSEI_BY"
        Me.lbl_JURYOU_KOUSEI_BY.Size = New System.Drawing.Size(59, 17)
        Me.lbl_JURYOU_KOUSEI_BY.TabIndex = 1028
        Me.lbl_JURYOU_KOUSEI_BY.Text = "実施者"
        Me.lbl_JURYOU_KOUSEI_BY.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lbl_JURYOU_KEKKA
        '
        Me.lbl_JURYOU_KEKKA.Location = New System.Drawing.Point(9, 46)
        Me.lbl_JURYOU_KEKKA.Name = "lbl_JURYOU_KEKKA"
        Me.lbl_JURYOU_KEKKA.Size = New System.Drawing.Size(59, 17)
        Me.lbl_JURYOU_KEKKA.TabIndex = 1026
        Me.lbl_JURYOU_KEKKA.Text = "合否"
        Me.lbl_JURYOU_KEKKA.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lbl_JURYOU_CHECK_DATE
        '
        Me.lbl_JURYOU_CHECK_DATE.Location = New System.Drawing.Point(2, 19)
        Me.lbl_JURYOU_CHECK_DATE.Name = "lbl_JURYOU_CHECK_DATE"
        Me.lbl_JURYOU_CHECK_DATE.Size = New System.Drawing.Size(66, 21)
        Me.lbl_JURYOU_CHECK_DATE.TabIndex = 1027
        Me.lbl_JURYOU_CHECK_DATE.Text = "検査日"
        Me.lbl_JURYOU_CHECK_DATE.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'JURYOU_KOUSEI_BY
        '
        Me.JURYOU_KOUSEI_BY.Enabled = False
        Me.JURYOU_KOUSEI_BY.Location = New System.Drawing.Point(72, 69)
        Me.JURYOU_KOUSEI_BY.MaxLength = 20
        Me.JURYOU_KOUSEI_BY.Name = "JURYOU_KOUSEI_BY"
        Me.JURYOU_KOUSEI_BY.Size = New System.Drawing.Size(138, 19)
        Me.JURYOU_KOUSEI_BY.TabIndex = 1020
        Me.JURYOU_KOUSEI_BY.Tag = "BUNRUI_01"
        '
        'JURYOU_CHECK_DATE
        '
        Me.JURYOU_CHECK_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.JURYOU_CHECK_DATE.Location = New System.Drawing.Point(72, 19)
        Me.JURYOU_CHECK_DATE.Name = "JURYOU_CHECK_DATE"
        Me.JURYOU_CHECK_DATE.ShowCheckBox = True
        Me.JURYOU_CHECK_DATE.Size = New System.Drawing.Size(133, 19)
        Me.JURYOU_CHECK_DATE.TabIndex = 1018
        '
        'gpx_TENKEN_GROUP
        '
        Me.gpx_TENKEN_GROUP.Controls.Add(Me.lbl_TENKEN_KEKKA_N)
        Me.gpx_TENKEN_GROUP.Controls.Add(Me.TENKEN_REMARKS)
        Me.gpx_TENKEN_GROUP.Controls.Add(Me.lbl_TENKEN_KEKKA_Y)
        Me.gpx_TENKEN_GROUP.Controls.Add(Me.lbl_TENKEN_REMARKS)
        Me.gpx_TENKEN_GROUP.Controls.Add(Me.lbl_TENKEN_KOUSEI_BY)
        Me.gpx_TENKEN_GROUP.Controls.Add(Me.lbl_TENKEN_KEKKA)
        Me.gpx_TENKEN_GROUP.Controls.Add(Me.lbl_TENKEN_CHECK_DATE)
        Me.gpx_TENKEN_GROUP.Controls.Add(Me.TENKEN_KOUSEI_BY)
        Me.gpx_TENKEN_GROUP.Controls.Add(Me.TENKEN_CHECK_DATE)
        Me.gpx_TENKEN_GROUP.Location = New System.Drawing.Point(9, 250)
        Me.gpx_TENKEN_GROUP.Name = "gpx_TENKEN_GROUP"
        Me.gpx_TENKEN_GROUP.Size = New System.Drawing.Size(454, 131)
        Me.gpx_TENKEN_GROUP.TabIndex = 1030
        Me.gpx_TENKEN_GROUP.TabStop = False
        Me.gpx_TENKEN_GROUP.Text = "点検"
        '
        'lbl_TENKEN_KEKKA_N
        '
        Me.lbl_TENKEN_KEKKA_N.AutoSize = True
        Me.lbl_TENKEN_KEKKA_N.Checked = True
        Me.lbl_TENKEN_KEKKA_N.Location = New System.Drawing.Point(131, 46)
        Me.lbl_TENKEN_KEKKA_N.Name = "lbl_TENKEN_KEKKA_N"
        Me.lbl_TENKEN_KEKKA_N.Size = New System.Drawing.Size(59, 16)
        Me.lbl_TENKEN_KEKKA_N.TabIndex = 1033
        Me.lbl_TENKEN_KEKKA_N.TabStop = True
        Me.lbl_TENKEN_KEKKA_N.Text = "不合格"
        Me.lbl_TENKEN_KEKKA_N.UseVisualStyleBackColor = True
        '
        'TENKEN_REMARKS
        '
        Me.TENKEN_REMARKS.Location = New System.Drawing.Point(72, 94)
        Me.TENKEN_REMARKS.MaxLength = 500
        Me.TENKEN_REMARKS.Multiline = True
        Me.TENKEN_REMARKS.Name = "TENKEN_REMARKS"
        Me.TENKEN_REMARKS.Size = New System.Drawing.Size(376, 32)
        Me.TENKEN_REMARKS.TabIndex = 1025
        '
        'lbl_TENKEN_KEKKA_Y
        '
        Me.lbl_TENKEN_KEKKA_Y.AutoSize = True
        Me.lbl_TENKEN_KEKKA_Y.Location = New System.Drawing.Point(77, 46)
        Me.lbl_TENKEN_KEKKA_Y.Name = "lbl_TENKEN_KEKKA_Y"
        Me.lbl_TENKEN_KEKKA_Y.Size = New System.Drawing.Size(47, 16)
        Me.lbl_TENKEN_KEKKA_Y.TabIndex = 1032
        Me.lbl_TENKEN_KEKKA_Y.Text = "合格"
        Me.lbl_TENKEN_KEKKA_Y.UseVisualStyleBackColor = True
        '
        'lbl_TENKEN_REMARKS
        '
        Me.lbl_TENKEN_REMARKS.Location = New System.Drawing.Point(9, 94)
        Me.lbl_TENKEN_REMARKS.Name = "lbl_TENKEN_REMARKS"
        Me.lbl_TENKEN_REMARKS.Size = New System.Drawing.Size(59, 17)
        Me.lbl_TENKEN_REMARKS.TabIndex = 1025
        Me.lbl_TENKEN_REMARKS.Text = "備考"
        Me.lbl_TENKEN_REMARKS.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lbl_TENKEN_KOUSEI_BY
        '
        Me.lbl_TENKEN_KOUSEI_BY.Location = New System.Drawing.Point(9, 71)
        Me.lbl_TENKEN_KOUSEI_BY.Name = "lbl_TENKEN_KOUSEI_BY"
        Me.lbl_TENKEN_KOUSEI_BY.Size = New System.Drawing.Size(59, 17)
        Me.lbl_TENKEN_KOUSEI_BY.TabIndex = 1028
        Me.lbl_TENKEN_KOUSEI_BY.Text = "実施者"
        Me.lbl_TENKEN_KOUSEI_BY.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lbl_TENKEN_KEKKA
        '
        Me.lbl_TENKEN_KEKKA.Location = New System.Drawing.Point(9, 46)
        Me.lbl_TENKEN_KEKKA.Name = "lbl_TENKEN_KEKKA"
        Me.lbl_TENKEN_KEKKA.Size = New System.Drawing.Size(59, 17)
        Me.lbl_TENKEN_KEKKA.TabIndex = 1026
        Me.lbl_TENKEN_KEKKA.Text = "合否"
        Me.lbl_TENKEN_KEKKA.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lbl_TENKEN_CHECK_DATE
        '
        Me.lbl_TENKEN_CHECK_DATE.Location = New System.Drawing.Point(2, 19)
        Me.lbl_TENKEN_CHECK_DATE.Name = "lbl_TENKEN_CHECK_DATE"
        Me.lbl_TENKEN_CHECK_DATE.Size = New System.Drawing.Size(66, 21)
        Me.lbl_TENKEN_CHECK_DATE.TabIndex = 1027
        Me.lbl_TENKEN_CHECK_DATE.Text = "検査日"
        Me.lbl_TENKEN_CHECK_DATE.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'TENKEN_KOUSEI_BY
        '
        Me.TENKEN_KOUSEI_BY.Enabled = False
        Me.TENKEN_KOUSEI_BY.Location = New System.Drawing.Point(72, 69)
        Me.TENKEN_KOUSEI_BY.MaxLength = 20
        Me.TENKEN_KOUSEI_BY.Name = "TENKEN_KOUSEI_BY"
        Me.TENKEN_KOUSEI_BY.Size = New System.Drawing.Size(138, 19)
        Me.TENKEN_KOUSEI_BY.TabIndex = 1024
        Me.TENKEN_KOUSEI_BY.Tag = "BUNRUI_01"
        '
        'TENKEN_CHECK_DATE
        '
        Me.TENKEN_CHECK_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.TENKEN_CHECK_DATE.Location = New System.Drawing.Point(72, 19)
        Me.TENKEN_CHECK_DATE.Name = "TENKEN_CHECK_DATE"
        Me.TENKEN_CHECK_DATE.ShowCheckBox = True
        Me.TENKEN_CHECK_DATE.Size = New System.Drawing.Size(133, 19)
        Me.TENKEN_CHECK_DATE.TabIndex = 1022
        '
        'btn_FileClear
        '
        Me.btn_FileClear.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btn_FileClear.Location = New System.Drawing.Point(408, 389)
        Me.btn_FileClear.Name = "btn_FileClear"
        Me.btn_FileClear.Size = New System.Drawing.Size(55, 21)
        Me.btn_FileClear.TabIndex = 1028
        Me.btn_FileClear.Text = "クリア"
        '
        'btn_File
        '
        Me.btn_File.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btn_File.Location = New System.Drawing.Point(323, 389)
        Me.btn_File.Name = "btn_File"
        Me.btn_File.Size = New System.Drawing.Size(82, 21)
        Me.btn_File.TabIndex = 1027
        Me.btn_File.Text = "ファイル選択"
        '
        'CHECK_FILE
        '
        Me.CHECK_FILE.Enabled = False
        Me.CHECK_FILE.Location = New System.Drawing.Point(82, 388)
        Me.CHECK_FILE.Name = "CHECK_FILE"
        Me.CHECK_FILE.Size = New System.Drawing.Size(235, 19)
        Me.CHECK_FILE.TabIndex = 1030
        '
        'lbl_CHECK_FILE
        '
        Me.lbl_CHECK_FILE.Location = New System.Drawing.Point(17, 387)
        Me.lbl_CHECK_FILE.Name = "lbl_CHECK_FILE"
        Me.lbl_CHECK_FILE.Size = New System.Drawing.Size(61, 23)
        Me.lbl_CHECK_FILE.TabIndex = 1034
        Me.lbl_CHECK_FILE.Text = "検査書類"
        Me.lbl_CHECK_FILE.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btn_FileDownload
        '
        Me.btn_FileDownload.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btn_FileDownload.Location = New System.Drawing.Point(323, 411)
        Me.btn_FileDownload.Name = "btn_FileDownload"
        Me.btn_FileDownload.Size = New System.Drawing.Size(140, 21)
        Me.btn_FileDownload.TabIndex = 1029
        Me.btn_FileDownload.Text = "ダウンロード"
        '
        'btn_TENKEN_IN
        '
        Me.btn_TENKEN_IN.Location = New System.Drawing.Point(323, 437)
        Me.btn_TENKEN_IN.Name = "btn_TENKEN_IN"
        Me.btn_TENKEN_IN.Size = New System.Drawing.Size(140, 23)
        Me.btn_TENKEN_IN.TabIndex = 1032
        Me.btn_TENKEN_IN.Text = "点検結果入力"
        Me.btn_TENKEN_IN.UseVisualStyleBackColor = True
        '
        'KouseiUpdate
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(474, 504)
        Me.Controls.Add(Me.btn_TENKEN_IN)
        Me.Controls.Add(Me.btn_FileDownload)
        Me.Controls.Add(Me.btn_FileClear)
        Me.Controls.Add(Me.btn_File)
        Me.Controls.Add(Me.CHECK_FILE)
        Me.Controls.Add(Me.lbl_CHECK_FILE)
        Me.Controls.Add(Me.gpx_TENKEN_GROUP)
        Me.Controls.Add(Me.btn_Update)
        Me.Controls.Add(Me.lbl_MODIFIED_BY)
        Me.Controls.Add(Me.lbl_MODIFIED_DATE)
        Me.Controls.Add(Me.lbl_DUE_DATE)
        Me.Controls.Add(Me.lbl_KOUSEI_COMPANY)
        Me.Controls.Add(Me.MODIFIED_BY)
        Me.Controls.Add(Me.KOUSEI_COMPANY)
        Me.Controls.Add(Me.DUE_DATE)
        Me.Controls.Add(Me.MODIFIED_DATE)
        Me.Controls.Add(Me.KOUSEI_DATE)
        Me.Controls.Add(Me.lbl_KOUSEI_DATE)
        Me.Controls.Add(Me.lbl_BUNRUI_01)
        Me.Controls.Add(Me.M_HEADER_BUNRUI_01)
        Me.Controls.Add(Me.lbl_KANRI_NO)
        Me.Controls.Add(Me.M_HEADER_KANRI_NO)
        Me.Controls.Add(Me.btn_Cancel)
        Me.Controls.Add(Me.btn_Insert)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.gpx_JURYOU_GROUP)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "KouseiUpdate"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "校正実施登録"
        Me.gpx_JURYOU_GROUP.ResumeLayout(False)
        Me.gpx_JURYOU_GROUP.PerformLayout()
        Me.gpx_TENKEN_GROUP.ResumeLayout(False)
        Me.gpx_TENKEN_GROUP.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btn_Cancel As System.Windows.Forms.Button
    Friend WithEvents btn_Insert As System.Windows.Forms.Button
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents lbl_BUNRUI_01 As System.Windows.Forms.Label
    Friend WithEvents M_HEADER_BUNRUI_01 As System.Windows.Forms.TextBox
    Friend WithEvents lbl_KANRI_NO As System.Windows.Forms.Label
    Friend WithEvents M_HEADER_KANRI_NO As System.Windows.Forms.TextBox
    Friend WithEvents KOUSEI_DATE As System.Windows.Forms.DateTimePicker
    Friend WithEvents lbl_KOUSEI_DATE As System.Windows.Forms.Label
    Friend WithEvents lbl_KOUSEI_COMPANY As System.Windows.Forms.Label
    Friend WithEvents KOUSEI_COMPANY As System.Windows.Forms.TextBox
    Friend WithEvents lbl_DUE_DATE As System.Windows.Forms.Label
    Friend WithEvents lbl_MODIFIED_DATE As System.Windows.Forms.Label
    Friend WithEvents MODIFIED_BY As System.Windows.Forms.TextBox
    Friend WithEvents lbl_MODIFIED_BY As System.Windows.Forms.Label
    Friend WithEvents DUE_DATE As System.Windows.Forms.DateTimePicker
    Friend WithEvents btn_Update As System.Windows.Forms.Button
    Friend WithEvents MODIFIED_DATE As System.Windows.Forms.DateTimePicker
    Friend WithEvents gpx_JURYOU_GROUP As System.Windows.Forms.GroupBox
    Friend WithEvents JURYOU_REMARKS As System.Windows.Forms.TextBox
    Friend WithEvents lbl_JURYOU_REMARKS As System.Windows.Forms.Label
    Friend WithEvents lbl_JURYOU_KOUSEI_BY As System.Windows.Forms.Label
    Friend WithEvents lbl_JURYOU_KEKKA As System.Windows.Forms.Label
    Friend WithEvents lbl_JURYOU_CHECK_DATE As System.Windows.Forms.Label
    Friend WithEvents JURYOU_KOUSEI_BY As System.Windows.Forms.TextBox
    Friend WithEvents JURYOU_CHECK_DATE As System.Windows.Forms.DateTimePicker
    Friend WithEvents gpx_TENKEN_GROUP As System.Windows.Forms.GroupBox
    Friend WithEvents TENKEN_REMARKS As System.Windows.Forms.TextBox
    Friend WithEvents lbl_TENKEN_REMARKS As System.Windows.Forms.Label
    Friend WithEvents lbl_TENKEN_KOUSEI_BY As System.Windows.Forms.Label
    Friend WithEvents lbl_TENKEN_KEKKA As System.Windows.Forms.Label
    Friend WithEvents lbl_TENKEN_CHECK_DATE As System.Windows.Forms.Label
    Friend WithEvents TENKEN_KOUSEI_BY As System.Windows.Forms.TextBox
    Friend WithEvents TENKEN_CHECK_DATE As System.Windows.Forms.DateTimePicker
    Friend WithEvents btn_FileClear As System.Windows.Forms.Button
    Friend WithEvents btn_File As System.Windows.Forms.Button
    Friend WithEvents CHECK_FILE As System.Windows.Forms.TextBox
    Friend WithEvents lbl_CHECK_FILE As System.Windows.Forms.Label
    Friend WithEvents btn_FileDownload As System.Windows.Forms.Button
    Friend WithEvents lbl_JURYOU_KEKKA_N As System.Windows.Forms.RadioButton
    Friend WithEvents lbl_JURYOU_KEKKA_Y As System.Windows.Forms.RadioButton
    Friend WithEvents lbl_TENKEN_KEKKA_N As System.Windows.Forms.RadioButton
    Friend WithEvents lbl_TENKEN_KEKKA_Y As System.Windows.Forms.RadioButton
    Friend WithEvents btn_TENKEN_IN As System.Windows.Forms.Button

End Class

﻿Imports System.Windows.Forms
Imports System.Data

'
'   クラス：KouseiUpdate
'   処理　：校正実施登録のメイン画面を構成し、個別情報の編集まで対応する
'
Public Class KouseiUpdate
    Private gid As String
    Private sid As String
    Private kanriNo As String
    Private bunrui As String
    Private kouseiForm As Kousei
    Private blob As Byte() = Nothing
    Private check_file_name As String = Nothing
    Private is_file_change As Boolean = False

    Private haita_flag As Boolean = False

    ''' <summary>
    ''' Exception
    ''' </summary>
    ''' <remarks></remarks>
    Private isException As Boolean = False


    '
    '   クラスの新規生成処理
    '   
    Public Sub New(kousei As Kousei, _gid As String, _sid As Integer, _kanriNo As String, _bunrui As String)
        InitializeComponent()
        kouseiForm = kousei
        gid = _gid
        sid = _sid
        kanriNo = _kanriNo
        bunrui = _bunrui
        '   Verify the user's access permission
        If LoginRoler.Employee Is Nothing Then
            Me.Close()
            Application.ExitThread()
            Return
        End If
    End Sub

    Private Sub KouseiUpdate_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Helper.SetAllControlText(Me)
        clearCom()
        SetUserRole()
        M_HEADER_KANRI_NO.Text = kanriNo
        M_HEADER_BUNRUI_01.Text = bunrui
        If sid > 0 Then
            '更新
            btn_Insert.Visible = False
            btn_Update.Visible = True
            Dim dt As DataTable = KouseiRepository.GetDataBySID(sid)
            If dt IsNot Nothing And dt.Rows.Count > 0 Then
                M_HEADER_KANRI_NO.Text = Helper.DB_NullOrEmpty(dt.Rows.Item(0).Item("KANRI_NO"))

                If Helper.DB_NullOrEmpty(dt.Rows.Item(0).Item("KOUSEI_DATE")) IsNot "" Then
                    KOUSEI_DATE.Value = Helper.DB_NullOrEmpty(dt.Rows.Item(0).Item("KOUSEI_DATE"))
                    Me.KOUSEI_DATE.Checked = True
                End If

                KOUSEI_COMPANY.Text = Helper.DB_NullOrEmpty(dt.Rows.Item(0).Item("KOUSEI_COMPANY"))

                If Helper.DB_NullOrEmpty(dt.Rows.Item(0).Item("JURYOU_CHECK_DATE")) IsNot "" Then
                    JURYOU_CHECK_DATE.Value = Helper.DB_NullOrEmpty(dt.Rows.Item(0).Item("JURYOU_CHECK_DATE"))
                    Me.JURYOU_CHECK_DATE.Checked = True
                End If

                If Helper.DB_NullOrEmpty(dt.Rows.Item(0).Item("TENKEN_CHECK_DATE")) IsNot "" Then
                    TENKEN_CHECK_DATE.Value = Helper.DB_NullOrEmpty(dt.Rows.Item(0).Item("TENKEN_CHECK_DATE"))
                    Me.TENKEN_CHECK_DATE.Checked = True
                End If
                Dim yuryou_kekka As String = Helper.DB_NullOrEmpty(dt.Rows.Item(0).Item("JURYOU_KEKKA"))
                If yuryou_kekka = lbl_JURYOU_KEKKA_Y.Text Then
                    lbl_JURYOU_KEKKA_Y.Checked = True
                Else
                    lbl_JURYOU_KEKKA_N.Checked = True
                End If
                JURYOU_KOUSEI_BY.Text = Helper.DB_NullOrEmpty(dt.Rows.Item(0).Item("JURYOU_KOUSEI_BY"))

                Dim tenken_kekka As String = Helper.DB_NullOrEmpty(dt.Rows.Item(0).Item("TENKEN_KEKKA"))
                If tenken_kekka = lbl_TENKEN_KEKKA_Y.Text Then
                    lbl_TENKEN_KEKKA_Y.Checked = True
                Else
                    lbl_TENKEN_KEKKA_N.Checked = True
                End If

                TENKEN_KOUSEI_BY.Text = Helper.DB_NullOrEmpty(dt.Rows.Item(0).Item("TENKEN_KOUSEI_BY"))



                If Helper.DB_NullOrEmpty(dt.Rows.Item(0).Item("DUE_DATE")) IsNot "" Then
                    DUE_DATE.Value = Helper.DB_NullOrEmpty(dt.Rows.Item(0).Item("DUE_DATE"))
                    Me.DUE_DATE.Checked = True
                End If

                JURYOU_REMARKS.Text = Helper.DB_NullOrEmpty(dt.Rows.Item(0).Item("JURYOU_REMARKS"))
                TENKEN_REMARKS.Text = Helper.DB_NullOrEmpty(dt.Rows.Item(0).Item("TENKEN_REMARKS"))
                MODIFIED_BY.Text = Helper.DB_NullOrEmpty(dt.Rows.Item(0).Item("MODIFIED_BY"))
                Try
                    If dt.Rows.Item(0).Item("MODIFIED_DATE") IsNot Nothing Then
                        MODIFIED_DATE.Value = Date.Parse(dt.Rows.Item(0).Item("MODIFIED_DATE"))
                    End If

                    check_file_name = Helper.DB_NullOrEmpty(dt.Rows.Item(0).Item("CHECK_FILE_NAME"))
                    CHECK_FILE.Text = check_file_name
                    blob = dt.Rows.Item(0).Item("CHECK_FILE")
                Catch ex As Exception
                End Try
            End If
        Else
            btn_Insert.Visible = True
            MODIFIED_BY.Text = LoginRoler.Employee.EMP_NAME
            btn_Update.Visible = False
        End If
    End Sub

    Private Sub clearCom()
        Me.KOUSEI_DATE.Checked = False
        Me.KOUSEI_DATE.Format = DateTimePickerFormat.Custom
        Me.KOUSEI_DATE.CustomFormat = "   "

        Me.JURYOU_CHECK_DATE.Checked = False
        Me.JURYOU_CHECK_DATE.Format = DateTimePickerFormat.Custom
        Me.JURYOU_CHECK_DATE.CustomFormat = "   "

        Me.TENKEN_CHECK_DATE.Checked = False
        Me.TENKEN_CHECK_DATE.Format = DateTimePickerFormat.Custom
        Me.TENKEN_CHECK_DATE.CustomFormat = "   "

        Me.DUE_DATE.Checked = False
        Me.DUE_DATE.Format = DateTimePickerFormat.Custom
        Me.DUE_DATE.CustomFormat = "   "

    End Sub

    Private Sub M_KOUSEI_KANRI_KOUSEI_DATE_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles KOUSEI_DATE.ValueChanged, MODIFIED_DATE.ValueChanged
        If KOUSEI_DATE.Checked = False Then
            Me.KOUSEI_DATE.Format = DateTimePickerFormat.Custom
            Me.KOUSEI_DATE.CustomFormat = "   "

            Me.DUE_DATE.Checked = False
            Me.DUE_DATE.Format = DateTimePickerFormat.Custom
            Me.DUE_DATE.CustomFormat = "   "
            Return
        End If
        Me.KOUSEI_DATE.Format = DateTimePickerFormat.Short
        Me.KOUSEI_DATE.CustomFormat = vbNull

        Me.DUE_DATE.Checked = True
        Dim interval As String = kouseiForm.M_PME_KOUSEI_INTERVAL.Text
        If Not String.IsNullOrEmpty(interval) Then
            interval = Replace(interval, "ヶ月", "")
            Me.DUE_DATE.Value = Me.KOUSEI_DATE.Value.AddMonths(CType(interval, Integer)).AddDays(-1)
        Else
            Me.DUE_DATE.Value = Me.KOUSEI_DATE.Value
        End If
    End Sub

    Private Sub JURYOU_CHECK_DATE_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles JURYOU_CHECK_DATE.ValueChanged
        If JURYOU_CHECK_DATE.Checked = False Then
            Me.JURYOU_CHECK_DATE.Format = DateTimePickerFormat.Custom
            Me.JURYOU_CHECK_DATE.CustomFormat = "   "
            Return
        End If
        Me.JURYOU_CHECK_DATE.Format = DateTimePickerFormat.Short
        Me.JURYOU_CHECK_DATE.CustomFormat = vbNull
    End Sub

    Private Sub TENKEN_CHECK_DATE_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TENKEN_CHECK_DATE.ValueChanged
        If TENKEN_CHECK_DATE.Checked = False Then
            Me.TENKEN_CHECK_DATE.Format = DateTimePickerFormat.Custom
            Me.TENKEN_CHECK_DATE.CustomFormat = "   "
            Return
        End If
        Me.TENKEN_CHECK_DATE.Format = DateTimePickerFormat.Short
        Me.TENKEN_CHECK_DATE.CustomFormat = vbNull
    End Sub

    Private Sub DUE_DATE_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DUE_DATE.ValueChanged
        If DUE_DATE.Checked = False Then
            Me.DUE_DATE.Format = DateTimePickerFormat.Custom
            Me.DUE_DATE.CustomFormat = "   "
            Return
        End If
        Me.DUE_DATE.Format = DateTimePickerFormat.Short
        Me.DUE_DATE.CustomFormat = vbNull
    End Sub

    Private Sub btn_Insert_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Insert.Click

        Try
            If haita_flag Then
                Return
            End If
            haita_flag = True
            Me.Cursor = Cursors.WaitCursor
            Dim paramDict As Dictionary(Of String, Object) = New Dictionary(Of String, Object)

            'Dim sql As String = "INSERT INTO M_KOUSEI_KANRI (GID, KANRI_NO, KOUSEI_DATE, KOUSEI_COMPANY, JURYOU_CHECK_DATE, KEKKA, KOUSEI_BY, DUE_DATE, REMARKS, MODIFIED_DATE, MODIFIED_BY, VCHG) VALUES (:GID, :KANRI_NO, :KOUSEI_DATE, :KOUSEI_COMPANY, :JURYOU_CHECK_DATE, :KEKKA, :KOUSEI_BY, :DUE_DATE, :REMARKS, :MODIFIED_DATE, :MODIFIED_BY, :VCHG)"
            paramDict.Add("SID", KouseiRepository.GetSEQ())
            paramDict.Add("VCHG", "N")

            GetParams(paramDict)

            Dim resCheckUp = KouseiRepository.Insert("M_KOUSEI_KANRI", paramDict)
            If resCheckUp Then
                ReloadKousei()
                MsgBox(Ini.ReadValue(Ini.Config.LANG, "Main", "M02108"), MsgBoxStyle.Information, "Information")
                Me.Close()
            Else
                MsgBox(Ini.ReadValue(Ini.Config.LANG, "Main", "M02111"), MsgBoxStyle.Information, "Information")
                isException = True
            End If

        Catch ex As Exception
            Helper.Log("KouseiUpdate_btnInsert_Click", ex)
            Helper.MsgBoxAndLog(Ini.ReadValue(Ini.Config.LANG, "System", "E00001"), ex)
            isException = True
        Finally
            haita_flag = False
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub btn_Update_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Update.Click

        If TENKEN_KOUSEI_BY.Text = JURYOU_KOUSEI_BY.Text Then
            '    MsgBox(Ini.ReadValue(Ini.Config.LANG, "Main", "M02111"), MsgBoxStyle.Information, "Information")
            isException = False
            MessageBox.Show("受領実施者と点検実施者が同じです。",
                "エラー",
                MessageBoxButtons.OK,
                MessageBoxIcon.Error)
            '  MsgBox("受領実施者と点検実施者が同じです。", MsgBoxStyle.Exclamation)
            isException = True
        End If
        Try

            If isException = False Then


                If haita_flag Then
                    Return
                End If
                haita_flag = True
                Me.Cursor = Cursors.WaitCursor
                Dim paramDict As Dictionary(Of String, Object) = New Dictionary(Of String, Object)
                Dim whereDict As Dictionary(Of String, Object) = New Dictionary(Of String, Object)

                'Dim sql As String = "INSERT INTO M_KOUSEI_KANRI (GID, KANRI_NO, KOUSEI_DATE, KOUSEI_COMPANY, JURYOU_CHECK_DATE, KEKKA, KOUSEI_BY, DUE_DATE, REMARKS, MODIFIED_DATE, MODIFIED_BY, VCHG) VALUES (:GID, :KANRI_NO, :KOUSEI_DATE, :KOUSEI_COMPANY, :JURYOU_CHECK_DATE, :KEKKA, :KOUSEI_BY, :DUE_DATE, :REMARKS, :MODIFIED_DATE, :MODIFIED_BY, :VCHG)"
                whereDict.Add("SID", sid)
                paramDict.Add("VCHG", "U")
                GetParams(paramDict)

                Dim resCheckUp = KouseiRepository.Update("M_KOUSEI_KANRI", paramDict, whereDict)
                If resCheckUp Then
                    ReloadKousei()
                    MsgBox(Ini.ReadValue(Ini.Config.LANG, "Main", "M02109"), MsgBoxStyle.Information, "Information")
                    Me.Close()
                Else
                    MsgBox(Ini.ReadValue(Ini.Config.LANG, "Main", "M02111"), MsgBoxStyle.Information, "Information")
                    isException = True
                End If
            End If
        Catch ex As Exception
            Helper.Log("KouseiUpdate_btnUpdate_Click", ex)
            Helper.MsgBoxAndLog(Ini.ReadValue(Ini.Config.LANG, "System", "E00001"), ex)
            isException = True
        Finally
            haita_flag = False
            Me.Cursor = Cursors.Default
        End Try


    End Sub

    Private Function GetParams(ByVal paramDict As Dictionary(Of String, Object))
        Try
            paramDict.Add("GID", gid)
            paramDict.Add("KANRI_NO", M_HEADER_KANRI_NO.Text)
            If KOUSEI_DATE.Checked Then
                paramDict.Add("KOUSEI_DATE", KOUSEI_DATE.Value)
            Else
                paramDict.Add("KOUSEI_DATE", DBNull.Value)
            End If

            paramDict.Add("KOUSEI_COMPANY", KOUSEI_COMPANY.Text)

            If gpx_JURYOU_GROUP.Enabled Then
                If JURYOU_CHECK_DATE.Checked Then
                    paramDict.Add("JURYOU_CHECK_DATE", JURYOU_CHECK_DATE.Value)
                Else
                    paramDict.Add("JURYOU_CHECK_DATE", DBNull.Value)
                End If
                If lbl_JURYOU_KEKKA_Y.Checked Then
                    paramDict.Add("JURYOU_KEKKA", lbl_JURYOU_KEKKA_Y.Text)
                Else
                    paramDict.Add("JURYOU_KEKKA", lbl_JURYOU_KEKKA_N.Text)
                End If

                ' paramDict.Add("JURYOU_KOUSEI_BY", LoginRoler.Employee.EMP_NAME)
                paramDict.Add("JURYOU_KOUSEI_BY", JURYOU_KOUSEI_BY.Text)
                paramDict.Add("JURYOU_REMARKS", JURYOU_REMARKS.Text)


                If gpx_TENKEN_GROUP.Enabled Then
                    If TENKEN_CHECK_DATE.Checked Then
                        paramDict.Add("TENKEN_CHECK_DATE", TENKEN_CHECK_DATE.Value)
                    Else
                        paramDict.Add("TENKEN_CHECK_DATE", DBNull.Value)
                    End If
                    If lbl_TENKEN_KEKKA_Y.Checked Then
                        paramDict.Add("TENKEN_KEKKA", lbl_TENKEN_KEKKA_Y.Text)
                    Else
                        paramDict.Add("TENKEN_KEKKA", lbl_TENKEN_KEKKA_N.Text)
                    End If
                    paramDict.Add("TENKEN_KOUSEI_BY", TENKEN_KOUSEI_BY.Text)
                    paramDict.Add("TENKEN_REMARKS", TENKEN_REMARKS.Text)
                End If



            ElseIf gpx_TENKEN_GROUP.Enabled Then
                If TENKEN_CHECK_DATE.Checked Then
                    paramDict.Add("TENKEN_CHECK_DATE", TENKEN_CHECK_DATE.Value)
                Else
                    paramDict.Add("TENKEN_CHECK_DATE", DBNull.Value)
                End If
                If lbl_TENKEN_KEKKA_Y.Checked Then
                    paramDict.Add("TENKEN_KEKKA", lbl_TENKEN_KEKKA_Y.Text)
                Else
                    paramDict.Add("TENKEN_KEKKA", lbl_TENKEN_KEKKA_N.Text)
                End If
                paramDict.Add("TENKEN_KOUSEI_BY", LoginRoler.Employee.EMP_NAME)
                paramDict.Add("TENKEN_REMARKS", TENKEN_REMARKS.Text)
            End If


            If DUE_DATE.Checked Then
                paramDict.Add("DUE_DATE", DUE_DATE.Value)
            Else
                paramDict.Add("DUE_DATE", DBNull.Value)
            End If

            paramDict.Add("MODIFIED_DATE", DateTime.Now)
            paramDict.Add("MODIFIED_BY", LoginRoler.Employee.EMP_NAME)

            If is_file_change Then
                Dim byte_buffer As Byte() = Nothing
                If Not String.IsNullOrEmpty(CHECK_FILE.Text) And IO.File.Exists(CHECK_FILE.Text) Then
                    byte_buffer = IO.File.ReadAllBytes(CHECK_FILE.Text)
                Else
                    byte_buffer = New Byte(0) {}
                End If
                paramDict.Add("CHECK_FILE", byte_buffer)
                paramDict.Add("CHECK_FILE_NAME", IO.Path.GetFileName(CHECK_FILE.Text))

            ElseIf CHECK_FILE.Text = "" Then
                '   Clean file
                paramDict.Add("CHECK_FILE", Nothing)
                paramDict.Add("CHECK_FILE_NAME", Nothing)

            End If
        Catch ex As Exception
            Throw ex
        End Try
        Return Nothing
    End Function

    Public Sub ReloadKousei()
        If kouseiForm IsNot Nothing Then
            kouseiForm.InitDataGrid(False)
        End If
    End Sub

    Private Sub KouseiUpdate_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        Me.Dispose()
    End Sub

    Private Sub KouseiUpdate_FormClosing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        If haita_flag Then
            Return
        End If
        haita_flag = True
        Me.Cursor = Cursors.WaitCursor

        If Me.btn_Insert.Visible = True Then
            Dim Rtn = MsgBox(Ini.ReadValue(Ini.Config.LANG, "Main", "W02003"), MsgBoxStyle.YesNo, "Warning")
            If Rtn = 6 Then
                e.Cancel = False
                Return
            End If
        Else
            Dim Rtn = MsgBox(Ini.ReadValue(Ini.Config.LANG, "Main", "W02002"), MsgBoxStyle.YesNo, "Warning")
            If Rtn = 6 Then
                e.Cancel = False
                Return
            End If
        End If
        e.Cancel = True
        haita_flag = False
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub SetUserRole()
        If LoginRoler.Employee.AX_KOSE = "N" Then
            If LoginRoler.Employee.ROLE = "1" Then
                Me.gpx_JURYOU_GROUP.Enabled = False
                Me.gpx_TENKEN_GROUP.Enabled = False
            End If

        ElseIf LoginRoler.Employee.AX_KOSE = "R" Then
            If LoginRoler.Employee.ROLE = "1" Or LoginRoler.Employee.ROLE = "2" Then
                Me.gpx_JURYOU_GROUP.Enabled = True
                JURYOU_KOUSEI_BY.Text = LoginRoler.Employee.EMP_NAME
                Me.gpx_TENKEN_GROUP.Enabled = False
                Me.btn_TENKEN_IN.Enabled = True
            End If
        ElseIf LoginRoler.Employee.AX_KOSE = "T" Then
            If LoginRoler.Employee.ROLE = "1" Or LoginRoler.Employee.ROLE = "3" Then
                Me.gpx_JURYOU_GROUP.Enabled = False
                Me.gpx_TENKEN_GROUP.Enabled = True
                TENKEN_KOUSEI_BY.Text = LoginRoler.Employee.EMP_NAME
                Me.btn_TENKEN_IN.Enabled = False
            End If
        ElseIf LoginRoler.Employee.AX_KOSE = "B" Then
            '2019/02/27 設備管理者の場合を追加
            If LoginRoler.Employee.ROLE = "1" Or LoginRoler.Employee.ROLE = "2" Then
                Me.gpx_JURYOU_GROUP.Enabled = True
                JURYOU_KOUSEI_BY.Text = LoginRoler.Employee.EMP_NAME
                Me.gpx_TENKEN_GROUP.Enabled = False
                Me.btn_TENKEN_IN.Enabled = True
            ElseIf LoginRoler.Employee.ROLE = "3" Then
                Me.gpx_JURYOU_GROUP.Enabled = False
                Me.gpx_TENKEN_GROUP.Enabled = True
                TENKEN_KOUSEI_BY.Text = LoginRoler.Employee.EMP_NAME
                Me.btn_TENKEN_IN.Enabled = False
            End If
        Else
            Me.gpx_JURYOU_GROUP.Enabled = False
            Me.gpx_TENKEN_GROUP.Enabled = False
        End If
    End Sub

    '
    '   検査書類ファイルの選択処理
    ' 
    Private Sub btn_File_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_File.Click
        Dim openFileDialog As New OpenFileDialog()
        openFileDialog.CheckFileExists = True
        If openFileDialog.ShowDialog(Me) = DialogResult.OK Then
            is_file_change = True
            Dim fname As String = openFileDialog.FileName
            Dim info As New System.IO.FileInfo(openFileDialog.FileName)
            Dim size As Long = info.Length
            If (size > Ini.ReadValue(Ini.Config.UX, "Setting", "FileMaxSize")) Then
                Call MsgBox("選択されたファイルのサイズがシステム制限（5M)を超えています。圧縮してから選択してください。")
            Else
                CHECK_FILE.Text = fname
            End If
        End If
    End Sub

    Private Sub btn_FileClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_FileClear.Click
        If haita_flag Then
            Return
        End If
        haita_flag = True
        Me.Cursor = Cursors.WaitCursor

        CHECK_FILE.Text = ""

        Me.Cursor = Cursors.Default
        haita_flag = False
    End Sub

    Private Sub btn_Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Cancel.Click
        If haita_flag Then
            Return
        End If
        haita_flag = True
        Me.Cursor = Cursors.WaitCursor

        If Me.btn_Insert.Visible = True Then
            Dim Rtn = MsgBox(Ini.ReadValue(Ini.Config.LANG, "Main", "W02003"), MsgBoxStyle.YesNo, "Warning")
            If Rtn = 6 Then
                Me.Close()
                Me.Dispose()
                Return
            End If
        Else
            Dim Rtn = MsgBox(Ini.ReadValue(Ini.Config.LANG, "Main", "W02002"), MsgBoxStyle.YesNo, "Warning")
            If Rtn = 6 Then
                Me.Close()
                Me.Dispose()
                Return
            End If
        End If
        haita_flag = False
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub btn_FileDownload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_FileDownload.Click
        Try
            If haita_flag Then
                Return
            End If
            haita_flag = True
            Me.Cursor = Cursors.WaitCursor

            If Helper.DownloadFile(blob, check_file_name) Then
                MsgBox(Ini.ReadValue(Ini.Config.LANG, "Main", "M02117"), MsgBoxStyle.Information, "Information")
            Else
                MsgBox(Ini.ReadValue(Ini.Config.LANG, "Main", "M02103"), MsgBoxStyle.Information, "Information")
            End If
        Catch ex As Exception
            Helper.Log("btn_FileDownload_Click", ex)
        Finally
            haita_flag = False
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btn_TENKEN_IN.Click

        If gpx_TENKEN_GROUP.Enabled = True Then
            gpx_TENKEN_GROUP.Enabled = False
            TENKEN_KOUSEI_BY.Text = ""
        Else
            gpx_TENKEN_GROUP.Enabled = True
            TENKEN_KOUSEI_BY.Text = LoginRoler.Employee.EMP_NAME
        End If
    End Sub
End Class
